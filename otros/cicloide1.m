clear all;
R = 10.0;
Rp = 10.0;
w0 = -pi/4.0;
v0 = abs(w0*R);
t=[0.0:0.01:25.0];
xmru = v0*t;
ymru = 100.0*ones(1,length(t)); %el MRU sólo contribuye con coordenada X
theta = pi/2.0+w0*t;
xmcu = Rp*cos(theta);
ymcu = Rp*sin(theta);
plot(xmru+xmcu,ymru+ymcu);
axis('equal');
print('hola.svg','-dsvg');




% R = 10.0;
% w = pi/4.0;
% v0 = w*R;
% t=[0.0:0.01:50.0];
% xp(1) = 0.0;
% yp(1) = 0.5*R;
% xc(1) = 0.0;
% yc(1) = 0.0;
% r = yp(1);
% theta = 0.0;
% for i=2:length(t)
%     dt = t(i)-t(i-1);
%     fi = dt*w;
%     x = r*sin(fi);
%     y = r*(cos(fi)-1.0);
%     Rot = [cos(theta) -sin(theta)
%            sin(theta)  cos(theta)];
%     xxyy = Rot'*[x y]';
%     xp(i) = xp(i-1)+xxyy(1);
%     yp(i) = yp(i-1)+xxyy(2);
%     xc(i) = v0*t(i);
%     yc(i) = 0.0;
%     op = [xp(i)-xc(i) yp(i)-yc(1)];
%     op1 = [0.0 10.0];
%     r = norm(op);
%     theta = acos(op*op1'/(norm(op)*norm(op1)));
%     if xp(i)<xc(i)
%         theta = -theta;
%     end
% %     if theta>pi/2.0
% %         theta = theta-ceil(theta/(2.0*pi))*2.0*pi;
% %         theta = pi-theta;
% %     end
% %     [xc(i) yc(i) xp(i) yp(i)];
% % pause
% end
% plot(xp,yp);
% axis('equal');
%
% % x = R*(t-sin(t));
% % y = R*(1.0-cos(t));
% % plot(x,y);
% % axis('equal');
