%CÓRNEA
r1 = 0.0077;
r2 = 0.0068;
ni = 1.376;
n1 = 1.0;
n2 = 1.336;
fprintf(1,'Córnea:\n');
%Distancias focales
fo = -n1/((ni-n1)/r1+(n2-ni)/r2)
fi = n2/((ni-n1)/r1+(n2-ni)/r2)
%Potencia y poder refractor
P = 1.0/fi
F = n2/fi
%CRISTALINO
r1 = 0.010;
r2 = -0.006;
ni = 1.41;
n1 = 1.336;
n2 = 1.337;
fprintf(1,'Cristalino:\n');
%Distancias focales
fo = -n1/((ni-n1)/r1+(n2-ni)/r2)
fi = n2/((ni-n1)/r1+(n2-ni)/r2)
%Potencia y poder refractor
P = 1.0/fi
F = n2/fi
