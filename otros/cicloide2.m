clear all;
R = 10.0;
Rp = 10.0;
w0 = -pi/4.0;
v0 = abs(w0*R);
alfa = 70.0*pi/180.0; %ángulo medido desde la vertical
vx = v0*sin(alfa);
vz = v0*cos(alfa);
t=[0.0:0.01:15.0];
xmru = vx*t;
ymru = 100.0*ones(1,length(t)); %el MRU sólo contribuye con coordenadas X y Z
zmru = vz*t;
theta = pi/2.0+w0*t;
xmcu = Rp*cos(theta);
ymcu = Rp*sin(theta);
plot3(xmru+xmcu,ymru+ymcu,zmru);
axis('equal');
print('hola.svg','-dsvg');
