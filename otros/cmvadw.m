T1 = 2.0;
T2 = 1.8;
A1 = 4.0;
A2 = 2.0;
t = [0.0:0.01:25.0];
w1 = 2.0*pi/T1;
w2 = 2.0*pi/T2;
tau = T1*T2/(T1-T2)
y1 = A1*sin(w1*t);
y2 = A2*sin(w2*t);
y = y1+y2;
plot(t,y1,t,y2,t,y,'g');
axis('equal');
print('hola.svg','-dsvg');
