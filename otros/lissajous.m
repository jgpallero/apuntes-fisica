A1 = 2.0;
A2 = 2.0;
w1 = pi/4.0;
facw21 = [1 2
          1 3
          2 3
          3 4
          3 5
          4 5
          5 6];
delta = [0.0 pi/4.0 pi/2.0 3.0*pi/4.0 pi];
t = [0.0:0.1:50.0];
nFil = size(facw21,1);
nCol = length(delta);
pos = 1;
for i=1:nFil
    w2 = facw21(i,1)/facw21(i,2)*w1;
    x = A1*sin(w1*t);
    for j=1:nCol
        y = A2*sin(w2*t+delta(j));
        subplot(nFil,nCol,pos);
        plot(x,y);
        axis('equal');
        pos = pos+1;
    end
end
print('hola.svg');
