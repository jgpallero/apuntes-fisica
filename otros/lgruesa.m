%escala de inkscape: de 280 a 290 para mí son 100

n1 = 1.2;
ni = 3.0;
n2 = 1.1;
O1 = 0.0;
O2 = 1365.0;
r1 = 2620.0;
r2 = -1655.0;

%rayo hacia la derecha
ee = O2-O1;
sp1 = ni*r1/(ni-n1);
s2 = sp1-ee;
sp2 = n2*r2*(ni*r1+ee*(n1-ni))/...
      (ee*(ni*(ni-n1-n2)+n1*n2)+ni*(r1*(n2-ni)+r2*(ni-n1)))
sp2 = n2*r2*s2/(ni*r2+(n2-ni)*s2)
fp = sp1*sp2/s2
f = -n1/n2*fp
hp = sp2-fp

%rayo hacia la izquierda
aux = O1;
O1 = O2;
O2 = aux;
ee = O1-O2;
r1 = -r1;
r2 = -r2;
sp1 = ni*r2/(ni-n2);
s2 = sp1-ee;
sp2 = n1*r1*s2/(ni*r1+(n1-ni)*s2)
f = sp1*sp2/s2
h = sp2-f
