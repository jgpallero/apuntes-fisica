t = 0:0.01:50.0;
A = 5.0;
o0 = pi/4;
m = 0.1;
y0 = A;
v0 = 0.0;

b = 0.01; %subamortiguado coseno
alfa = -b/(2.0*m);
bet = sqrt(o0^2-(b/(2.0*m))^2);
fi0 = atan2(y0*alfa-v0,y0*bet);
A0 = y0/cos(fi0);
ysub_cos = A0*exp(alfa*t).*cos(bet*t+fi0);

%subamortiguado seno
fi0 = atan2(y0*bet,v0-y0*alfa);
A0 = y0/sin(fi0);
ysub_exp = A0*exp(alfa*t);
ysub_sen = ysub_exp.*sin(bet*t+fi0);

b = 0.5; %sobreamortiguado
aux1 = -b/m;
aux2 = sqrt((b/m)^2-4.0*o0^2);
r1 = (aux1+aux2)/2.0;
r2 = (aux1-aux2)/2.0;
c1 = (v0-y0*r2)/(r1-r2);
c2 = (y0*r1-v0)/(r1-r2);
ysobr = c1*exp(r1*t)+c2*exp(r2*t);

b = 1.570796326794897e-01; %crítico
c1 = y0;
c2 = v0+y0*b/(2.0*m);
ycrit = (c1+c2*t).*exp(-b/(2.0*m)*t);

plot(t,ysub_cos,t,ysub_sen,t,ysobr,t,ycrit,t,ysub_exp,t,-ysub_exp);
