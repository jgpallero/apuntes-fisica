\chapter{El Sistema Internacional de Unidades}

\section{Introducci\'on}

Todo proceso de medida da lugar a una \textit{magnitud},\index{magnitud} la cual
se traduce en un valor num\'erico relacionado con un patr\'on de referencia.
Este patr\'on se denomina \textit{unidad},\index{unidad} por lo que puede
deducirse que una magnitud consta de un n\'umero y su unidad.

Los \textit{s\'imbolos}\index{simbolo@s\'imbolo} de unidades son entidades
matem\'aticas y \textbf{no} abreviaciones, es decir, no equivalen ni a palabras
(nombres de unidades) ni a sus abreviaturas
\citep[\pnbs70]{bezos2008}\footnote{\cite{bezos2008} es una referencia
imprescindible en castellano en cuestiones de tipograf\'ia y notaci\'on.}. Al
formar productos o cocientes de unidades se aplican las reglas del \'algebra
\citep{cem2019b}. Los nombres de las unidades f\'isicas dependen de la lengua,
mientras que los s\'imbolos son internacionales e iguales en todos los idiomas
\citep[\pnbs70]{bezos2008}.

El valor de una magnitud se escribe como el producto de un n\'umero por una
unidad, siendo dicho guarismo el valor num\'erico de la citada magnitud con
respecto a la unidad de  trabajo (el n\'umero y el s\'imbolo de la unidad se
separan por un  espacio\footnote{En f\'ormulas el espacio \textbf{ha} de ser
fino; en texto normal, generalmente ordinario \citep[\pnbs82]{bezos2008}.}). En
el caso de magnitudes adimensionales, para las cuales la unidad es el n\'umero
$1$, se omite dicha unidad. El valor num\'erico de una magnitud depende de la
elecci\'on de la unidad, de forma que aqu\'ella puede tener valores num\'ericos
diferentes cuando se expresa referida a diferentes unidades. As\'i, podemos
expresar la velocidad de un m\'ovil como
$v=\SI[per-mode=reciprocal]{5}{\metre\per\second}$ o como
$v=\SI[per-mode=reciprocal]{18}{\kilo\metre\per\hour}$, siendo que en ambos
casos la magnitud es la misma.

El Sistema Internacional de Unidades (abreviado como SI) fue creado por la
Oficina Internacional de Pesas y Medidas en 1960 tomando como base el sistema
m\'etrico decimal (sistema que incluye el metro y se basa en potencias de $10$)
y las unidades pr\'acticas electromagn\'eticas, y ha sido adoptado en casi todos
los pa\'ises del mundo \citep[\pnbs69]{bezos2008}.

\section{Resumen del Sistema Internacional de Unidades}

En las p\'aginas siguientes se reproduce \'integramente el documento titulado
<<Resumen conciso del Sistema Internacional de Unidades, SI>> \citep{cem2019b},
que es un resumen del documento oficial donde se expone y detalla el Sistema
Internacional de Unidades \citep{bipm2019b}. De este \'ultimo texto existe una
traducci\'on oficial al castellano realizada por el Centro Espa\~nol de
Metrolog\'ia \citep{cem2019a}.

\includepdf[pages={-},offset=0.495cm -0.55cm,scale=0.865,pagecommand={\thispagestyle{pagpapers}}]{otros/si-cem}

\section{Ecuaci\'on de dimensiones}

Como hemos visto en el documento \cite{cem2019b}, las magnitudes b\'asicas son
siete (longitud, masa, tiempo, corriente el\'ectrica, temperatura
termodin\'amica, cantidad de sustancia e intensidad luminosa) y sus unidades
tienen s\'imbolos perfectamente definidos en el SI. Para representar algunas
de ellas (y aqu\'i no debe confundirse el s\'imbolo de la magnitud con el
s\'imbolo de la unidad) pueden emplearse diferentes notaciones, como $l$, $x$,
$r$, etc. para la longitud o $I$ o $i$ para la corriente el\'ectrica.

Por convenio, las magnitudes f\'isicas se organizan seg\'un un sistema de
dimensiones. Se considera que cada una de las siete magnitudes b\'asicas del SI
tiene su propia dimensi\'on, representada simb\'olicamente por \textbf{una sola}
letra may\'uscula en fuente redonda \citep[\pnbs34]{cem2019a}, tal y como se
muestra en \elcuadro~\ref{cuad:Dimensiones}.
\begin{table}[htb]
\centering
\caption{Magnitudes b\'asicas y s\'imbolos de sus dimensiones.}
\label{cuad:Dimensiones}
\begin{tabular}{l|c}
\textbf{Magnitud b\'asica}  & \textbf{S\'imbolo de la dimensi\'on} \\
\hline
Longitud                    & L \\
\hline
Masa                        & M \\
\hline
Tiempo                      & T \\
\hline
Corriente el\'ectrica       & I \\
\hline
Temperatura termodin\'amica & $\Theta$\\
\hline
Cantidad de sustancia       & N \\
\hline
Intensidad luminosa         & J \\
\hline
\end{tabular}
\end{table}

Como sabemos, todas las magnitudes distintas a las listadas en
\elcuadro~\ref{cuad:Dimensiones} son derivadas de \'estas, es decir, que pueden
ser expresadas como combinaci\'on de las magnitudes b\'asicas. Las dimensiones
de las magnitudes derivadas podr\'an escribirse entonces en forma de producto de
potencias de las magnitudes b\'asicas como \citep[\ppnbs22 y\nbs23]{cem2019a}
\begin{equation}
\label{ec:EcuacDim}
\text{dim }Q=\text{L}^\alpha\,\text{M}^\beta\,\text{T}^\gamma\,\text{I}^\delta\,
\Theta^\varepsilon\,\text{N}^\zeta\,\text{J}^\eta,
\end{equation}
donde $Q$ es la magnitud derivada, <<dim>> indica sus dimensiones y los
exponentes $\alpha$, $\beta$, $\gamma$, $\delta$, $\varepsilon$, $\zeta$ y/o
$\eta$ pueden valer $0$. La expresi\'on~(\ref{ec:EcuacDim}) recibe el nombre de
\textit{ecuaci\'on de dimensiones} de $Q$\index{ecuaci\'on!de dimensiones} y
puede plantearse para cualquier magnitud. Por lo tanto, si en la expresi\'on de
una ley f\'isica sustituimos cada magnitud fundamental por su s\'imbolo
obtendremos la ecuaci\'on de dimensiones de la magnitud derivada
correspondiente \citep[\pnbs8]{dejuana2003a}.

De este modo, por ejemplo, la ecuaci\'on de dimensiones de la velocidad se
expresar\'a como
\begin{equation}
\label{ec:EcuacDimVel}
\left[v\right]=\left[l\right]/\left[t\right]=\text{L}\,\text{T}^{-1},
\end{equation}
donde la notaci\'on $\left[\cdots\right]$ quiere decir <<dimensiones de>> (en
este caso trabajamos con las magnitudes b\'asicas longitud y tiempo). Para la
fuerza la ecuaci\'on de dimensiones ser\'a
\begin{equation}
\left[F\right]=\left[m\right]\,\left[a\right]=
\left[m\right]\,\left[v\right]/\left[t\right]=
\text{M}\,\text{L}\,\text{T}^{-1}\,\text{T}^{-1}=
\text{M}\,\text{L}\,\text{T}^{-2},
\end{equation}
donde hemos utilizado directamente la \ecuac~(\ref{ec:EcuacDimVel}) para la
velocidad.

Se dice que una expresi\'on es \textit{homog\'enea}
\index{dimensiones!homogeneidad} cuando ambos miembros tienen la misma
ecuaci\'on de dimensiones \citep[\pnbs8]{dejuana2003a}. Decimos tambi\'en que
una serie de unidades f\'isicas son \textit{coherentes}
\index{dimensiones!coherencia} entre s\'i cuando la ecuaci\'on de dimensiones de
la ley f\'isica que las relaciona no contiene factor num\'erico multiplicativo
alguno, salvo la unidad \citep[\pnbs8]{dejuana2003a}. Todas las unidades
derivadas obtenidas a partir de la \ecuac~(\ref{ec:EcuacDim}) son coherentes.
As\'i, podemos expresar una fuerza como
$\SI{100}{\newton}=\SI[per-mode=reciprocal]{100}{\kg\metre\per\square\second}$,
ya que al ser las unidades coherentes no necesitaremos ning\'un factor de
conversi\'on entre el newton y la expresi\'on
$\si[per-mode=reciprocal]{\kg\metre\per\square\second}$. Tambi\'en son
coherentes las unidades $\si{\pascal}$ y
$\si[per-mode=reciprocal]{\newton\per\square\metre}$ para dar un valor de
presi\'on. Sin embargo, una velocidad en millas por hora
($\si[per-mode=reciprocal]{\mile\per\hour}$) no es coherente con la misma
magnitud expresada en $\si[per-mode=reciprocal]{\kilo\metre\per\hour}$ al ser
necesario el factor $1/1.609344$ para pasar de $\si{\mile}$ a
$\si{\kilo\metre}$. Tampoco son coherentes los conjuntos de unidades donde se
mezclen m\'ultiplos y subm\'ultiplos, pues siempre ser\'a necesario utilizar un
factor distinto de la unidad para pasar de unas a otras (por ejemplo,
$\SI{1}{\kilo\metre}=\SI{1000}{\metre}$, luego $\si{\kilo\metre}$ y
$\si{\metre}$ no son cohrentes al estar presente el factor $10^3$ en la
conversi\'on).

\section{Cifras significativas}

Puesto que los n\'umeros que se manejan en ciencias, y en f\'isica en
particular, son en la mayor parte de los casos el resultado de una medida, que
\textbf{nunca} es perfecta, se introduce el concepto de \textit{cifra
significativa}\index{cifra significativa} como una manera aproximada de
relacionarlos con su incertidumbre. Recibe el nombre de cifra significativa todo
d\'igito, exceptuando el cero cuando se utiliza para situar el punto decimal,
cuyo valor se conoce con seguridad \citep[\ppnbs8 y\nbs9]{tipler2010a}. Podemos
dar las siguientes reglas al respecto:
\begin{itemize}
\item En valores que no contienen ceros todas las cifras son significativas. Por
      ejemplo, el n\'umero $18$ tiene dos cifras significativas y el $3.14159$
      tiene seis.
\item Los ceros situados entre cifras significativas son significativos. Por
      ejemplo, el n\'umero $305$ tiene tres cifras significativas y el
      $1.318034$ tiene siete.
\item Los ceros que se utilizan para situar el punto decimal no son
      significativos \citep[\ppnbs8 y\nbs9]{tipler2010a}. Otra forma de expresar
      lo anterior ser\'ia decir que para valores situados en el intervalo
      $(-1,1)$ todos los ceros a la izquierda del primer d\'igito distinto de
      cero no son significativos. Por ejemplo, el n\'umero $0.00609$ tiene tres
      cifras significativas. Obviamente los ceros situados a la izquierda de la
      parte entera del n\'umero no son significativos; por ejemplo, el n\'umero
      $09.5$ tiene dos cifras significativas.
\item En n\'umeros en valor absoluto mayores o iguales a la unidad los ceros
      situados a la derecha del punto decimal son significativos. Por ejemplo,
      el n\'umero $4.000$ tiene cuatro cifras significativas y el $-4.07$ tiene
      tres.
\item Por \'ultimo, en n\'umeros enteros que sean potencia de $10$ tenemos una
      ambig\"uedad y todo depender\'a de la manera en que los escribamos. As\'i,
      el n\'umero $\num{9000}$ tendr\'a una cifra significativa si lo expresamos
      como $\num{9e3}$ y tres cifras significativas si lo escribimos en la forma
      $\num{9.00e3}$.
\end{itemize}
