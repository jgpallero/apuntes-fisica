\chapter{Identidades trigonom\'etricas}
\label{cap:IT}

En este ap\'endice se muestran ciertas relaciones trigonom\'etricas \'utiles. El
lector interesado puede encontrar m\'as en
\citet[secci\'on\nbs4.3]{abramowitz1965}.

\section{Identidades b\'asicas}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\tan\alpha=\frac{\sin\alpha}{\cos\alpha},\\
&\cot\alpha=\frac{1}{\tan\alpha}=\frac{\cos\alpha}{\sin\alpha},\\
&\sec\alpha=1/\cos\alpha,\\
&\cosec\alpha=1/\sin\alpha.
\end{empheq}
\end{subequations}

\section{Relaciones de simetr\'ia}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin(\alpha)=\cos\left(\pm\alpha\mp\frac{\pi}{2}\right)=
              \cos\left[\pm\alpha\mp(2k+1)\frac{\pi}{2}\right],\text{ con } k\in\mathbb{N},\\
&\sin(-\alpha)=-\sin\alpha=\cos\left(\frac{\pi}{2}+\alpha\right)=
               \sin(\pi+\alpha)=\sin(2k\pi-\alpha),\text{ con } k\in\mathbb{Z},\label{ec:sin_a_sina}\\
&\cos(-\alpha)=\cos\alpha=\sin\left(\frac{\pi}{2}\pm\alpha\right)=
                -\cos(\pi\pm\alpha)=\cos(2k\pi\pm\alpha),\text{ con } k\in\mathbb{Z},\label{ec:cos_acosa}\\
&\tan(-\alpha)=-\tan\alpha=\cot\left(\frac{\pi}{2}+\alpha\right)=
                \tan(\pi-\alpha)=\tan(2k\pi-\alpha),\text{ con } k\in\mathbb{Z},\\
&\sin\left(\alpha+\frac{\pi}{4}\right)=\cos\left(\frac{\pi}{4}-\alpha\right),\\
&\cos\left(\alpha+\frac{\pi}{4}\right)=\sin\left(\frac{\pi}{4}-\alpha\right).
\end{empheq}
\end{subequations}

\section{Razones trigonom\'etricas de sumas y diferencias de \'angulos}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin(\alpha\pm\beta)=\sin\alpha\cos\beta\pm\cos\alpha\sin\beta,\label{ec:sinapmb}\\
&\cos(\alpha\pm\beta)=\cos\alpha\cos\beta\mp\sin\alpha\sin\beta,\label{ec:cosapmb}\\
&\tan(\alpha\pm\beta)=\frac{\tan\alpha\pm\tan\beta}{1\mp\tan\alpha\tan\beta}.
\end{empheq}
\end{subequations}

\section{F\'ormulas del \'angulo doble}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin(2\alpha)=2\sin\alpha\cos\alpha=\frac{2\tan\alpha}{1+\tan^2\alpha},\\
&\cos(2\alpha)=\cos^2\alpha-\sin^2\alpha=2\cos^2\alpha-1=1-2\sin^2\alpha=\frac{1-\tan^2\alpha}{1+\tan^2\alpha},\\
&\tan(2\alpha)=\frac{2\tan\alpha}{1-\tan^2\alpha}.
\end{empheq}
\end{subequations}

\section{F\'ormulas del \'angulo triple}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin(3\alpha)=2\sin\alpha-4\sin^3\alpha,\\
&\cos(3\alpha)=4\cos^3\alpha-3\cos\alpha,\\
&\tan(3\alpha)=\frac{3\tan\alpha-\tan^3\alpha}{1-3\tan^2\alpha},\label{ec:tan3a}
\end{empheq}
\end{subequations}
donde la \ecuac~(\ref{ec:tan3a}) ha sido tomada de la p\'agina web
\url{http://mathworld.wolfram.com/Multiple-AngleFormulas.html}.

\section{Relaciones de recurrencia para el \'angulo en\'esimo}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin(n\alpha)=2\sin\left[(n-1)\alpha\right]\cos\alpha-\sin\left[(n-2)\alpha\right],\\
&\cos(n\alpha)=2\cos\left[(n-1)\alpha\right]\cos\alpha-\cos\left[(n-2)\alpha\right],\\
&\tan(n\alpha)=\frac{\tan\left[(n-1)\alpha\right]+\tan\alpha}{1-\tan\left[(n-1)\alpha\right]\tan\alpha},
\end{empheq}
\end{subequations}
que han sido tomadas de
\url{https://mathworld.wolfram.com/Multiple-AngleFormulas.html}.

\section{F\'ormulas del \'angulo mitad}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin\frac{\alpha}{2}=(-1)^{\lfloor\frac{\alpha}{2\pi}\rfloor}\sqrt{\frac{1-\cos\alpha}{2}},\\
&\cos\frac{\alpha}{2}=(-1)^{\lfloor\frac{\alpha+\pi}{2\pi}\rfloor}\sqrt{\frac{1+\cos\alpha}{2}},\\
&\tan\frac{\alpha}{2}=(-1)^{\lfloor\frac{\alpha}{\pi}\rfloor}\sqrt{\frac{1-\cos\alpha}{1+\cos\alpha}}=
\frac{\left[(-1)^{\lfloor\frac{\alpha+\frac{\pi}{2}}{\pi}\rfloor}\sqrt{1+\tan^2\alpha}\right]-1}{\tan\alpha}=
\frac{1-\cos\alpha}{\sin\alpha}=\frac{\sin\alpha}{1+\cos\alpha},
\end{empheq}
\end{subequations}
que han sido tomadas de
\url{http://mathworld.wolfram.com/Half-AngleFormulas.html} y donde el operador
$\lfloor x\rfloor$ se define como
$\max\left\{m\in\mathbb{Z}\,|\,m\le x\right\}$, correspondiente a la funci\'on
\texttt{floor} que proveen la mayor\'ia de los lenguajes de programaci\'on.

\section{Relaciones entre cuadrados}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin^2\alpha+\cos^2\alpha=1,\label{ec:sin2apcos2a1}\\
&\sec^2\alpha-\tan^2\alpha=1,\\
&\cosec^2\alpha-\cot^2\alpha=1,\\
&\sin^2\alpha-\sin^2\beta=\sin(\alpha+\beta)\sin(\alpha-\beta),\\
&\cos^2\alpha-\cos^2\beta=-\sin(\alpha+\beta)\sin(\alpha-\beta),\\
&\cos^2\alpha-\sin^2\beta=\cos(\alpha+\beta)\cos(\alpha-\beta).
\end{empheq}
\end{subequations}

\section{Productos de funciones}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&2\sin\alpha\sin\beta=\cos(\alpha-\beta)-\cos(\alpha+\beta),\\
&2\cos\alpha\cos\beta=\cos(\alpha-\beta)+\cos(\alpha+\beta),\\
&2\sin\alpha\cos\beta=\sin(\alpha-\beta)+\sin(\alpha+\beta).
\end{empheq}
\end{subequations}

\section{Suma y resta de funciones}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\sin\alpha+\sin\beta=2\sin\frac{\alpha+\beta}{2}\cos\frac{\alpha-\beta}{2},\label{ec:sinapsinb}\\
&\sin\alpha-\sin\beta=2\cos\frac{\alpha+\beta}{2}\sin\frac{\alpha-\beta}{2},\label{ec:sinamsinb}\\
&\cos\alpha+\cos\beta=2\cos\frac{\alpha+\beta}{2}\cos\frac{\alpha-\beta}{2},\label{ec:cosapcosb}\\
&\cos\alpha-\cos\beta=-2\sin\frac{\alpha+\beta}{2}\sin\frac{\alpha-\beta}{2},\label{ec:cosamcosb}\\
&\tan\alpha\pm\tan\beta=\frac{\sin(\alpha\pm\beta)}{\cos\alpha\cos\beta},\\
&\sin\alpha+\cos\alpha=\sqrt{2}\sin\left(\alpha+\frac{\pi}{4}\right)=
                       \sqrt{2}\cos\left(\frac{\pi}{4}-\alpha\right),\\
&\sin\alpha-\cos\alpha=-\sqrt{2}\cos\left(\alpha+\frac{\pi}{4}\right)=
                       -\sqrt{2}\sin\left(\frac{\pi}{4}-\alpha\right).
\end{empheq}
\end{subequations}

\section{F\'ormulas de Euler}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&e^{i\alpha}=\cos\alpha+i\sin\alpha,\label{ec:eulerpos}\\
&e^{-i\alpha}=\cos\alpha-i\sin\alpha,\label{ec:eulerneg}\\
&\sin\alpha=\frac{e^{i\alpha}+e^{-i\alpha}}{2},\\
&\cos\alpha=\frac{e^{i\alpha}-e^{-i\alpha}}{2i}.
\end{empheq}
\end{subequations}

\section{Otras relaciones}

\begin{subequations}
\begin{empheq}[left=\empheqlbrace]{align}
&\tan\left(\frac{\pi}{4}+\alpha\right)=
\frac{\sin\left(2\alpha+\frac{\pi}{2}\right)}{1+\cos\left(2\alpha+\frac{\pi}{2}\right)}
=\frac{\cos(2\alpha)}{1-\sin(2\alpha)},\\
&\tan\left(\frac{\pi}{4}-\alpha\right)=
\frac{\sin\left(\frac{\pi}{2}-2\alpha\right)}{1+\cos\left(\frac{\pi}{2}-2\alpha\right)}
=\frac{\cos(2\alpha)}{1+\sin(2\alpha)}.
\end{empheq}
\end{subequations}
