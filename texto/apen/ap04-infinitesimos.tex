\chapter{Infinit\'esimos}

\section{Definici\'on y propiedades}

Decimos\footnote{La mayor parte del texto de este ap\'endice es una reescritura
del documento que se puede encontrar en
\url{http://asignaturas.topografia.upm.es/matematicas/primero/Apuntes/Formula_de_Taylor/Infinitesimos.pdf}.}
que una funci\'on \textbf{real} de variable \textbf{real} $y=f(x)$ es
infinitamente peque\~na, infinitesimal o
\textit{infinit\'esimo}\index{infinit\'esimos} para $x\rightarrow a$ o
$x\rightarrow\pm\infty$ si y s\'olo si se cumple que
\begin{equation}
\lim_{x\rightarrow a}f(x)=0\text{\hspace{0.5cm}o\hspace{0.5cm}}
\lim_{x\rightarrow\pm\infty}f(x)=0.
\end{equation}

Sea $\varepsilon$ un valor arbitrariamente peque\~no. Se cumple entonces
(v\'ease \lafigura~\ref{fig:apen1-01-infinitesimos}) que
\begin{itemize}
\item Si $\lim_{x\rightarrow a}f(x)=0$ existe un entorno de radio $\delta$ tal
      que para cada $x\in(a-\delta,a+\delta)$ se verifica la desigualdad
      $|f(x)|<\varepsilon$.
\item Si $\lim_{x\rightarrow\pm\infty}f(x)=0$ existe un valor $x_0$ tal que para
      cada valor $|x|>|x_0|$ se verifica la desigualdad $|f(x)|<\varepsilon$.
\end{itemize}
\begin{figure}[htb]
\centering
\includegraphics[width=0.99\textwidth]{fig-apen1-01-infinitesimos}
\caption{Infinit\'esimos.}
\label{fig:apen1-01-infinitesimos}
\end{figure}

Como ejemplos, podemos indicar que la funci\'on $f(x)=1/x$ es un infinit\'esimo
cuando $x\rightarrow\infty$, $f(x)=\sin(x)$ cuando $x\rightarrow0$,
$f(x)=\tan(x)-1$ cuando $x\rightarrow\pi/4$ y $f(x)=\ln(x)$ cuando
$x\rightarrow1$.

Los infinit\'esimos verifican las siguientes propiedades:
\begin{enumerate}
\item Sea $y=g(x)=b+f(x)$, donde $b\in\mathbb{R}$ y $f(x)$ es un infinit\'esimo
      cuando $x\rightarrow a$; entonces $\lim_{x\rightarrow a}g(x)=b$. De manera
      rec\'iproca, si $\lim_{x\rightarrow a}g(x)=b$ podremos escribir la
      funci\'on como $g(x)=b+f(x)$, con $f(x)$ infinit\'esimo cuando
      $x\rightarrow a$. La propiedad es an\'aloga para el caso de
      $x\rightarrow\pm\infty$.
\item La suma algebraica de un n\'umero \textbf{finito} de infinit\'esimos
      cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ es un infinit\'esimo
      cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ respectivamente.
\item El producto de una funci\'on \textbf{acotada} por un infinit\'esimo cuando
      $x\rightarrow a$ o $x\rightarrow\pm\infty$ es otro infinit\'esimo cuando
      $x\rightarrow a$ o $x\rightarrow\pm\infty$. En particular, el producto de
      dos infinit\'esimos cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ es
      otro infinit\'esimo cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$.
\item El cociente entre un infinit\'esimo cuando $x\rightarrow a$ o
      $x\rightarrow\pm\infty$ y una funci\'on \textbf{no nula} es otro
      infinit\'esimo cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$.
\end{enumerate}

\section{Infinit\'esimos comparables}

Dos infinit\'esimos $f(x)$ y $g(x)$ cuando $x\rightarrow a$ o
$x\rightarrow\pm\infty$ se dicen
\textit{comparables}\index{infinit\'esimos!comparables} si y s\'olo si existen
los siguientes l\'imites:
\begin{equation}
\lim_{x\rightarrow a}\frac{f(x)}{g(x)}=k\text{\hspace{0.5cm}o\hspace{0.5cm}}
\lim_{x\rightarrow\pm\infty}\frac{f(x)}{g(x)}=k,
\end{equation}
donde $k\in\mathbb{R}$. Entonces,
\begin{itemize}
\item Si $k\neq0$ se dice que $f(x)$ y $g(x)$ son infinit\'esimos del mismo
      orden.
\item Si $k=0$ implica que $\lim_{x\rightarrow a}g(x)/f(x)=\infty$ o bien que
      $\lim_{x\rightarrow\pm\infty}g(x)/f(x)=\infty$, y se dice que $f(x)$ es un
      infinit\'esimo de \textbf{mayor orden} o un infinit\'esimo de
      \textbf{orden superior} que $g(x)$, o, de manera equivalente, que $g(x)$
      es un infinit\'esimo de \textbf{menor orden} o de \textbf{orden inferior}
      que $f(x)$.
\end{itemize}

\section{Infinit\'esimos equivalentes}
\label{sec:IE}

Dos infinit\'esimos $f(x)$ y $g(x)$ cuando $x\rightarrow a$ o
$x\rightarrow\pm\infty$ se dicen
\textit{equivalentes}\index{infinit\'esimos!equvalentes} si y s\'olo si
\begin{equation}
\lim_{x\rightarrow a}\frac{f(x)}{g(x)}=1\text{\hspace{0.5cm}o\hspace{0.5cm}}
\lim_{x\rightarrow\pm\infty}\frac{f(x)}{g(x)}=1.
\end{equation}
Escribiremos en estos casos $f(x)\sim g(x)$ cuando $x\rightarrow a$ o
$x\rightarrow\pm\infty$. Son infinit\'esimos equivalentes, por ejemplo:
\begin{equation*}
\begin{split}
&\sin(x)\sim\tan(x)\sim\arcsin(x)\sim\arctan(x)\sim x\text{\hspace{0.5cm}cuando\hspace{0.5cm}} x\rightarrow0,\\
&1-\cos(x)\sim x^2/2\text{\hspace{0.5cm}cuando\hspace{0.5cm}} x\rightarrow0,\\
&a^x-1\sim x\ln(a)\text{\hspace{0.5cm}cuando\hspace{0.5cm}} x\rightarrow0.
\end{split}
\end{equation*}

\section{Orden de un infinit\'esimo}

Como consecuencia de las secciones precedentes, siendo $f(x)$ y $g(x)$ dos
infinit\'esimos cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ diremos que
la funci\'on $f(x)$ es un infinit\'esimo de orden $n$ con respecto de $g(x)$ si
y s\'olo si\index{infinit\'esimos!orden}
\begin{equation}
\lim_{x\rightarrow a}\frac{f(x)}{\left[g(x)\right]^n}=k\neq0
\text{\hspace{0.5cm}o\hspace{0.5cm}}
\lim_{x\rightarrow\pm\infty}\frac{f(x)}{\left[g(x)\right]^n}=k\neq0,
\end{equation}
con $k\in\mathbb{R}$. Como ejemplos podemos indicar que el infinit\'esimo
$f(x)=k\,x^n$ es de orden $n$ respecto del infinit\'esimo $g(x)=x$ cuando
$x\rightarrow0$, y que el infinit\'esimo $f(x)=k(x-a)^n$ es de orden $n$
respecto del infinit\'esimo $g(x)=x-a$ cuando $x\rightarrow a$. As\'i,
$f(x)=5x^2$ ser\'a un infinit\'esimo de orden $2$ con respecto a $g(x)=x$ cuando
$x\rightarrow0$, y $f(x)=7(x-1)^3$ lo ser\'a de orden $3$ con respecto a
$g(x)=x-1$ cuando $x\rightarrow1$.
\newpage
Se verifican adem\'as los siguientes teoremas:
\begin{teor}
La suma de un n\'umero \textbf{finito} de infinit\'esimos de distintos \'ordenes
cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ es otro infinit\'esimo
equivalente al de orden \textbf{inferior}.
\end{teor}
\begin{proof}
Sean $f(x)$, $g_1(x)$,$\dots$, $g_n(x)$ infinit\'esimos cuando $x\rightarrow a$
o $x\rightarrow\pm\infty$ y tal que el orden de las $g_*(x)$ es mayor que el de
$f(x)$. Entonces
\begin{equation}
\lim_{x\rightarrow a}\frac{f(x)+g_1(x)+\cdots+g_n(x)}{f(x)}=
\lim_{x\rightarrow a}\left[1+\frac{g_1(x)+\cdots+g_n(x)}{f(x)}\right]=
1+0=1
\end{equation}
por ser el orden de $g_*(x)$ es mayor que el de $f(x)$. Luego
$f(x)+\sum_{i=1}^ng_i(x)\sim f(x)$ cuando $x\rightarrow a$. La demostraci\'on es
an\'aloga si los infinit\'esimos lo son cuando $x\rightarrow\pm\infty$.
\end{proof}
\begin{remark}
El infinit\'esimo $p(x)=5x^3-4x^2+2x$ es equivalente al infinit\'esimo $f(x)=2x$
cuando $x\rightarrow0$ puesto que
\begin{equation}
\lim_{x\rightarrow0}\frac{p(x)}{f(x)}=
\lim_{x\rightarrow0}\frac{5x^3-4x^2+2x}{2x}=
\lim_{x\rightarrow0}\left(\frac{5}{2}x^2-2x+1\right)=1,
\end{equation}
luego $5x^3-4x^2+2x\sim2x$ cuando $x\rightarrow0$.
\end{remark}

\begin{teor}
El l\'imite cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$ de toda
expresi\'on de la forma $E(x)\cdot f(x)$, donde $f(x)$ es un infinit\'esimo
cuando $x\rightarrow a$ o $x\rightarrow\pm\infty$, no var\'ia si se sustituye
$f(x)$ por un infinit\'esimo equivalente $p(x)\sim f(x)$ que cumpla la
condici\'on de ser no nulo en un cierto entorno reducido de $a$.
\end{teor}
\begin{proof}
\begin{equation}
\begin{split}
&\lim_{x\rightarrow a}\left[E(x)\cdot f(x)\right]=
\lim_{x\rightarrow a}\left[E(x)\cdot f(x)\frac{p(x)}{p(x)}\right]=
\lim_{x\rightarrow a}\left[E(x)\cdot p(x)\frac{f(x)}{p(x)}\right]=\\
&\lim_{x\rightarrow a}\left[E(x)\cdot p(x)\right]\cdot
\lim_{x\rightarrow a}\frac{f(x)}{p(x)}=
\lim_{x\rightarrow a}\left[E(x)\cdot p(x)\right]
\end{split}
\end{equation}
al ser $\lim_{x\rightarrow a}f(x)/p(x)=1$ por ser infinit\'esimos equivalentes
cuando $x\rightarrow a$. La demostraci\'on es an\'aloga si los infinit\'esimos
lo son cuando $x\rightarrow\pm\infty$.
\end{proof}
