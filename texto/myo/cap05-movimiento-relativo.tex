\chapter{Movimiento relativo}
\label{cap:MR}

\section{Sistemas de referencia fijos y m\'oviles}

En el estudio realizado hasta ahora del movimiento del punto material y del
s\'olido r\'igido (cap\'itulos~\ref{cap:CPM} y~\ref{cap:CSR}) se supon\'ia que
el sistema de coordenadas tomado como referencia estaba fijo en el espacio, es
decir, en reposo absoluto. En este cap\'itulo vamos a describir el movimiento
con respecto a dos sistemas de referencia distintos: uno en reposo absoluto y
otro m\'ovil, con un desplazamiento conocido con respecto al del sistema fijo
llamado \textit{movimiento de arrastre}.\index{movimiento!de arrastre}

Supongamos un sistema cartesiano de referencia fijo $(O,X,Y,Z)$, cuyos vectores
unitarios sean $(\vec{I},\vec{J},\vec{K})$, y otro sistema cartesiano m\'ovil
$(o,x,y,z)$, de vectores unitarios $(\vec{i},\vec{j},\vec{k})$, tal y como se
muestra en \lafigura~\ref{fig:cap5-01-sist-cart-mov-rel}. La posici\'on de un
objeto m\'ovil, $P$, por ejemplo, con respecto al sistema fijo estar\'a
determinada por medio de un vector de posici\'on
$\vec{R}_P=X\,\vec{I}+Y\,\vec{J}+Z\,\vec{K}$. Al variar el tiempo el extremo de
dicho vector el punto $P$ describir\'a una curva referida al sistema fijo
denominada \textit{trayectoria absoluta}.\index{trayectoria!absoluta} La
velocidad y la aceleraci\'on de $P$ en cada instante con respecto a este sistema
(esto es, para un observador situado en $O$) ser\'an, respectivamente, la
\textit{velocidad} y \textit{aceleraci\'on absolutas},\index{velocidad!absoluta}
\index{aceleraci\'on!absoluta} dadas por las expresiones $d\vec{R}_P/dt$ y
$d^2\vec{R}_P/dt^2$. Por todo ello, el movimiento que el punto describe con
respecto al sistema fijo se denomina \textit{movimiento absoluto}.
\index{movimiento!absoluto}
\begin{figure}[htb]
\centering
\includegraphics[width=0.70\textwidth]{fig-cap5-01-sist-cart-mov-rel}
\caption{Sistemas de referencia fijo y m\'ovil.}
\label{fig:cap5-01-sist-cart-mov-rel}
\end{figure}

La posici\'on del punto $P$ en un instante de tiempo con respecto al sistema
m\'ovil vendr\'a descrita por el vector
$\vec{r}_\vc{p}=x\,\vec{i}+y\,\vec{j}+z\,\vec{k}$. El extremo de
$\vec{r}_\vc{p}$ describir\'a al transcurrir el tiempo una curva llamada
\textit{trayectoria relativa},\index{trayectoria!relativa} mientras que la
velocidad y la aceleraci\'on referidas a este sistema m\'ovil reciben el nombre
de \textit{velocidad} y
\textit{aceleraci\'on relativas},\index{velocidad!relativa}
que\index{aceleraci\'on!relativa} estar\'an determinadas por las ecuaciones
\begin{equation}
\begin{dcases}
\left(\frac{d\vec{r}_\vc{p}}{dt}\right)_{(\vec{i},\vec{j},\vec{k})=\text{cte.}}=
\frac{dx}{dt}\,\vec{i}+\frac{dy}{dt}\,\vec{j}+\frac{dz}{dt}\,\vec{k},\\
\left(\frac{d^2\vec{r}_\vc{p}}{dt^2}\right)_{(\vec{i},\vec{j},\vec{k})=\text{cte.}}=
\frac{d^2x}{dt^2}\,\vec{i}+\frac{d^2y}{dt^2}\,\vec{j}+\frac{d^2z}{dt^2}\,\vec{k},
\end{dcases}
\end{equation}
donde es importante recalcar que el sub\'indice
$(\vec{i},\vec{j},\vec{k})=\text{cte.}$ indica que los ejes $(x,y,z)$ del
sistema m\'ovil han de considerarse \textbf{fijos} en los c\'alculos. El
movimiento que $P$ describe en estas circunstancias se llama \textit{movimiento
relativo}.\index{movimiento!relativo}

Por \'ultimo, si consideramos que el punto $P$ est\'a ligado al sistema m\'ovil
$(o,x,y,z)$, es decir, que sus coordenadas $(x_\vc{p},y_\vc{p},z_\vc{p})$ son
constantes en el transcurso del tiempo, $P$ \textbf{estar\'a en reposo} con
respecto a dicho sistema m\'ovil (as\'i lo ver\'a un observador situado en $o$),
pero se desplazar\'a con respecto al sistema fijo $(O,X,Y,Z)$ con el llamado
\textit{movimiento de arrastre},\index{movimiento!de arrastre} siendo su
trayectoria, velocidad y aceleraci\'on en este caso tambi\'en de arrastre.
\index{trayectoria!de arrastre}\index{velocidad!de arrastre}
\index{aceleraci\'on!de arrastre}

De \lafigura~\ref{fig:cap5-01-sist-cart-mov-rel}, para un instante de tiempo y
llamando $\vec{R}_o=X_o\,\vec{I}+Y_o\,\vec{J}+Z_o\,\vec{K}$ al vector de
posici\'on del origen del sistema m\'ovil con respecto al fijo, $\vec{R}_P$ al
vector de posici\'on de $P$ tambi\'en con respecto al sistema fijo y
$\vec{r}_\vc{p}$ al vector de posici\'on de $P$ con respecto al sistema m\'ovil,
tendremos que se cumple la relaci\'on\footnote{\label{pie:NotaMatRot}En la
\ecuac~(\ref{ec:RpRorp}) se ha incurrido deliberadamente en un abuso de
notaci\'on, puesto que tal y como est\'a planteada se est\'an sumando vectores
referidos a dos sistemas distintos. Para realizar la suma en la pr\'actica uno
de los vectores ha de llevarse al sistema de coordenadas del otro mediante la
correspondiente matriz de rotaci\'on entre los dos sistemas para el instante de
trabajo. En lo que resta de cap\'itulo se trabajar\'a con los vectores (de
cualquier tipo, ya sea posici\'on, velocidad, etc.) en sus sistemas de
coordenadas particulares, omitiendo las transformaciones que, repetimos, son
\textbf{imprescindibles} a la hora de llevar a la pr\'actica la teor\'ia aqu\'i
expuesta.}
\begin{equation}
\label{ec:RpRorp}
\vec{R}_P=\vec{R}_o+\vec{r}_\vc{p},
\end{equation}
ecuaci\'on que indica que la trayectoria absoluta es igual a la suma de las
trayectorias de arrastre m\'as la relativa.

Rigurosamente no existe en la realidad ning\'un sistema de coordenadas fijo, por
lo que los movimientos observados son siempre relativos, al estar cualquier
sistema de referencia siempre en movimiento \citep[\pnbs67]{dejuana2003a}. El
sistema cartesiano tridimensional geoc\'entrico, por ejemplo, sigue los
movimientos de rotaci\'on y traslaci\'on de la Tierra, mientras que el sistema
helioc\'entrico, con origen en el centro de gravedad del Sol, tambi\'en est\'a
en movimiento por estarlo el Sistema Solar en nuestra galaxia.

\section{Velocidades en el movimiento relativo}

Considerando la expresi\'on~(\ref{ec:RpRorp}), al variar el tiempo el extremo de
$\vec{R}_P$ describe con relaci\'on al sistema fijo la trayectoria absoluta del
m\'ovil.\index{trayectoria!absoluta} La posici\'on de $P$ (el vector
$\vec{R}_P$) es funci\'on del tiempo a trav\'es de $\vec{R}_o$ y
$\vec{r}_\vc{p}$, por lo que la velocidad absoluta\index{velocidad!absoluta}
habr\'a que hallarla considerando esta dependencia. As\'i pues,
\begin{equation}
\label{ec:VpdRdr}
\vec{v}_\vc{a}^\vc{p}=\frac{d\vec{R}_P}{dt}=
\frac{d\vec{R}_o}{dt}+\frac{d\vec{r}_\vc{p}}{dt}=\frac{d\vec{R}_o}{dt}+
\frac{dx_\vc{p}}{dt}\,\vec{i}+\frac{dy_\vc{p}}{dt}\,\vec{j}+
\frac{dz_\vc{p}}{dt}\,\vec{k}+\frac{d\vec{i}}{dt}\,x_\vc{p}+
\frac{d\vec{j}}{dt}\,y_\vc{p}+\frac{d\vec{k}}{dt}\,z_\vc{p},
\end{equation}
donde el t\'ermino
\begin{equation}
\label{ec:velRel}
\vec{v}_r^\vc{p}=
\frac{dx_\vc{p}}{dt}\,\vec{i}+\frac{dy_\vc{p}}{dt}\,\vec{j}+
\frac{dz_\vc{p}}{dt}\,\vec{k}
\end{equation}
es la derivada del vector $\vec{r}_\vc{p}$ con respecto al tiempo considerando
los ejes $(\vec{i},\vec{j},\vec{k})$ fijos, es decir, el vector velocidad
relativa.\index{velocidad!relativa} Dicho vector es equivalente a
\begin{equation}
\vec{v}_r^\vc{p}=
\left(\frac{d\vec{R}_P}{dt}\right)_{(\vec{R}_o,\vec{i},\vec{j},\vec{k})=
\text{cte}.}
\end{equation}
El t\'ermino
\begin{equation}
\label{ec:velArrastre}
\vec{v}_a^\vc{p}=\frac{d\vec{R}_o}{dt}+
\frac{d\vec{i}}{dt}\,x_\vc{p}+\frac{d\vec{j}}{dt}\,y_\vc{p}+
\frac{d\vec{k}}{dt}\,z_\vc{p}
\end{equation}
es la velocidad de arrastre,\index{velocidad!de arrastre} que se compone de la
velocidad absoluta del origen del sistema m\'ovil m\'as la derivada de
$\vec{r}_\vc{p}$ con respecto al tiempo suponiendo que $P$ es fijo con respecto
al sistema m\'ovil. Los t\'erminos $d\vec{i}/dt$, $d\vec{j}/dt$ y $d\vec{k}/dt$
tienen en cuenta la variaci\'on en la orientaci\'on de los ejes del sistema
m\'ovil con respecto al fijo a lo largo del tiempo.

Por lo tanto, la \ecuac~(\ref{ec:VpdRdr}) queda
\begin{equation}
\label{ec:Vpvrva}
\vec{v}_\vc{a}^\vc{p}=\vec{v}_r^\vc{p}+\vec{v}_a^\vc{p},
\end{equation}
que es la \textit{ley de composici\'on de velocidades},
\index{ley!de composici\'on de velocidades} v\'alida, con un error despreciable,
para velocidades inferiores a un veinteavo de la velocidad de la luz en el
vac\'io \citep[\pnbs68]{dejuana2003a}.

La expresi\'on~(\ref{ec:velArrastre}), que nos da la velocidad de arrastre,
puede modificarse considerado el sistema m\'ovil como un s\'olido r\'igido
formado por los versores $(\vec{i},\vec{j},\vec{k})$ y el punto $P$, sometido a
una traslaci\'on y a una rotaci\'on alrededor de un eje que pasa por $o$ y de
velocidad angular $\vec*{\omega}$. Con base en la
\ecuac~(\ref{ec:vpSolRigRotGen}) podemos escribir
\begin{equation}
\begin{dcases}
\frac{d\vec{i}}{dt}=\vec*{\omega}\wedge\vec{i},\\
\frac{d\vec{j}}{dt}=\vec*{\omega}\wedge\vec{j},\\
\frac{d\vec{k}}{dt}=\vec*{\omega}\wedge\vec{k},
\end{dcases}
\end{equation}
lo que significa que dichas derivadas son las velocidades de los extremos de los
versores $\vec{i}$, $\vec{j}$ y $\vec{k}$ debidas a la rotaci\'on
$\vec*{\omega}$. La \ecuac~(\ref{ec:velArrastre}) queda, pues,
\begin{equation}
\label{ec:vaPvo}
\begin{split}
\vec{v}_a^\vc{p}&=\vec{v}_o+
(\vec*{\omega}\wedge\vec{i})\,x_\vc{p}+(\vec*{\omega}\wedge\vec{j})\,y_\vc{p}+
(\vec*{\omega}\wedge\vec{k})\,z_\vc{p} \\
&=\vec{v}_o+\vec*{\omega}\wedge(x_\vc{p}\,\vec{i}+y_\vc{p}\,\vec{j}+
z_\vc{p}\,\vec{k}) \\
&=\vec{v}_o+\vec*{\omega}\wedge\vec{r}_\vc{p},
\end{split}
\end{equation}
pudiendo escribirse la velocidad absoluta\index{velocidad!absoluta} de $P$ como
\begin{equation}
\label{ec:VelAbsolutaP}
\vec{v}_\vc{a}^\vc{p}=
\vec{v}_r^\vc{p}+\vec{v}_o+\vec*{\omega}\wedge\vec{r}_\vc{p}.
\end{equation}

Consideremos ahora el ejemplo, siguiendo a \citet[\pnbs69]{dejuana2003a}, de dos
part\'iculas, $P$ y $Q$, que se mueven con respecto a un sistema de referencia
fijo con velocidades absolutas $\vec{v}_\vc{a}^\vc{p}$ y $\vec{v}_\vc{a}^Q$,
respectivamente. Se trata de calcular la velocidad de $Q$ con respecto a la de
$P$, para lo cual consideraremos un sistema de referencia m\'ovil con origen en
$P$ y cuyos ejes se conservan \textbf{paralelos} a los del sistema
fijo\footnote{En el caso de que los ejes del sistema m\'ovil se conserven
paralelos a los del fijo y con su mismo sentido, no es necesario tener en cuenta
lo indicado en la nota~\footref{pie:NotaMatRot} de la
p\'agina~\pageref{pie:NotaMatRot} con respecto a la necesidad de transformar los
vectores de un  sistema de referencia a otro en los c\'alculos.}. En este caso
tendremos que $\vec*{\omega}=0$, por los que la \ecuac~(\ref{ec:VelAbsolutaP})
adopta, para $P$ y $Q$, la forma
\begin{equation}
\begin{cases}
\vec{v}_\vc{a}^\vc{p}=\vec{v}_r^\vc{p}+\vec{v}_o,\\
\vec{v}_\vc{a}^Q=\vec{v}_r^Q+\vec{v}_o,
\end{cases}
\end{equation}
de donde se deduce, igualando $\vec{v}_o$, que
\begin{equation}
\vec{v}_\vc{a}^\vc{p}-\vec{v}_r^\vc{p}=\vec{v}_\vc{a}^Q-\vec{v}_r^Q\longrightarrow
\vec{v}_r^Q-\vec{v}_r^\vc{p}=\vec{v}_\vc{a}^Q-\vec{v}_\vc{a}^\vc{p}\longrightarrow
\vec{v}_r^{Q|P}=\vec{v}_\vc{a}^Q-\vec{v}_\vc{a}^\vc{p},
\end{equation}
es decir, que la velocidad relativa de $Q$ con respecto a $P$ es la diferencia
entre las velocidades absolutas de ambas part\'iculas.

\section{Aceleraciones en el movimiento relativo}

Siguiendo con el ejemplo de \lafigura~(\ref{fig:cap5-01-sist-cart-mov-rel})
planteemos la ecuaci\'on de la aceleraci\'on absoluta
\index{aceleraci\'on!absoluta} de $P$ para un instante determinado, que se
obtendr\'a derivando la ecuaci\'on de la velocidad absoluta
\index{velocidad!absoluta} del punto (expresiones~(\ref{ec:Vpvrva})
o~(\ref{ec:VelAbsolutaP})) con respecto al tiempo. Tendremos, entonces,
\begin{equation}
\label{ec:aAGeneral}
\vec{a}_\vc{a}^\vc{p}=\frac{d\vec{v}_\vc{a}^\vc{p}}{dt}=
\frac{d\vec{v}_r^\vc{p}}{dt}+\frac{d\vec{v}_a^\vc{p}}{dt}=
\frac{d\vec{v}_r^\vc{p}}{dt}+\frac{d\vec{v}_o^\vc{p}}{dt}+
\frac{d}{dt}\left(\vec*{\omega}\wedge\vec{r}_\vc{p}\right).
\end{equation}

El primer sumando de la \ecuac~(\ref{ec:aAGeneral}), que es la
expresi\'on~(\ref{ec:velRel}), se desarrolla como
\begin{equation}
\label{ec:dvrp}
\begin{split}
\frac{d\vec{v}_r^\vc{p}}{dt}&=\frac{d^2x_\vc{p}}{dt^2}\,\vec{i}+
\frac{d^2y_\vc{p}}{dt^2}\,\vec{j}+\frac{d^2z_\vc{p}}{dt^2}\,\vec{k}+
\frac{dx_\vc{p}}{dt}\,\frac{d\vec{i}}{dt}+
\frac{dy_\vc{p}}{dt}\,\frac{d\vec{j}}{dt}+
\frac{dz_\vc{p}}{dt}\,\frac{d\vec{k}}{dt} \\
&=\vec{a}_r^\vc{p}+\frac{dx_\vc{p}}{dt}(\vec*{\omega}\wedge\vec{i})+
\frac{dy_\vc{p}}{dt}(\vec*{\omega}\wedge\vec{j})+
\frac{dz_\vc{p}}{dt}(\vec*{\omega}\wedge\vec{k}) \\
&=\vec{a}_r^\vc{p}+\vec*{\omega}\wedge(\frac{dx_\vc{p}}{dt}\,\vec{i}+
\frac{dy_\vc{p}}{dt}\,\vec{j}+\frac{dz_\vc{p}}{dt}\,\vec{k})=
\vec{a}_r^\vc{p}+\vec*{\omega}\wedge\vec{v}_r^\vc{p},
\end{split}
\end{equation}
donde se tiene en cuenta que la orientaci\'on del sistema m\'ovil puede variar,
de ah\'i que se derive tambi\'en con respecto a $\vec{i}$, $\vec{j}$ y
$\vec{k}$, al igual que se hizo en el caso de la velocidad con la
\ecuac~(\ref{ec:VpdRdr}). El resultado tiene a su vez dos sumandos, al primero
de los cuales llamaremos aceleraci\'on relativa,\index{aceleraci\'on!relativa}
debida al cambio en la velocidad de $P$ con respecto al sistema m\'ovil,
mientras que el segundo es otra aceleraci\'on inducida por la variaci\'on de
dicho sistema m\'ovil en s\'i.

La derivada con respecto al tiempo de la velocidad de arrastre en la
\ecuac~(\ref{ec:aAGeneral}) tiene por expresi\'on
\begin{equation}
\label{ec:dvap}
\begin{split}
\frac{d\vec{v}_a^\vc{p}}{dt}&=
\frac{d\vec{v}_o^\vc{p}}{dt}+\frac{d}{dt}
\left(\vec*{\omega}\wedge\vec{r}_\vc{p}\right)\\
&=\frac{d^2X_o^P}{dt^2}\,\vec{I}+\frac{d^2Y_o^P}{dt^2}\,\vec{J}+
\frac{d^2Z_o^P}{dt^2}\,\vec{K}+\frac{d\vec*{\omega}}{dt}\wedge\vec{r}_\vc{p}+
\vec*{\omega}\wedge\frac{d\vec{r}_\vc{p}}{dt} \\
&=\vec{a}_o+\frac{d\vec*{\omega}}{dt}\wedge\vec{r}_\vc{p}+
\vec*{\omega}\wedge\vec{v}_r^\vc{p}+
\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p}),
\end{split}
\end{equation}
donde el valor del t\'ermino
$d\vec{r}_\vc{p}/dt=\vec{v}_r^\vc{p}+\vec*{\omega}\wedge\vec{r}_\vc{p}$ se
deduce a partir de las \ecuacs~(\ref{ec:VpdRdr}) a~(\ref{ec:vaPvo}). Definiendo
el vector aceleraci\'on de arrastre\index{aceleraci\'on!de arrastre} como
\begin{equation}
\vec{a}_a^\vc{p}=\vec{a}_o+\frac{d\vec*{\omega}}{dt}\wedge\vec{r}_\vc{p}+
\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p}),
\end{equation}
es decir, como la derivada de $\vec{v}_a^\vc{p}$ con respecto al tiempo
considerando los ejes $(x,y,z)$ del sistema m\'ovil constantes, la
\ecuac~(\ref{ec:dvap}) tiene por expresi\'on
\begin{equation}
\frac{d\vec{v}_a^\vc{p}}{dt}=
\vec{a}_a^\vc{p}+\vec*{\omega}\wedge\vec{v}_r^\vc{p},
\end{equation}
que junto con la relaci\'on~(\ref{ec:dvrp}) lleva a que la
expresi\'on~(\ref{ec:aAGeneral}) para la aceleraci\'on absoluta
\index{aceleraci\'on!absoluta} se pueda escribir como
\begin{equation}
\label{ec:AcelAbsolutaP}
\vec{a}_\vc{a}^\vc{p}=\vec{a}_r^\vc{p}+\vec{a}_a^\vc{p}+2\vec*{\omega}\wedge
\vec{v}_r^\vc{p}=\vec{a}_r^\vc{p}+\vec{a}_a^\vc{p}+\vec{a}_c^\vc{p},
\end{equation}
donde el t\'ermino $\vec{a}_c^\vc{p}=2\vec*{\omega}\wedge\vec{v}_r^\vc{p}$ se
denomina \textit{aceleraci\'on de
Coriolis}\footnote{\href{\biomate{Coriolis}}
{Gaspard Gustave de Coriolis (1792--1843)}.}
\indnp{Coriolis, Gaspard Gustave de}\index{aceleraci\'on!de Coriolis} o
\textit{complementaria}\index{aceleraci\'on!complementaria|see{de Coriolis}}.
Dicha aceleraci\'on ser\'a nula si el sistema m\'ovil no rota, si no existe
movimiento relativo por parte del m\'ovil o si, existiendo ambos, los vectores
$\vec*{\omega}$ y $\vec{v}_r^\vc{p}$ son paralelos. La aceleraci\'on de Coriolis
aparece, por ejemplo, cuando se mide la aceleraci\'on de la gravedad en la
Tierra desde un veh\'iculo en movimiento, efecto hay que corregir de las medidas
realizadas, que estar\'an afectadas por $\vec{a}_c^\vc{p}$.

\section{Movimiento de arrastre de traslaci\'on}
\label{sec:MAT}

Si el movimiento del sistema $(o,x,y,z)$ es una traslaci\'on pura las
expresiones deducidas hasta ahora se simplifican notablemente, ya que tendremos
que $\vec*{\omega}=0$. Las \ecuacs~(\ref{ec:VelAbsolutaP})
y~(\ref{ec:AcelAbsolutaP}) se convierten en
\index{velocidad!absoluta}\index{aceleraci\'on!absoluta}
\begin{equation}
\begin{dcases}
\vec{v}_\vc{a}^\vc{p}=\vec{v}_r^\vc{p}+\vec{v}_o,\\
\vec{a}_\vc{a}^\vc{p}=\vec{a}_r^\vc{p}+\vec{a}_o.
\end{dcases}
\end{equation}

Si el movimiento de $(o,x,y,z)$ es una traslaci\'on pura debida a un movimiento
rectil\'ineo uniforme tendremos, adem\'as de $\vec*{\omega}=0$, que
$\vec{v}_o=\text{cte.}$ y $\vec{a}_o=0$, cumpli\'endose que la aceleraci\'on
absoluta coincide con la relativa:
\index{velocidad!absoluta}\index{aceleraci\'on!absoluta}
\begin{equation}
\label{ec:vAPaAP}
\begin{dcases}
\vec{v}_\vc{a}^\vc{p}=\vec{v}_r^\vc{p}+\vec{v}_o,\\
\vec{a}_\vc{a}^\vc{p}=\vec{a}_r^\vc{p}.
\end{dcases}
\end{equation}
La aceleraci\'on es invariante al pasar de un sistema de referencia considerado
fijo a otro que tiene con respecto a \'el un movimiento de traslaci\'on
rectil\'ineo y uniforme. Tales sistemas son conocidos como \textit{sistemas de
referencia inerciales}.\index{sistema de referencia inercial} Citemos a
\citet[\pnbs1320]{tipler2012c}:
\begin{quote}
Un sistema de referencia en el que son v\'alidas las leyes de Newton se denomina
\textit{sistema de referencia inercial}. Todos los sistemas de referencia que se
mueven con velocidad constante respecto a un sistema de referencia inercial son
tambi\'en sistemas de referencia inerciales. Si tenemos dos sistemas de
referencia inerciales que se mueven con velocidad constante uno respecto al
otro, no existe ning\'un experimento mec\'anico que pueda decirnos cu\'al est\'a
en reposo y cu\'al est\'a en movimiento, o si ambos est\'an movi\'endose.
\end{quote}
De otro modo, si sobre un objeto no act\'ua ninguna fuerza, cualquier sistema de
referencia con respecto al cual la aceleraci\'on del objeto es cero es un
sistema de referencia inercial \citep[\pnbs94]{tipler2010a}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.70\textwidth]{fig-cap5-02-mov-rel-galileo}
\caption{Sistemas de referencia fijo y m\'ovil, este \'ultimo desplazandose con
         movimiento rectil\'ineo uniforme y sin rotaci\'on.}
\label{fig:cap5-02-mov-rel-galileo}
\end{figure}

Estudiemos ahora el movimiento del punto $P$ en estas circunstancias
($\vec*{\omega}=0$, $\vec{v}_o=\text{cte.}$ y $\vec{a}_o=0$) para un observador
situado en el origen del sistema fijo, $O$, y para otro situado en el origen del
sistema m\'ovil, $o$. Supongamos que en el instante inicial $t=0$ el sistema
m\'ovil est\'a situado con respecto al fijo seg\'un el vector
$\vec{R}_i=X_i\,\vec{I}+Y_i\,\vec{J}+Z_i\,\vec{K}$, y se desplaza con velocidad
$\vec{v}_o=v_X\,\vec{I}+v_Y\,\vec{J}+v_Z\,\vec{K}$ (ver
\figura~\ref{fig:cap5-02-mov-rel-galileo}). Con el transcurso del tiempo los
ejes del sistema m\'ovil se trasladar\'an, permaneciendo sus ejes paralelos a
los del sistema fijo. En el instante $t$ el origen del sistema m\'ovil tendr\'a
por vector de posici\'on $\vec{R}_o=\vec{R}_i+\vec{v}_o\,t$. Si consideramos
ahora el movimiento del punto $P$ su trayectoria absoluta ser\'a
\begin{equation}
\label{ec:TrayRPAbsMRU}
\vec{R}_P=\vec{R}_o+\vec{r}_\vc{p}=\vec{R}_i+\vec{v}_o\,t+\vec{r}_\vc{p}=
\begin{dcases}
X_P=X_i+v_X\,t+x_\vc{p},\\
Y_P=Y_i+v_Y\,t+y_\vc{p},\\
Z_P=Z_i+v_Z\,t+z_\vc{p},
\end{dcases}
\end{equation}
mientras que la relativa se expresar\'a como
\begin{equation}
\label{ec:TrayrPAbsMRU}
\vec{r}_\vc{p}=
\begin{dcases}
x_\vc{p}=X_P-X_i-v_X\,t,\\
y_\vc{p}=Y_P-Y_i-v_Y\,t,\\
z_\vc{p}=Z_P-Z_i-v_Z\,t.
\end{dcases}
\end{equation}
El tiempo \textbf{es independiente} de que el observador se encuentre en reposo
o en movimiento, constituyendo las \ecuacs~(\ref{ec:TrayRPAbsMRU})
y~(\ref{ec:TrayrPAbsMRU}) la denominada \textit{transformaci\'on de
Galileo}\footnote{\href{\biomate{Galileo.html}}{Galileo Galilei (1564--1642)}.}.
\index{transformaci\'on de Galileo}\indnp{Galileo Galilei} En cuanto a las
velocidades y aceleraciones de $P$, se relacionan seg\'un la
\ecuac~(\ref{ec:vAPaAP}), como ha quedado indicado. Citando de
\citet[\pnbs74]{dejuana2003a}:
\begin{quote}
La aceleraci\'on de una part\'icula es la misma en todos los sistemas de
referencia con movimiento relativo de traslaci\'on uniforme. Este
\textit{principio de relatividad de Galileo}
\index{principio!de relatividad de Galileo} fue generalizado por
Einstein\footnote{\href{\biomate{Einstein.html}}{Albert Einstein (1879--1955)}.}
\indnp{Einstein, Albert} al postular que todas las leyes de la naturaleza son
las mismas para todos los observadores en movimiento relativo de traslaci\'on
uniforme, lo que constituye la base de la F\'isica relativista.
\end{quote}
La transformaci\'on de Galileo \textbf{no} es consistente con los postulados de
Einstein de la relatividad especial y s\'olo es v\'alida para $v\ll c$, con $c$
la velocidad de la luz en el vac\'io.

\section{Movimiento de arrastre rotacional}

Supongamos ahora un sistema m\'ovil que no tiene traslaci\'on con respecto a uno
fijo, sino que s\'olo se ve sometido a una rotaci\'on $\vec*{\omega}$ en torno a
un eje arbitrario $e$ (ver \figura~\ref{fig:cap5-03-mov-rel-at-an}). En este
caso, al no haber traslaci\'on del sistema m\'ovil la velocidad de arrastre de
un punto $P$ (\ecuac~(\ref{ec:vaPvo})) se ver\'a reducida a
\index{velocidad!de arrastre}
\begin{equation}
\vec{v}_a^\vc{p}=\vec*{\omega}\wedge\vec{r}_\vc{p},
\end{equation}
expresi\'on correspondiente a la velocidad de una part\'icula que realiza un
movimiento circular en torno al eje $e$. En cada instante $t$ de tiempo el
m\'odulo de dicha velocidad ser\'a
\begin{equation}
v_a^\vc{p}(t)=\omega(t)\,\rho,
\end{equation}
donde $\rho$ es el radio de la circunferencia. La velocidad absoluta de $P$
ser\'a (\ecuac~(\ref{ec:VelAbsolutaP})\index{velocidad!absoluta}
\begin{equation}
\label{ec:VelAbsRot}
\vec{v}_\vc{a}^\vc{p}=\vec{v}_r+\vec*{\omega}\wedge\vec{r}_\vc{p}.
\end{equation}
\begin{figure}[htb]
\centering
\includegraphics[width=0.68\textwidth]{fig-cap5-03-mov-rel-at-an}
\caption{Sistemas de referencia fijo y m\'ovil, este \'ultimo rotando con
         velocidad angular $\vec*{\omega}(t)$ y sin desplazamiento.}
\label{fig:cap5-03-mov-rel-at-an}
\end{figure}

En cuanto a la aceleraci\'on, la expresi\'on general~(\ref{ec:AcelAbsolutaP}) se
reduce a\index{aceleraci\'on!absoluta}
\begin{equation}
\vec{a}_\vc{a}^\vc{p}=\vec{a}_r^\vc{p}+
\underbrace{\frac{d\vec*{\omega}}{dt}\wedge\vec{r}_\vc{p}+\vec*{\omega}\wedge
(\vec*{\omega}\wedge\vec{r}_\vc{p})}_{\vec{a}_a^\vc{p}}+
\underbrace{2\vec*{\omega}\wedge\vec{v}_r^\vc{p}}_{\vec{a}_c^\vc{p}},
\end{equation}
ya que $\vec{a}_o=0$ al ser $\vec{v}_o=0$ (a esta expresi\'on se puede llegar
tambi\'en derivando la \ecuac~(\ref{ec:VelAbsRot}) con respecto al tiempo).
Vemos que hay una componente relativa, $\vec{a}_r^\vc{p}$, una correspondiente a
la aceleraci\'on de Coriolis,
\index{aceleraci\'on!de Coriolis}
$\vec{a}_c^\vc{p}=2\vec*{\omega}\wedge\vec{v}_r^\vc{p}$, que ser\'a distinta de
cero s\'olo si el punto $P$ est\'a en movimiento con respecto al sistema
m\'ovil, y la componente de arrastre, que tendr\'a en este caso por
expresi\'on\index{aceleraci\'on!de arrastre}
\begin{equation}
\vec{a}_a^\vc{p}=\frac{d\vec*{\omega}}{dt}\wedge\vec{r}_\vc{p}+
\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p})=\vec{a}_t+\vec{a}_n=
\vec*{\alpha}\wedge\vec*{\rho}-\omega^2\,\vec*{\rho},
\end{equation}
ecuaci\'on equivalente a la expresi\'on~(\ref{ec:aQatan}), como veremos. El
primer sumando de esta aceleraci\'on de arrastre es la componente tangencial de
la aceleraci\'on en el movimiento circular que describe $P$,
\index{aceleraci\'on!componente tangencial} mientras el segundo es la componente
normal. Dado que $\vec*{\omega}$ es un vector deslizante el producto vectorial
$d\vec*{\omega}/dt\wedge\vec{r}_\vc{p}$ es equivalente a
$\vec*{\alpha}\wedge\vec*{\rho}$, donde se ha tenido en cuenta el concepto de
aceleraci\'on angular $\vec*{\alpha}=d\vec*{\omega}/dt$ y que $\vec*{\rho}$ es
el radio vector de la circunferencia que describe $P$. Esta ecuaci\'on coincide
en m\'odulo con la expresi\'on~(\ref{ec:acelTangMCUV}), que da el m\'odulo de la
aceleraci\'on tangencial en el movimiento circular uniformemente variado.
\index{movimiento!circular uniformemente variado} En cuanto a la componente
normal,\index{aceleraci\'on!componente normal} el doble producto vectorial
$\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p})$ puede escribirse como
$\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec*{\rho})$ si aplicamos
$\vec*{\omega}$ en el punto de intersecci\'on entre el plano que contiene a la
circunferencia que genera $P$ y el eje de giro. Entonces, usando las
\ecuacs~(\ref{ec:DoblePVmn}) y~(\ref{ec:mnDoblePV}) tendremos que
$\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec*{\rho})=
(\vec*{\omega}\cdot\vec*{\rho})\,\vec*{\omega}-
(\vec*{\omega}\cdot\vec*{\omega})\,\vec*{\rho}=-\omega^2\,\vec*{\rho}$, donde
$(\vec*{\omega}\cdot\vec*{\rho})\,\vec*{\omega}=0$ por ser $\vec*{\omega}$ y
$\vec*{\rho}$ perpendiculares. Tenemos, pues, que el vector aceleraci\'on normal
para un instante concreto de tiempo es $-\omega^2\,\vec*{\rho}$, donde el signo
menos indica que el vector tiene sentido contrario al definido por $\vec*{\rho}$,
que apunta del centro al exterior de la circunferencia. El m\'odulo es
$\omega^2\,\rho$, ecuaci\'on equivalente al m\'odulo de la segunda ecuaci\'on
de~(\ref{ec:atanmruv}). Todo lo hasta aqu\'i explicado acerca de las componentes
normal y tangencial de la aceleraci\'on de arrastre se muestra en
\lafigura~\ref{fig:cap5-03-mov-rel-at-an}.

Si $\omega=\text{cte.}$ la rotaci\'on del sistema m\'ovil ser\'a uniforme y el
movimiento de arrastre ser\'a a su vez una rotaci\'on uniforme en torno al eje
$e$ de velocidad\index{velocidad!de arrastre}
\begin{equation}
\vec{v}_a^\vc{p}=\vec*{\omega}\wedge\vec{r}_\vc{p},
\end{equation}
cuyo m\'odulo ser\'a constante e igual a
\begin{equation}
v_a^\vc{p}=\omega\,\rho,
\end{equation}
La ecuaci\'on que da la velocidad absoluta de $P$ es equivalente a la
expresi\'on~(\ref{ec:VelAbsRot}).\index{velocidad!absoluta} Al ser en este caso
$d\vec*{\omega}/dt=0$, la aceleraci\'on absoluta\index{aceleraci\'on!absoluta}
de $P$ responder\'a a la ecuaci\'on
\begin{equation}
\label{ec:aApWcte}
\vec{a}_\vc{a}^\vc{p}=\vec{a}_r^\vc{p}+
\underbrace{\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p})}_{\vec{a}_a^\vc{p}}+
\underbrace{2\vec*{\omega}\wedge\vec{v}_r^\vc{p}}_{\vec{a}_c^\vc{p}},
\end{equation}
de nuevo con componentes relativa, de Coriolis\index{aceleraci\'on!de Coriolis}
y de arrastre, la \'ultima de expresi\'on
\begin{equation}
\vec{a}_a^\vc{p}=\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p})=
-\omega^2\,\vec*{\rho}=\vec{a}_n,
\end{equation}
con componente s\'olo normal, como corresponde al movimiento circular uniforme
descrito por $P$.\index{movimiento!circular uniforme}

\section{Aceleraciones de un punto en la superficie de la Tierra}

A partir de la \ecuac~(\ref{ec:aApWcte}) estudiaremos\footnote{En esta secci\'on
seguimos en gran medida a \citet[\ppnbs71 a\nbs74]{alonso1995}.} las
aceleraciones a las que est\'a sometido un punto $P$ situado sobre la superficie
de la Tierra, considerando un sistema de coordenadas m\'ovil ligado a \'esta con
origen en su centro de masas, cuyos ejes $x$ e $y$ est\'an contenidos en el
plano del ecuador y con eje $z$ coincidente con el eje de rotaci\'on de nuestro
planeta.

Para un observador que gire con el sistema descrito la aceleraci\'on relativa
de $P$ se deduce a partir de la \ecuac~(\ref{ec:aApWcte}), correspondiente a la
aceleraci\'on absoluta percibida por un observador situado en un sistema de
coordenadas fijo, y deriva en la expresi\'on
\begin{equation}
\vec{a}_r^\vc{p}=
\vec{a}_\vc{a}^\vc{p}-\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p})-
2\vec*{\omega}\wedge\vec{v}_r^\vc{p},
\end{equation}
donde la componente $\vec{a}_\vc{a}^\vc{p}$ ser\'a el vector de la gravedad
$\vec{g}_0$ que medir\'ia un observador que \textbf{no} girase con la Tierra y
situado en $P$, lo que es equivalente a decir que $\vec{g}_0$ es debida
\'unicamente a la atracci\'on de la masa terrestre (ver
\figura~\ref{fig:cap5-04-f-centrifuga}, donde se considera un modelo de Tierra
esf\'erica y homog\'enea, por lo que $\vec{g}_0$ est\'a dirigida hacia su centro
geom\'etrico). Vemos que las componentes de arrastre y de
Coriolis\index{aceleraci\'on!de Coriolis} tienen ahora signo negativo, lo que
indica que el punto $P$ detecta estas aceleraciones con un sentido opuesto al
que las ver\'ia un observador externo (\ecuac~(\ref{ec:aApWcte})).

Cuando el observador se sit\'ua en el sistema m\'ovil la componente de arrastre
recibe el nombre de \textit{aceleraci\'on centr\'ifuga},
\index{aceleraci\'on!centr\'ifuga} ya que \'el la percibe como un vector que lo
aleja del eje de rotaci\'on\footnote{Un observador situado en el polo no se
ver\'a sometido a aceleraci\'on centr\'ifuga, mientras que en el ecuador
experimentar\'ia su valor m\'aximo al estar posicionado en el punto m\'as
alejado posible del eje de rotaci\'on.}. En el caso que estamos estudiando, y
considerando por el momento que el punto $P$ no tiene movimiento relativo
alguno, motivo por el cual la aceleraci\'on de Coriolis ser\'a cero, un
observador en $P$ obtendr\'a un valor de la gravedad en valor absoluto menor que
el que detectar\'ia si la Tierra no girase. Vectorialmente, la gravedad
realmente observada ser\'a el vector resultado de la suma de $\vec{g}_0$ m\'as
la aceleraci\'on centr\'ifuga, tal y como se puede apreciar en
\lafigura~\ref{fig:cap5-04-f-centrifuga}. La gravedad observada responder\'a,
entonces, a la expresi\'on
\begin{equation}
\vec{g}=\vec{g}_0-\vec*{\omega}\wedge(\vec*{\omega}\wedge\vec{r}_\vc{p}).
\end{equation}
Vemos que el vector $\vec{g}$ no s\'olo var\'ia en m\'odulo con respecto a
$\vec{g}_0$, sino que tambi\'en lo hace en direcci\'on, si bien el \'angulo
entre ambos es muy peque\~no.
\begin{figure}[htb]
\centering
\includegraphics[width=0.76\textwidth]{fig-cap5-04-f-centrifuga}
\caption{Aceleraciones de la gravedad y centr\'ifuga (izquierda) y
         descomposici\'on de esta \'ultima seg\'un el horizonte local del lugar
         (derecha) para un modelo de Terra esf\'erica y homog\'enea. Se
         considera que el observador no se desplaza, por lo que la aceleraci\'on
         de Coriolis es nula.}
\label{fig:cap5-04-f-centrifuga}
\end{figure}

El vector aceleraci\'on centr\'ifuga \index{aceleraci\'on!centr\'ifuga} puede
descomponerse seg\'un un sistema de coordenadas local a $P$ con eje $x'$
tangente a la superficie terrestre y en direcci\'on norte-sur, y eje $y'$
seg\'un el radio vector del punto (ver \figura~\ref{fig:cap5-04-f-centrifuga},
derecha). El m\'odulo de la proyecci\'on del vector aceleraci\'on centr\'ifuga
sobre el eje $y'$ del sistema de coordenadas local descrito es
\begin{equation}
a_{cf}^\vc{p}=\omega^2\,r_\vc{p}\,\cos^2\varphi=\omega^2\,d\,\cos\varphi,
\end{equation}
donde $\varphi$ es la latitud del lugar y $d$ el radio del paralelo de $P$. Por
tanto, el m\'odulo del vector de la gravedad observada ser\'a
\begin{equation}
g=g_0-\omega^2\,d\,\cos\varphi,
\end{equation}
donde se asume que el \'angulo entre $\vec{g}_0$ y $\vec{g}$ es tan peque\~no
que el valor de la proyecci\'on de $\vec{g}$ sobre el radio vector de $P$ es
pr\'acticamente igual al propio vector $\vec{g}$. La componente horizontal de la
aceleraci\'on centr\'ifuga apuntar\'a hacia el sur en el hemisferio norte y
hacia el norte en los puntos situados en el hemisferio sur.

Supongamos ahora que el observador en $P$ est\'a en movimiento con velocidad (su
m\'odulo) constante con respecto a la Tierra, caso que se da en las
observaciones gravim\'etricas realizadas desde barcos, aviones u otro tipo de
veh\'iculo (en realidad estos veh\'iculos pueden estar tambi\'en acelerados,
pero asumamos que su velocidad es constante). En este caso la aceleraci\'on de
Coriolis\index{aceleraci\'on!de Coriolis} ser\'a distinta de cero, por lo que
parte de su efecto tendr\'a una componente a lo largo de la vertical, es decir,
estar\'a contenido en la gravedad observada y habr\'a que corregirlo. Siguiendo
con nuestro modelo de Tierra esf\'erica y homog\'enea, el valor del m\'odulo del
vector gravedad corregido por el movimiento del observador con velocidad
constante ser\'a \citep[\pnbs271]{torge1989}
\begin{equation}
\label{ec:CorrEot}
g'=g+
\underbrace{2\omega\,v\,\cos\varphi\,\sin\alpha+
\frac{v^2}{r_\vc{p}}}_{\delta_\vc{e}},
\end{equation}
donde $g$ es la gravedad observada, $\omega$ la velocidad de rotaci\'on de la
Tierra, $v$ la velocidad del observador, $\varphi$ la latitud, $\alpha$ el
acimut de su trayectoria y $r_\vc{p}$ el radio vector geoc\'entrico del punto. El
t\'ermino $\delta_\vc{e}$ se conoce como \textit{correcci\'on de
E\"otv\"os}\footnote{\href{\biomate{Eotvos}}
{L\'or\'and E\"otv\"os (1848--1919)}.}.\indnp{E\"otv\"os, L\'or\'and}
\index{correcci\'on!de E\"otv\"os}

La \ecuac~(\ref{ec:CorrEot}) tiene en cuenta la componente vertical de la
aceleraci\'on de Coriolis,\index{aceleraci\'on!de Coriolis} pero \'esta tiene
tambi\'en una componente horizontal que puede ser detectada f\'acilmente.
Supongamos un p\'endulo oscilando inicialmente en direcci\'on este-oeste con
posici\'on inicial en el punto $A$ (ver \figura~\ref{fig:cap5-05-p-foucault}).
A causa de la componente horizontal de la aceleraci\'on de Coriolis
\index{aceleraci\'on!de Coriolis} el p\'endulo se desv\'ia en el hemisferio
norte constantemente hacia la derecha, motivo por el cual el punto de llegada de
la primera oscilaci\'on ser\'a el punto $B'$, en lugar del $B$, que ser\'ia el
destino si la Tierra no rotase. En las siguientes oscilaciones seguir\'a
desvi\'andose a la derecha y, por lo tanto, la trayectoria de las posiciones
extremas de la oscilaci\'on ser\'a una circunferencia recorrida en el sentido de
las agujas del reloj (en el hemisferio sur el desv\'io ser\'a hacia la izquierda
y la circunferencia se recorrer\'a en sentido contrario). El \'angulo de
rotaci\'on del plano de oscilaci\'on presenta una velocidad de
$\SI{15}{\degree}\,\sin\varphi$ cada hora, donde $\varphi$ es la latitud del
lugar. Este experimento fue llevado a cabo por el f\'isico franc\'es Jean
Foucault\footnote{\href{\biomate{Foucault}}
{Jean Bernard L\'eon Foucault (1819--1868)}.}
\indnp{Foucault, Jean Bernard L\'eon}
en $\si{1851}$ \citep[\pnbs74]{alonso1995}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.32\textwidth]{fig-cap5-05-p-foucault}
\caption{Componente horizontal de la aceleraci\'on de Coriolis y su efecto sobre
         un p\'endulo. Imagen adaptada de \citet[\pnbs74]{alonso1995}.}
\label{fig:cap5-05-p-foucault}
\end{figure}
