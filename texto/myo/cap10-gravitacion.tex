\chapter{Gravitaci\'on}
\label{cap:Grav}

\section{Leyes de Kepler}
\label{sec:LK}

La primera hip\'otesis relacionada con el movimiento de los planetas consisti\'o
en suponer que \'estos describ\'ian c\'irculos alrededor de la Tierra. Sin
embargo, este modelo no era capaz de explicar los movimientos observados de
estos astros, por lo que
Ptolomeo\footnote{\href{\biomate{Ptolemy}}{Claudio Ptolomeo (85--165)}.}
\indnp{Ptolomeo, Claudio} desarroll\'o un sistema en el que la Luna, el Sol y
los planetas orbitan en torno a la Tierra movidos por una serie de esferas
llamadas \textit{epiciclos}\index{epiciclo} y \textit{deferentes},
\index{deferente} y con unos puntos especiales llamados \textit{ecuantes}
\index{ecuante} (\'esta es la idea propiamente original de Ptolomeo, pues los
epiciclos y deferentes hab\'ian sido usados ya).

El modelo ptolemaico fue aceptado hasta el siglo \textsc{xvi}, cuando
Cop\'ernico\footnote{\href{\biomate{Copernicus}}
{Nicol\'as Cop\'ernico (1473--1543)}.}\indnp{Cop\'ernico, Nicol\'as (Miko\l{}aj Kopernik)}
puso de manifiesto que la descripci\'on del movimiento de los planetas se
simplificaba mucho si el astro que se supon\'ia fijo era el Sol. Bas\'andose en
la teor\'ia de Cop\'ernico y en las observaciones de
Tycho Brahe\footnote{\href{\biomate{Brahe}}{Tycho Brahe (1546--1601)}.},
\indnp{Brahe, Tycho} Johannes Kepler\indnp{Kepler, Johannes} descubri\'o las
leyes que rigen el movimiento orbital de los planetas, cuyo enunciado es el
siguiente:
\begin{enumerate}
\item \textit{Primera ley de Kepler}.\index{leyes de Kepler!primera ley} Los
      planetas describen \'orbitas el\'ipticas alrededor del Sol, estando \'este
      en uno de sus focos.\index{foco!de una c\'onica}
\item \textit{Segunda ley de Kepler}.\index{leyes de Kepler!segunda ley} El
      vector de posici\'on con respecto al Sol de cualquier planeta barre
      \'areas iguales en tiempos iguales. Por lo tanto, un planeta se mueve
      m\'as r\'apidamente cuando est\'a cerca del Sol que cuando est\'a lejos.
      Esta ley est\'a relacionada con el teorema de conservaci\'on del momento
      angular,\index{teorema!de conservaci\'on del momento angular} tal y como
      vimos en la secci\'on~\ref{sec:FC}.
\item \textit{Tercera ley de Kepler}.\index{leyes de Kepler!tercera ley} El
      cuadrado del per\'iodo de revoluci\'on de un planeta es directamente
      proporcional al cubo del semieje mayor de la elipse que define su
      \'orbita, luego
      \begin{equation}
      \label{ec:TercLeyKepler}
      T^2=c\,a^3\longrightarrow\frac{T^2}{a^3}=c=\text{cte.},
      \end{equation}
      donde $T$ es el per\'iodo, $a$ el semieje mayor de la elipse y $c$ es la
      constante de proporcionalidad, que es \textbf{la misma} para todos los
      planetas.
\end{enumerate}

Las leyes de Kepler son postulados puramente emp\'iricos que se corresponden con
una descripci\'on \'unicamente cinem\'atica del movimiento de los planetas, no
conteniendo ning\'un aspecto relativo a la din\'amica del mismo.

\section{Ley de la gravitaci\'on universal}

Bas\'andose en su segunda ley (ver p\'agina~\pageref{pag:LeyFundDinamica}), Newton
\indnp{Newton, Isaac} demostr\'o en $1686$ que una fuerza que var\'ia en raz\'on
inversa al cuadrado de la distancia entre el Sol y un planeta era la causa de
las \'orbitas observadas por Kepler,\indnp{Kepler, Johannes} y que tal fuerza
exist\'ia entre dos objetos cualesquiera del universo
\citep[\pnbs367]{tipler2010a}. Quedaba as\'i establecida la \textit{ley de la
gravitaci\'on universal},\index{ley!de la gravitaci\'on universal} que dice que
entre dos cuerpos cualquiera existe una fuerza de atracci\'on directamente
proporcional al producto de sus masas e inversamente proporcional al cuadrado de
la distancia que los separa. Matem\'aticamente se expresa del siguiente modo:
\begin{equation}
\label{ec:LeyGravNewton}
\vec{F}_{AB}=-G\frac{m_\vc{a}m_\vc{b}}{r^2}\vec{u}_\vc{a}^\vc{b},
\end{equation}
donde $\vec{F}_{AB}$ simboliza la fuerza que ejerce $A$ sobre $B$, $G$ es un
factor llamado \textit{constante de gravitaci\'on universal},
\index{constante!de gravitaci\'on universal} $m_\vc{a}$ y $m_\vc{b}$ son las
masas de los puntos $A$ y $B$, respectivamente, y $r$ es la distancia que los
separa. El vector unitario $\vec{u}_\vc{a}^\vc{b}$ apunta de $A$ a $B$.
\begin{figure}[htb]
\centering
\includegraphics[width=0.50\textwidth]{fig-cap10-01-ley-gravitacion}
\caption{Dos masas se atraen seg\'un la ley de la gravitaci\'on universal.}
\label{fig:cap10-01-ley-gravitacion}
\end{figure}

\Lafigura~\ref{fig:cap10-01-ley-gravitacion} muestra esquem\'aticamente la
situaci\'on de dos part\'iculas que se atraen seg\'un la ley de la gravitaci\'on
universal de Newton. Del mismo modo que hay una fuerza aplicada en $B$ con
sentido de $B$ a $A$, $\vec{F}_{AB}$ (provocada por $m_\vc{a}$), en virtud de la
tercera ley de Newton (ver p\'agina~\pageref{pag:TerLeyNewton}) existir\'a una
fuerza se igual m\'odulo y sentido opuesto aplicada en $A$: $\vec{F}_{BA}$
(provocada por $m_\vc{b}$). La fuerza de la gravedad es una fuerza central,
\index{fuerza!central} por lo que su direcci\'on de actuaci\'on coincide con la
l\'inea que une los centros de masas de los dos cuerpos implicados.

Como hemos indicado anteriormente, el factor $G$ de la
\ecuac~(\ref{ec:LeyGravNewton}) recibe el nombre de constante de gravitaci\'on
universal y su valor es
igual\footnote{\url{https://physics.nist.gov/cgi-bin/cuu/Value?bg\#mid}.} a
\begin{equation}
G=\SI{6.67430e-11}{\cubic\metre\per\kilogram\per\square\second}\text{ }
\left(\si{\newton\square\metre\per\square\kilogram}\right).
\end{equation}
La constante $G$ puede determinarse experimentalmente mediante el m\'etodo de
Cavendish\footnote{\href{https://es.wikipedia.org/wiki/Henry_Cavendish}
{Henry Cavendish (1731--1810)}.}.\indnp{Cavendish, Henry}

Es interesante tambi\'en comentar los conceptos de \textit{masa gravitacional}
\index{masa!gravitacional} y \textit{masa inercial}.\index{masa!inercial}
\index{masa!inerte|see{masa inercial}} La primera es la responsable de la fuerza
gravitacional que un cuerpo ejerce sobre otro, mientras que la segunda mide la
resistencia de un cuerpo a ser acelerado \citep[\pnbs370]{tipler2010a}. Para
determinar la relaci\'on entre ellas vamos a suponer que son distintas, llamando
$m_g$ a la masa gravitacional y $m$ a la inercial. El m\'odulo de la fuerza de
la gravedad que la Tierra ejerce sobre un objeto ser\'a, entonces,
\begin{equation}
\label{ec:FgMasGrav}
F_g=G\frac{M_{T_g}m_g}{r^2},
\end{equation}
mientras que, debido a la segunda ley de Newton,\indnp{Newton, Isaac} la
aceleraci\'on de ca\'ida de ese objeto ser\'a
\begin{equation}
\label{ec:aMasIn}
a=\frac{F_g}{m},
\end{equation}
donde en este caso la masa implicada es la inercial. Si sustituimos la
\ecuac~(\ref{ec:FgMasGrav}) en la expresi\'on~(\ref{ec:aMasIn}) obtenemos
\begin{equation}
a=G\frac{M_{T_g}}{r^2}\frac{m_g}{m}.
\end{equation}
Por experimentaci\'on se sabe que la aceleraci\'on que adquiere un cuerpo en su
ca\'ida (despreciando el rozamiento con el aire) es independiente de su masa y
que coincide con el t\'ermino $G\,M_{T_g}/r$, luego la relaci\'on $m_g/m$ ser\'a
igual a la unidad y $m_g=m$. Actualmente la equivalencia entre $m_g$ y
$m$ se encuentra establecida aproximadamente en una parte entre $\num{5e13}$
\citep[\pnbs370]{tipler2010a}.

\section{Campo gravitacional de una masa puntual}

Dada una masa puntual, \'esta genera un campo de fuerzas en el espacio que la
rodea tal que se pone de manifiesto al situar otra masa en sus proximidades. A
este campo lo llamamos \textit{campo gravitacional}.\index{campo!gravitacional}
La intensidad de campo ser\'a\footnote{Usamos aqu\'i la expresi\'on de la fuerza
en la forma $\vec{F}=-G\frac{m\,m'}{r^2}\,\vec{u}$.}, seg\'un lo dicho en la
p\'agina~\pageref{pag:DefIntCampo},
\begin{equation}
\label{ec:grav}
\vec{I}_C=\frac{\vec{F}}{m'}\longrightarrow\vec{g}=-G\frac{m}{r^2}\vec{u},
\end{equation}
donde a $\vec{g}$ la llamamos \textit{gravedad}
\index{gravedad|see{aceleraci\'on de la gravedad}} y que tiene por unidades
\index{aceleraci\'on!de la gravedad}
$\si[per-mode=reciprocal]{\newton\per\kilogram}\equiv
\si[per-mode=reciprocal]{\metre\per\square\second}$.  No obstante, hay que
aclarar que la intensidad del campo gravitacional es una magnitud din\'amica
(fuerza por unidad de masa) y no una magnitud cinem\'atica (aceleración). La
gravedad es una funci\'on vectorial\index{funci\'on!vectorial} que a cada punto
del espacio le asigna un vector definido por la \ecuac~(\ref{ec:grav}). Situada
una masa $m'$ en un punto, se ver\'a afectada por una fuerza igual a
\begin{equation}
\vec{F}=m'\vec{g}.
\end{equation}
Las l\'ineas que representan el campo gravitacional de una part\'icula nacen en
el infinito y son radiales y con sentido hacia la propia part\'icula, tal y como
se muestra en \lafigura~\ref{fig:cap10-02-campo-gravedad}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.57\textwidth]{fig-cap10-02-campo-gravedad}
\caption{Campo y potencial gravitacionales generado por una part\'icula de masa
         $m$.}
\label{fig:cap10-02-campo-gravedad}
\end{figure}

\section{Potencial gravitacional}

El campo gravitacional es conservativo, luego el trabajo realizado por la fuerza
que lo caracteriza es independiente del camino seguido. Dada una masa cualquiera
en el interior del campo, \'esta se ve afectada por una fuerza que la desplaza
de puntos de mayor energ\'ia a puntos de menor energ\'ia potencial, tal y como
se indic\'o en la p\'agina~\pageref{pag:EpDisminuye}.
\index{energ\'ia!potencial!gravitacional}

Supongamos el origen de la energ\'ia potencial en un punto con
$r\rightarrow\infty$, luego $E_{p_\infty}=0$. Entonces, y de acuerdo con la
\ecuac~(\ref{ec:WPInf}), el trabajo necesario para llevar una masa testigo desde
un punto arbitrario $A$ hasta el origen de energ\'ia potencial ser\'a
\begin{equation}
W_A^\infty=-\Delta E_p=E_{p_A}-E_{p_\infty}=E_{p_A}-0=E_{p_A}=
\int_A^\infty\vec{F}\cdot d\vec{r},
\end{equation}
luego
\begin{equation}
E_{p_A}=\int_A^\infty F\,dr\cos\theta=-\int_A^\infty F\,dr,
\end{equation}
donde asumimos que la trayectoria seguida es recta (una de las l\'ineas del
campo, por ejemplo, ya que al ser conservativo el camino es indiferente), por lo
que $\theta=\pi$, ya que $\vec{F}$ y $d\vec{r}$ tienen sentidos opuestos, y, por
lo tanto, $\cos\theta=-1$. Sustituyendo el valor del m\'odulo de la fuerza
seg\'un la expresi\'on~(\ref{ec:LeyGravNewton}) tenemos
\begin{equation}
E_{p_A}=\int_{r_A}^\infty G\frac{m\,m'}{r^2}\,dr=
\left.G\frac{m\,m'}{r}\right|_{r_A}^\infty=0-G\frac{m\,m'}{r_A},
\end{equation}
por lo que la energ\'ia potencial gravitacional es
\index{energ\'ia!potencial!gravitacional}
\begin{equation}
\label{ec:EPotAbs}
E_p=-G\frac{m\,m'}{r},
\end{equation}
donde el signo negativo indica que para llevar una masa testigo al origen de
energ\'ia potencial se ha de hacer un trabajo \textbf{opuesto} al que realiza el
campo.

La raz\'on por la que la f\'ormula~(\ref{ec:EPotAbs}) tiene un signo opuesto al
de la expresi\'on~(\ref{ec:EPot}) es que en este \'ultimo caso se consider\'o el
origen de potenciales en el plano de comparaci\'on para el cual $h=0$. Esto
quiere decir que las energ\'ias potenciales en el entorno de la Tierra ser\'an
negativas si consideramos su origen en el infinito ($E_{p_\infty}\rightarrow0$),
pero como en la pr\'actica lo que se manejan son incrementos de energ\'ia
potencial no supone ning\'un problema. En el ejemplo de
\lafigura~\ref{fig:cap9-04-energia-potencial} tendremos que el campo realiza un
trabajo positivo, $W_A^O>0$, para llevar la part\'icula del punto $A$ hasta el
plano $h_\vc{o}=0$ en virtud de la \ecuac~(\ref{ec:EPot}), ya que
$h_\vc{a}>h_\vc{o}$ y $W_A^O=E_{p_\vc{a}}-E_{p_\vc{o}}=E_{p_\vc{a}}$. Si ahora
utilizamos la \ecuac~(\ref{ec:EPotAbs}) el trabajo es el mismo, ya que,
suponiendo que $h_\vc{o}$ es la superficie de la Tierra, es decir
$r_\vc{o}=R_T$, tendremos que $r_\vc{a}=R_T+h_\vc{a}$, por lo que
\begin{equation}
W_A^O=E_{p_\vc{a}}-E_{p_\vc{o}}=
-G\frac{m_\vc{t}m'}{R_T+h_\vc{a}}+G\frac{m_\vc{t}m'}{R_T},
\end{equation}
donde $m_\vc{t}$ es la masa de la Tierra y $m'$ la de la part\'icula. Como
$R_T+h_\vc{a}>R_T$ y los numeradores son iguales, el primer sumando ser\'a m\'as
peque\~no que el segundo, por lo que $W_A^O>0$.

Si dividimos la energ\'ia potencial gravitacional entre la unidad de masa
obtenemos el potencial gravitacional\index{potencial!gravitacional}, tal y como
vimos en la secci\'on~\ref{sec:Potencial}, luego
\begin{equation}
\label{ec:potGravFis}
V=\frac{E_p}{m'}=-G\frac{m}{r},
\end{equation}
siendo su origen tambi\'en en $r\rightarrow\infty$, por lo que $V_\infty=0$. Las
superficies donde $V=\text{cte.}$ se llaman \textit{superficies equipotenciales}
\index{superficie!equipotencial} y para una masa puntual o una esfera
homog\'enea tendr\'an la forma de superficies esf\'ericas (ver
\figura~\ref{fig:cap10-02-campo-gravedad}). Teniendo en cuenta el resultado
mostrado en la \ecuac~(\ref{ec:ICgradV}) y haciendo los c\'alculos
correspondientes se prueba que
\begin{equation}
\label{ec:g_gradV}
\vec{g}=-\grad V.
\end{equation}

Sin embargo, en Geodesia y Geof\'isica, campos de estudio fundamental en los
Grados a los que van dirigidos estos apuntes, por convenio los potenciales se
definen siempre como positivos, por lo que la \ecuac~(\ref{ec:potGravFis}) se
transforma en
\begin{equation}
\label{ec:potGravGeod}
V=G\frac{m}{r}
\end{equation}
y la intensidad de campo pasa a ser simplemente el gradiente del potencial, sin
el signo negativo (v\'eanse \citet[\pnbs45]{blakely1996},
\citet[\pnbs5]{hofmann2005}, \citet[\pnbs32]{jacoby2009},
\citet[\pnbs47]{torge2001} o \citet[\pnbs83]{vanicek1986}, por ejemplo):
\begin{equation}
\label{ec:g_gradVGeod}
\vec{g}=\grad V.
\end{equation}
Por tanto, el campo de fuerzas apuntar\'a en este caso hacia los potenciales
crecientes.

% Vemos entonces que el potencial de este campo tiene siempre valores negativos
% cuando escogemos su origen en el infinito, como es habitual. De este modo, y
% suponiendo un cuerpo cualquiera de masa $M$ que engendre un campo gravitatorio,
% si dejamos <<caer>> hacia \'el una masa testigo situada en el origen (esta masa
% se ver\'a atra\'ida por el cuerpo $M$), a medida que se vaya acercando ir\'a
% ganando energ\'ia cin\'etica y perdiendo energ\'ia potencial, la cual quedar\'a
% representada mediante un n\'umero negativo. Por ejemplo, si cuando la masa
% testigo est\'a en un punto $A$ a una distancia $1000$ de $M$ la energ\'ia
% potencial es $E_{p_A}=\SI{-50}{\joule}$, cuando se encuentre en un punto $B$ a
% una distancia $300$ la energ\'ia potencial ser\'a menor, es decir
% $E_{p_B}=\SI{-150}{\joule}$, por ejemplo, lo cual es congruente con que la
% energ\'ia potencial de $A$ con respecto a $B$ sea positiva, puesto que
% $E_{p_A}-E_{p_B}=-50-(-150)=\SI{100}{\joule}$ (ver ejemplo de la
% p\'agina~\pageref{pag:ejEp}).

\section{Movimiento de una part\'icula en un campo newtoniano}
\label{sec:MPCN}

\index{movimiento!de una part\'icula en un campo newtoniano}
Estudiemos el movimiento de una part\'icula de masa $m'$ que se mueve en un
instante de tiempo con velocidad $\vec{v}$ y que est\'a sometida a la acci\'on
de un campo  de fuerzas newtoniano generado por una part\'icula de masa $m$
(origen de coordenadas), cuya expresi\'on es
\begin{equation}
\label{ec:Fkr2ur}
\vec{F}=-G\frac{m\,m'}{r^2}\vec{u}_r\longrightarrow\text{con $k=-G\,m\,m'$}
\longrightarrow\vec{F}=\frac{k}{r^2}\vec{u}_r.
\end{equation}

Al ser el campo de fuerzas central\index{fuerza!central} se cumplir\'a el
teorema de conservaci\'on del momento angular\index{teorema!de conservaci\'on
del momento angular} (ver \ecuac~(\ref{ec:ConsMomAng}) y
secci\'on~\ref{sec:FC}), mientras que por ser conservativo se cumplir\'a el
teorema de conservaci\'on de la energ\'ia
mec\'anica\index{teorema!de conservaci\'on de la energ\'ia mec\'anica} (ver
\ecuac~(\ref{ec:EpAEcAEpBEcB})). Por el teorema de conservaci\'on del momento
angular sabemos que el movimiento de la part\'icula $m'$ tiene lugar en un
plano, por lo que una forma pr\'actica de proceder consistir\'a en descomponer
su vector velocidad $\vec{v}$ en coordenadas polares (con centro en la masa $m$
que genera el campo), tal y como se puede ver en
\lafigura~\ref{fig:cap10-03-mov-newtoniano}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.45\textwidth]{fig-cap10-03-mov-newtoniano}
\caption{Descomposici\'on en coordenadas polares del movimiento de una
         part\'icula de masa $m'$ en el campo gravitacional generado por otra
         part\'icula de masa $m$.}
\label{fig:cap10-03-mov-newtoniano}
\end{figure}

El vector velocidad se puede expresar como
\begin{equation}
\label{ec:DescVPol1}
\vec{v}=\frac{d\vec{r}}{dt}=\frac{d}{dt}(r\,\vec{u}_r)=
\frac{dr}{dt}\vec{u}_r+r\frac{d\vec{u}_r}{dt},
\end{equation}
pero teniendo en cuenta que los vectores unitarios de la descomposici\'on en
polares son
\begin{equation}
\begin{dcases}
\vec{u}_r=\cos\theta\,\vec{i}+\sin\theta\,\vec{j},\\
\vec{u}_\theta=-\sin\theta\,\vec{i}+\cos\theta\,\vec{j},
\end{dcases}
\end{equation}
podemos escribir
\begin{equation}
\label{ec:durdtdthetadtu}
\frac{d\vec{u}_r}{dt}=
-\sin\theta\frac{d\theta}{dt}\,\vec{i}+\cos\theta\frac{d\theta}{dt}\,\vec{j}=
\frac{d\theta}{dt}\vec{u}_\theta,
\end{equation}
por lo que la \ecuac~(\ref{ec:DescVPol1}) queda como
\begin{equation}
\label{ec:vdrdturdtheta}
\vec{v}=\frac{dr}{dt}\vec{u}_r+r\frac{d\theta}{dt}\vec{u}_\theta,
\end{equation}
donde el primer t\'ermino se denomina \textit{velocidad radial}
\index{velocidad!radial} (es paralela al vector de posici\'on $\vec{r}$), $
\vec{v}_r$, y el segundo \textit{velocidad transversal}
\index{velocidad!transversal} (es perpendicular al vector de posici\'on
$\vec{r}$), $\vec{v}_\theta$.\index{movimiento!de una part\'icula en un campo
newtoniano} Por lo tanto se cumple que
\begin{equation}
\label{ec:vvrvt}
\vec{v}=\vec{v}_r+\vec{v}_\theta
\end{equation}
y, por el teorema de Pit\'agoras,\indnp{Pit\'agoras de Samos}
\begin{equation}
v^2=v_r^2+v_\theta^2.
\end{equation}

Calculemos ahora el momento angular\index{momento!angular} de la pat\'icula $m'$
con respecto a la part\'icula $m$. Seg\'un la \ecuac~(\ref{ec:DelMomAng})
tendremos
\begin{equation}
\vec{L}_m=\vec{r}\wedge m'\vec{v}=\vec{r}\wedge m'(\vec{v}_r+\vec{v}_\theta)=
\vec{r}\wedge m'\vec{v}_r+\vec{r}\wedge m'\vec{v}_\theta,
\end{equation}
pero al ser $\vec{r}$ y $\vec{v}_r$ paralelos su producto vectorial se
anular\'a, por lo que
\begin{equation}
\vec{L}_m=\vec{r}\wedge m'\vec{v}_\theta,
\end{equation}
de donde a partir de la definici\'on del m\'odulo del producto vectorial (ver
\ecuac~(\ref{ec:ModProdVec})) podemos obtener la expresi\'on para el m\'odulo de
$\vec{v}_\theta$ como
\begin{equation}
L_m=r\,m'v_\theta\sin\frac{\pi}{2}\longrightarrow v_\theta=\frac{L_m}{r\,m'}.
\end{equation}
\index{movimiento!de una part\'icula en un campo newtoniano}

La energ\'ia cin\'etica\index{energ\'ia!cin\'etica} de la part\'icula $m'$
ser\'a, entonces,
\begin{equation}
E_c=\frac{1}{2}m'v^2=\frac{1}{2}m'(v_r^2+v_\theta^2)=
\frac{1}{2}m'\left(\frac{dr}{dt}\right)^2+\frac{1}{2}m'\frac{L_m^2}{r^2\,m'^2}=
\frac{1}{2}m'\left(\frac{dr}{dt}\right)^2+\frac{1}{2}\frac{L_m^2}{r^2\,m'},
\end{equation}
luego podremos escribir la energ\'ia mec\'anica\index{energ\'ia!mec\'anica} o
energ\'ia total como
\begin{equation}
\label{ec:ETMovNewtoniano}
E_T=E_c+E_p=\frac{1}{2}m'v^2+\frac{k}{r}=
\frac{1}{2}m'\left(\frac{dr}{dt}\right)^2+\frac{1}{2}\frac{L_m^2}{r^2\,m'}+
\frac{k}{r},
\end{equation}
donde la energ\'ia potencial es la correspondiente a la
\ecuac~(\ref{ec:EPotAbs}), con $k=-G\,m\,m'$, tal y como se utiliz\'o en la
expresi\'on~(\ref{ec:Fkr2ur}).

De la segunda ley de Newton (\ecuac~(\ref{ec:Fma})),\indnp{Newton, Isaac}
tenemos que
\begin{equation}
\label{ec:kr2urm1dvdt}
\frac{k}{r^2}\vec{u}_r=m'\vec{a}=m'\frac{d\vec{v}}{dt},
\end{equation}
que es la ecuaci\'on del movimiento de la part\'icula. Si \'esta tiene una
velocidad inicial que no coincide en direcci\'on con $\vec{r}$ describir\'a una
trayectoria plana que, como demostraremos a continuaci\'on, es una c\'onica.

Si multiplicamos vectorialmente ambos miembros de la
\ecuac~(\ref{ec:kr2urm1dvdt}) por el momento angular con respecto a $m$
obtenemos
\begin{equation}
m'\vec{a}\wedge\vec{L}_m=\frac{k}{r^2}\vec{u}_r\wedge\vec{L}_m=
\frac{k}{r^2}\vec{u}_r\wedge(\vec{r}\wedge m'\vec{v}),
\end{equation}
que se puede simplificar a
\begin{equation}
\label{ec:aLmmkru}
\vec{a}\wedge\vec{L}_m=\frac{k}{r}\vec{u}_r\wedge(\vec{u}_r\wedge\vec{v}),
\end{equation}
donde hemos utilizado la identidad $\vec{u}_r=\vec{r}/r$. Teniendo en cuenta la
identidad~(\ref{ec:DoblePVmn}) la \ecuac~(\ref{ec:aLmmkru}) se transforma en
\begin{equation}
\label{ec:aLm-krvt}
\vec{a}\wedge\vec{L}_m=
\frac{k}{r}\left[(\vec{u}_r\cdot\vec{v})\vec{u}_r-
(\vec{u}_r\cdot\vec{u}_r)\vec{v}\right]=\frac{k}{r}(\vec{v}_r-\vec{v})=
-\frac{k}{r}\vec{v}_\theta,
\end{equation}
donde hemos tenido en cuenta que $\vec{u}_r\cdot\vec{v}$ es la componente radial
de la velocidad y hemos utilizado la relaci\'on~(\ref{ec:vvrvt}).

Utilizando las \ecuacs~(\ref{ec:durdtdthetadtu}) y~(\ref{ec:vdrdturdtheta}) y
recordando que $\vec{a}=d\vec{v}/dt$ y que $\vec{L}_m$ es constante la
\ecuac~(\ref{ec:aLm-krvt}) se transforma en
\begin{equation}
\frac{d}{dt}(\vec{v}\wedge\vec{L}_m)=-k\frac{d\vec{u}_r}{dt},
\end{equation}
que integrada proporciona
\begin{equation}
\vec{v}\wedge\vec{L}_m=-k(\vec{u}_r+\vec{e}),
\end{equation}
donde $k\,\vec{e}$ es un vector que act\'ua como constante de integraci\'on y
est\'a contenido en el plano de la trayectoria de la part\'icula. Si ahora
multiplicamos escalarmente por $\vec{r}$ tenemos
\begin{equation}
\vec{r}\cdot(\vec{v}\wedge\vec{L}_m)=-k(r+\vec{r}\cdot\vec{e}),
\end{equation}
que, utilizando las relaciones circulares~(\ref{ec:RelCirculares}), el hecho de
que $\vec{L}_m/m'=\vec{r}\wedge\vec{v}$ y desarrollando el segundo t\'ermino, se
transforma en
\begin{equation}
\vec{L}_m\cdot(\vec{r}\wedge\vec{v})=\frac{L_m^2}{m'}=-k(r+r\,e\cos\theta).
\end{equation}
Finalmente, despejando $r$ obtenemos
\begin{equation}
\label{ec:rMovNewtoniano}
r=\frac{-\frac{L_m^2}{k\,m'}}{1+e\cos\theta}=\frac{s}{1+e\cos\theta},
\end{equation}
donde $\theta$ es el \'angulo que forma $\vec{r}$ con la direcci\'on constante
$\vec{e}$ y con $s=-L_m^2k^{-1}m'^{-1}$ una constante del movimiento que junto a
$e$ determinan la trayectoria.
\index{movimiento!de una part\'icula en un campo newtoniano}

La expresi\'on~(\ref{ec:rMovNewtoniano}) es la ecuaci\'on de una c\'onica
\citep[\pnbs465]{burgos2000} de excentricidad $e$, luego la trayectoria de una
masa $m'$ con velocidad inicial $\vec{v}$ en el campo gravitacional generado por
una masa $m$ ser\'a una elipse, una par\'abola o una hip\'erbola donde $m$
estar\'a situada en uno de sus focos.\index{foco!de una c\'onica} Si $e=0$ el
vector $\vec{r}$ tendr\'a m\'odulo constante, por lo que la trayectoria ser\'a
una circunferencia, si $e<1$ ser\'a una elipse, si $e=1$ una par\'abola y si
$e>1$ la trayectoria ser\'a una hip\'erbola.
\index{movimiento!de una part\'icula en un campo newtoniano}

La excentricidad, a su vez, vendr\'a determinada por la energ\'ia total de la
masa $m'$, tal y como probaremos en lo que sigue. Sea cual sea la excentricidad,
si el \'angulo $\theta$, \'angulo entre el radio vector $\vec{r}$ y el vector
$\vec{e}$, vale $1$ el denominador de la \ecuac~(\ref{ec:rMovNewtoniano}) ser\'a
m\'aximo, lo que implica, al ser $s$ una constante, que $r$ ser\'a m\'inimo. Por
lo tanto, la direcci\'on y sentido de $\vec{e}$ coinciden con los del valor
m\'inimo de $\vec{r}$. En este caso la velocidad radial $\vec{v}_r$ de la
part\'icula se anula, por lo que su energ\'ia total
(\ecuac~(\ref{ec:ETMovNewtoniano})) ser\'a
\begin{equation}
E_T=\frac{1}{2}\frac{L_m^2}{r^2\,m'}+\frac{k}{r},
\end{equation}
donde
\begin{equation}
r=\frac{s}{1+e},
\end{equation}
ecuaciones que combinadas con $s=-L_m^2k^{-1}m'^{-1}$ proporcionan
\begin{equation}
\begin{split}
E_T=&\frac{1}{2}\frac{L_m^2(1+e)^2}{s^2\,m'}+\frac{k(1+e)}{s}=
\frac{1+e}{s}\left[\frac{L_m^2(1+e)}{2\,s\,m'}+k\right]=\\
=&-\frac{(1+e)k\,m'}{L_m^2}\left[-\frac{(1+e)k}{2}+k\right]=
\frac{k^2m'}{2L_m^2}(e^2-1),
\end{split}
\end{equation}
luego
\begin{equation}
e=\sqrt{1+\frac{2E_TL_m^2}{k^2m'}}.
\end{equation}
por lo que atendiendo a los posibles valores de la energ\'ia total tendremos
que\index{movimiento!de una part\'icula en un campo newtoniano}
\begin{equation*}
\begin{dcases}
E_T>0\longrightarrow e>1\longrightarrow\text{ hip\'erbola},\\
E_T=0\longrightarrow e=1\longrightarrow\text{ par\'abola},\\
E_T<0
\begin{dcases}
0<e<1\longrightarrow\text{ elipse},\\
e=0\longrightarrow\text{ circunferencia},\\
e<0\longrightarrow\text{ no permitido.}
\end{dcases}
\end{dcases}
\end{equation*}

% \vspace{2mm}
% \diagram{}
% {
% $E_T>0\longrightarrow e>1\longrightarrow$ hip\'erbola, \\
% $E_T=0\longrightarrow e=1\longrightarrow$ par\'abola,\\
% $E_T<0$
% \diagram{}
% {
%     $0<e<1\longrightarrow$ elipse,\\
%     $e=0\longrightarrow$ circunferencia,\\
%     $e<0\longrightarrow$ no permitido.
% }
% }
% \vspace{2mm}

De todo lo anterior podemos deducir que para poner un sat\'elite en \'orbita
necesitaremos imprimirle una determinada velocidad inicial una vez situado a la
altura deseada sobre la superficie de la Tierra. Seg\'un sea esa velocidad la
energ\'ia total ser\'a una u otra, lo que dar\'a lugar a alguna de las \'orbitas
estudiadas.

Veamos el caso particular de una \'orbita circular. En este caso la fuerza de
atracci\'on gravitacional ser\'a, para cualquier punto de la trayectoria,
\begin{equation}
\vec{F}=-G\frac{m\,m'}{r^2}\vec{u}_r,
\end{equation}
mientras que, por tratarse de un movimiento circular uniforme,
\index{movimiento!circular uniforme} la fuerza que act\'ua sobre la part\'icula
que orbita debida a la aceleraci\'on normal es (ver
\ecuac~(\ref{ec:AcelNormal}))
\begin{equation}
\vec{F}_n=-m'\frac{v^2}{r}\vec{u}_r.
\end{equation}
Ambas fuerzas han de ser iguales, luego\label{pag:OrbCircV}
\begin{equation}
-G\frac{m\,m'}{r^2}\vec{u}_r=-m'\frac{v^2}{r}\vec{u}_r\longrightarrow
v=\sqrt{G\frac{m}{r}},
\end{equation}
de donde se deduce que la energ\'ia total ser\'a
\begin{equation}
\begin{drcases}
E_c=\frac{1}{2}m'v^2=\frac{1}{2}m'G\frac{m}{r},\\
E_p=-G\frac{m\,m'}{r}
\end{drcases}
\longrightarrow
E_T=E_c+E_p=-\frac{1}{2}G\frac{m\,m'}{r}.
\end{equation}

\section{Velocidad de escape}

Para que la trayectoria de una part\'icula de masa $m'$ que orbita alrededor de
otra de masa $m$ sea abierta ($E_T\ge0$, hip\'erbola o par\'abola) ha de
cumplirse que (ver \ecuac~(\ref{ec:ETMovNewtoniano}))
\begin{equation}
\frac{1}{2}m'v^2\ge-\frac{k}{r}\longrightarrow
\frac{1}{2}m'v^2\ge G\frac{m\,m'}{r},
\end{equation}
es decir, que la energ\'ia cin\'etica para una posici\'on determinada ha de ser
mayor que el valor absoluto de la energ\'ia potencial correspondiente. Si
despejamos la velocidad obtenemos
\begin{equation}
\label{ec:vesqrt2Gmr}
v_e\ge\sqrt{\frac{2\,G\,m}{r}},
\end{equation}
cantidad llamada \textit{velocidad de escape} (o de \textit{fuga}),
\index{velocidad!de escape}\index{velocidad!de fuga|see{velocidad de escape}}
que es la velocidad m\'inima que $m'$ ha de tener para que su trayectoria sea
abierta. En concreto, si $v>v_e$ la trayectoria ser\'a una hip\'erbola, si
$v=v_e$ ser\'a una par\'abola y si $v<v_e$ ser\'a una elipse o una
circunferencia.

Si nos encontramos en la superficie de la Tierra y queremos lanzar un objeto de
masa $m'$ hacia arriba con la intenci\'on de que escape del campo de la
gravedad, es decir, que no caiga de nuevo hacia nuestro planeta, habremos de
imprimirle una velocidad inicial $v_e$ tal que la energ\'ia cin\'etica que
conlleve sea como m\'inimo igual en valor absoluto que la energ\'ia potencial
que tiene el objeto en el instante inicial. En efecto, y en virtud del teorema
de conservaci\'on de la energ\'ia mec\'anica,
\index{teorema!de conservaci\'on de la energ\'ia mec\'anica} la energ\'ia
cin\'etica que pierda el objeto en su ascenso ha de ser igual a la energ\'ia
potencial que gane, luego la suma de ambas en el instante inicial ser\'a igual a
la suma en el infinito, origen de la energ\'ia potencial y punto en que la
velocidad alcanzar\'a el valor $0$:
\begin{equation}
\frac{1}{2}m'v_e^2-G\frac{m_\vc{t}m'}{r_\vc{t}}=0+0\longrightarrow
v_e=\sqrt{\frac{2\,G\,m_\vc{t}}{r_\vc{t}}},
\end{equation}
que no es m\'as que la \ecuac~(\ref{ec:vesqrt2Gmr}) particularizada para un
punto sobre la superficie de la Tierra, donde $m_\vc{t}$ es la masa del planeta
y $r_\vc{t}$ su radio.
\index{velocidad!de escape}

Una consecuencia del concepto de velocidad de escape\index{velocidad!de escape}
es el hecho de que gracias a \'el podemos conjeturar la composici\'on que
tendr\'a la atm\'osfera de un planeta o sat\'elite. Transcribimos a
continuaci\'on parte del texto que se puede leer en
\citet[\pnbs375]{tipler2010a}:
\begin{quote}
La velocidad de escape de un planeta o sat\'elite respecto a las velocidades
t\'ermicas de las mol\'eculas de un gas determina el tipo de atm\'osfera que el
planeta o sat\'elite puede tener. La energ\'ia cin\'etica media de las
mol\'eculas del gas, $(1/2\,m\,v^2)_\text{m}$, es proporcional a la temperatura
absoluta $T$. En la superficie de la Tierra las velocidades moleculares del
ox\'igeno y nitr\'ogeno son muy inferiores a la velocidad de escape y, por ello,
estos gases permanecen en nuestra atm\'osfera. En cambio, las mol\'eculas m\'as
ligeras de hidr\'ogeno y helio poseen en su mayor parte velocidades superiores a
la de escape; por ello, estos gases no se encuentran en nuestra atm\'osfera.
[...] La velocidad de escape para la Luna es de
$\SI[per-mode=reciprocal]{2.3}{\kilo\metre\per\second}$, muy inferior a la de la
Tierra y demasiado peque\~na para que pueda existir all\'i ning\'un tipo de
atm\'osfera.
\end{quote}

\section{\'Orbita geoestacionaria}

Un \textit{sat\'elite geoestacionario}\index{sat\'elite geoestacionario} es
aqu\'el que en su trayectoria permanece fijo sobre la vertical del mismo punto
del ecuador de la Tierra. De esta definici\'on se deduce que su \'orbita ser\'a
circular y que su per\'iodo ser\'a \textbf{el mismo} que el de rotaci\'on del
planeta. Para calcular el radio de esta \textit{\'orbita geoestacionaria}
\index{orbita@\'orbita geoestacionaria} tendremos en cuenta que, al ser un
movimiento circular uniforme,\index{movimiento!circular uniforme} la fuerza
normal a la que se ve sometido el sat\'elite se iguala con la fuerza de
atracci\'on del campo de la gravedad, luego, al igual que hicimos en el caso del
c\'alculo de la energ\'ia total de una \'orbita circular (ver
p\'agina~\pageref{pag:OrbCircV}),
\begin{equation}
-G\frac{m\,m'}{r^2}\vec{u}_r=-m'\frac{v^2}{r}\vec{u}_r=
-m'\frac{\omega^2r^2}{r}\vec{u}_r=-m'\omega^2r\,\vec{u}_r,
\end{equation}
done se tiene en cuenta que $v=\omega\,r$, siendo $\omega$ la velocidad angular.
Despejando $r$ obtenemos
\begin{equation}
r=\sqrt[3]{G\frac{m}{\omega^2}},
\end{equation}
donde $m$ es la masa del planeta. Para la Tierra,
$r\approx\SI{42200}{\kilo\metre}$.

\section{Deducci\'on de la tercera ley de Kepler}

La primera ley de Kepler\indnp{Kepler, Johannes}
\index{leyes de Kepler!primera ley} se ha deducido en este texto en la
secci\'on~\ref{sec:MPCN}, mientras que la segunda
\index{leyes de Kepler!segunda ley} se dedujo en la secci\'on~\ref{sec:FC}, en
el contexto general del estudio de las fuerzas centrales\index{fuerza!central} y
el principio de conservaci\'on del momento angular.
\index{teorema!de conservaci\'on del momento angular}
En esta secci\'on estudiaremos, para el caso particular de una \'orbita
circular y siguiendo a \citet[\pnbs372]{tipler2010a}, c\'omo la tercera ley de
Kepler\indnp{Kepler, Johannes}\index{leyes de Kepler!tercera ley} deriva de la
ley de gravitaci\'on.

Supongamos una part\'icula de masa $m'$ orbitando alrededor de otra de masa $m$
con una trayectoria circular de radio $r$ y velocidad (su m\'odulo) $v$. El
m\'odulo de la aceleraci\'on normal\index{aceleraci\'on!componente normal} de
este movimiento circular uniforme queda descrito mediante la
\ecuac~(\ref{ec:AcelNormalMCU}), que ser\'a igual al m\'odulo deducido a
part\'ir de la ley de gravitaci\'on universal (\ecuac~(\ref{ec:grav})), luego
\begin{equation}
\label{ec:v2Gmr}
\frac{v^2}{r}=G\frac{m}{r^2}\longrightarrow v^2=G\frac{m}{r}.
\end{equation}

Como $m'$ recorre el per\'imetro de la \'orbita en un tiempo $T$ su velocidad
se escribe como
\begin{equation}
v=\frac{2\pi\,r}{T},
\end{equation}
ecuaci\'on que combinada con la expresi\'on~(\ref{ec:v2Gmr}) proporciona
\begin{equation}
T^2=\frac{4\pi^2}{G\,m}r^3,
\end{equation}
tercera ley de Kepler\indnp{Kepler, Johannes}
\index{leyes de Kepler!tercera ley} (\ecuac~(\ref{ec:TercLeyKepler})) para
una \'orbita circular y donde $c=4\pi^2/(G\,m)=\text{cte.}$

\section{Medici\'on de la gravedad por medio de p\'endulos}

Si bien hoy en d\'ia las mediciones absolutas de la gravedad de llevan a cabo
mediante experimentos de ca\'ida libre de una masa o sim\'etricos de lanzamiento
hacia arriba y ca\'ida (v\'ease \citet[cap\'itulo\nobreakspace5]{torge1989}),
durante $300$ a\~nos la t\'ecnica utilizada fue el m\'etodo del p\'endulo. En
esta secci\'on expondremos de manera muy general la teor\'ia mediante la cual
puede medirse la aceleraci\'on de la gravedad en un punto utilizando tal
dispositivo. Cabe indicar aqu\'i que, si bien las unidades de la aceleraci\'on
de la gravedad en el Sistema Internacional con las consabidas de
$\si[per-mode=reciprocal]{\metre\per\second\squared}$, en la pr\'actica
gravim\'etrica en Geodesia y Geof\'isica es muy com\'un utilizar el sistema cgs,
donde la unidad correspondiente es el gal
($\SI{1}{\gal}=\SI[per-mode=reciprocal]{1}{\centi\metre\per\second\squared}$).
\index{unidades!gal}

Un p\'endulo simple\index{p\'endulo!simple} consta de una part\'icula de masa
$m$ suspendida de un punto $O$ mediante un cable de longitud $L$ y masa
despreciable. Si separamos la masa de su posici\'on de equilibrio un peque\~no
\'angulo (tal que $\sin\varphi\approx\varphi$) comenzar\'a a oscilar
describiendo un movimiento vibratorio arm\'onico.\index{movimiento!vibratorio
arm\'onico} \Lafigura~\ref{fig:cap10-04-pendulo} (izquierda) muestra
esquem\'aticamente un p\'endulo simple y las fuerzas implicadas en su
oscilaci\'on. Por una parte tenemos el peso (en lo que sigue prescindimos del
car\'acter vectorial de las fuerzas), $P=m\,g$, y por otra la tensi\'on $T$ del
cable que une la part\'icula de masa $m$ con el punto $O$. Si descomponemos el
peso nos encontramos con una fuerza normal $F_n$ que equilibra la tensi\'on $T$
y con una componente transversal igual a
\begin{equation}
\label{ec:FtmgcosalfaPendulo}
F_t=-m\,g\sin\varphi,
\end{equation}
donde el signo negativo indica que esta fuerza se opone al movimiento de la
masa.

Para \'angulos $\varphi$ peque\~nos la trayectoria seguida por la masa $m$ puede
asimilarse a una recta (la distancia $\overline{AA'}$ en
\lafigura~\ref{fig:cap10-04-pendulo}, izquierda, es despreciable), por lo que
$\sin\varphi=x/L$ y la \ecuac~(\ref{ec:FtmgcosalfaPendulo}) se transforma en
\begin{equation}
\label{ec:FtmgxLPendulo}
F_t=-m\,g\frac{x}{L}=-k\,x,
\end{equation}
con $k=m\,g/L$. Ahora bien, si dividimos la \ecuac~(\ref{ec:FtmgxLPendulo})
entre la masa $m$ la expresi\'on resultante describe la aceleraci\'on
(recordemos que $\vec{F}=m\,\vec{a}$) de un movimiento vibratorio arm\'onico
\index{movimiento!vibratorio arm\'onico}(ver \ecuac~(\ref{ec:a-ky})) con
$k=g/L$, luego sabiendo que en el MVA $k=\omega^2$ tendremos que
\begin{equation}
\label{ec:gPenduloSimple}
\frac{g}{L}=\omega^2=\frac{4\pi^2}{T^2}\longrightarrow g=\frac{4\pi^2L}{T^2},
\end{equation}
donde $T$ es el per\'iodo de oscilaci\'on ($\omega=2\pi/T$). La
\ecuac~(\ref{ec:gPenduloSimple}) es v\'alida \textbf{\'unicamente para
peque\~nas} oscilaciones.
\begin{figure}[htb]
\centering
\includegraphics[width=0.60\textwidth]{fig-cap10-04-pendulo}
\caption{Esquema de un p\'endulo simple (izquierda) y de un p\'endulo f\'isico
         (derecha).}
\label{fig:cap10-04-pendulo}
\end{figure}
\index{p\'endulo!simple}\index{p\'endulo!f\'isico}
\index{p\'endulo!compuesto|see{p\'endulo f\'isico}}

Si en lugar de un p\'endulo simple tenemos un s\'olido r\'igido de masa $m$ que
puede girar en torno a un eje $e$ nos encontramos ante un
\textit{p\'endulo f\'isico} o \textit{compuesto}.\index{p\'endulo!f\'isico}
\index{p\'endulo!compuesto|see{p\'endulo f\'isico}}
\Lafigura~\ref{fig:cap10-04-pendulo} (derecha) muestra un esquema donde el
s\'olido es un cuerpo plano. Llamemos $d$ a la distancia entre el punto de
intersecci\'on $O$ con el eje y el centro de masas $\textsc{cm}$ del cuerpo, el
cual estar\'a separado de la posici\'on de equilibrio un \'angulo
\textbf{peque\~no} $\varphi$, tal que $\sin\varphi\approx\varphi$. Si en ese
instante se deja libre, el cuerpo comenzar\'a a oscilar en torno al eje.

El momento de la fuerza $m\,\vec{g}$ con respecto al eje $\vec{e}$ es
\begin{equation}
M_e=-m\,g\,d\sin{\varphi}=-m\,g\,d\,\varphi,
\end{equation}
donde nuevamente el signo negativo indica que dicho momento se opone al
movimiento de oscilaci\'on del cuerpo y hemos tenido en cuenta que
$\sin\varphi\approx\varphi$. Si ahora introducimos el valor de $M_e$ en la
\ecuac~(\ref{ec:MeIeAlfa}) tenemos que
\begin{equation}
-m\,g\,d\,\varphi=I_e\alpha,
\end{equation}
que es la ecuaci\'on de un movimiento vibratorio arm\'onico.
\index{movimiento!vibratorio arm\'onico!din\'amica}
En efecto, si tenemos en cuenta que la aceleraci\'on angular es
$\alpha=d^2\varphi/dt^2$ podemos escribir
\begin{equation}
\frac{d^2\varphi}{dt^2}=-\frac{m\,g\,d}{I_e}\varphi=-k\,\varphi,
\end{equation}
ecuaci\'on id\'entica en su forma a la expresi\'on~(\ref{ec:FtmgxLPendulo})
(dividida entre la masa), donde $k=m\,g\,d/I_e$. Como sabemos que en un
movimiento vibratorio arm\'onico $k=\omega^2$ y que $\omega=2\pi/T$, siendo $T$
el per\'iodo, deducimos finalmente que
\begin{equation}
\label{ec:gPenduloFisico}
\omega^2=\frac{4\pi^2}{T^2}=\frac{m\,g\,d}{I_e}\longrightarrow
g=\frac{4\pi^2I_e}{m\,d\,T^2}.
\end{equation}

Por \'ultimo cabe indicar que se puede calcular una longitud $L$ de cable para
un p\'endulo simple de tal forma que tenga el mismo per\'iodo que el de un
p\'endulo f\'isico equivalente.\index{p\'endulo!simple}
\index{p\'endulo!f\'isico} De las \ecuacs~(\ref{ec:gPenduloSimple})
y~(\ref{ec:gPenduloFisico}) se deduce que
\begin{equation}
\begin{dcases}
T_{PS}=2\pi\sqrt{\frac{L}{g}},\\
T_{PF}=2\pi\sqrt{\frac{I_e}{m\,d\,g}},
\end{dcases}
\end{equation}
de donde se obtiene la denominada \textit{longitud equivalente} como
\index{p\'endulo!f\'isico!longitud equivalente}
\begin{equation}
2\pi\sqrt{\frac{L}{g}}=2\pi\sqrt{\frac{I_e}{m\,d\,g}}\longrightarrow
L=\frac{I_e}{m\,d}.
\end{equation}
