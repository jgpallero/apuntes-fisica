\chapter{Est\'atica}
\label{cap:Estatica}

\section{Est\'atica del s\'olido r\'igido}

La Est\'atica\index{estatica@Est\'atica} se define como la parte de la
Mec\'anica que se ocupa del \textit{estado de equilibrio}
\index{estado!de equilibrio} de los cuerpos o sistemas materiales. Entenderemos
por estado de equilibrio aquella configuraci\'on de un sistema que, bajo la
acci\'on de un conjunto de fuerzas, hace que permanezca invariable con el
transcurso del tiempo. Esta definici\'on de equilibrio contiene el aspecto
cinem\'atico del mismo y, adem\'as, nos presenta a la Est\'atica como la ciencia
que estudia las leyes que deben satisfacer los conjuntos de fuerzas que al
actuar sobre un sistema no modifiquen los par\'ametros que fijan la posici\'on
de \'este.

En cuanto a los tipos de fuerzas o \textit{acciones} que act\'uan sobre un
sistema cualquiera, podemos hacer una primera clasificaci\'on distingui\'endolas
entre \textit{acciones de contacto}\index{acci\'on!de contacto} y
\textit{acciones a distancia}\index{acci\'on!a distancia}
\citep[p\'ags.\nbs 123 y\nbs124]{dejuana2003a}. Las primeras son acciones en las
que el agente que las produce ha de estar en contacto con el sistema, como por
ejemplo el rozamiento, las fuerzas ejercidas mediante cables, muelles, etc. En
las acciones a distancia no hay necesidad de contacto f\'isico, siendo la
acci\'on gravitacional un ejemplo importante. Si atendemos a la localizaci\'on
de las acciones con respecto al sistema, \'estas pueden clasificarse como
\textit{interiores}\index{acci\'on!interior} o \textit{exteriores},
\index{acci\'on!exterior} seg\'un que la causa pertenezca al sistema o sea
exterior a \'el \citep[p\'ag.\nbs 124]{dejuana2003a}.

\section{Equilibrio del s\'olido r\'igido libre}

En Cinem\'atica hemos definido como s\'olido r\'igido
\index{solido@s\'olido r\'igido} (p\'agina~\pageref{pag:DefSolRig}) a todo
sistema de puntos materiales cuyo v\'inculo sea la rigidez, esto es, que las
distancias mutuas entre dos cualquiera de sus puntos permanezcan invariables
ante el fen\'omeno f\'isico a que el cuerpo se vea sometido. Como la distancia
entre sus puntos debe permanecer invariable, al aplicar dos fuerzas iguales y
opuestas a lo largo de la recta que une dos puntos cualesquiera de un s\'olido
r\'igido en equilibrio, \'este debe subsistir, ya que el \'unico efecto de tales
fuerzas ser\'ia un desplazamiento a lo largo de la citada direcci\'on, y tal
desplazamiento debe ser nulo en virtud de la condici\'on geom\'etrica de
rigidez.\index{solido@s\'olido r\'igido!condici\'on geom\'etrica de rigidez} Tal
propiedad se conoce como \textit{caracter\'istica est\'atica del s\'olido
r\'igido}.\index{solido@s\'olido r\'igido!caracter\'istica est\'atica}

El hecho de que el equilibrio de un s\'olido r\'igido no se altere cuando se
aplican a dos puntos arbitrarios fuerzas iguales y opuestas a lo largo de la
recta que los une lleva impl\'icito el postulado de que las fuerzas actuantes
sobre el s\'olido se comportan como vectores deslizantes. Sea un s\'olido
r\'igido sometido a una fuerza $\vec{F}$ aplicada en $O$, con una direcci\'on
$\overline{\textsc{oo}'}$ (ver
\figura~\ref{fig:cap8-01-estatica-vec-deslizantes}, izquierda). Si a\~nadimos
dos fuerzas $-\vec{F}$ y $\vec{F}$, iguales en m\'odulo a $\vec{F}$, con la
misma direcci\'on, sentidos opuestos y aplicadas en $O$ y $O'$, respectivamente
(\figura~\ref{fig:cap8-01-estatica-vec-deslizantes}, centro), la fuerza original
en $O$ se cancelar\'a con $-\vec{F}$, quedando el s\'olido sometido a $\vec{F}$,
pero aplicada en $O'$ (\figura~\ref{fig:cap8-01-estatica-vec-deslizantes},
derecha). La fuerza original se ha deslizado a lo largo de su recta de acci\'on
mientras que el s\'olido permanece est\'aticamente invariable.
\begin{figure}[htb]
\centering
\includegraphics[width=0.99\textwidth]{fig-cap8-01-estatica-vec-deslizantes}
\caption{Las fuerzas aplicadas a un s\'olido r\'igido se comportan como vectores
         deslizantes.}
\label{fig:cap8-01-estatica-vec-deslizantes}
\end{figure}

La \textit{condici\'on de equilibrio}
\index{solido@s\'olido r\'igido!condici\'on de equilibrio} de un s\'olido
r\'igido consiste en que el sistema de fuerzas que act\'uan sobre \'el formen un
sistema de vectores deslizantes nulo,\index{sistema de vectores deslizantes} es
decir, que su resultante y su momento resultante respecto de cualquier punto
fijo han de ser cero:
\begin{equation}
\label{ec:condEqSolRig}
\begin{dcases}
\sum_{i=1}^N\vec{F}_i=0,\\
\sum_{i=1}^N\vec{M}_i=0,
\end{dcases}
\end{equation}
donde $N$ es el n\'umero de fuerzas actuantes. A esta condici\'on se ha de
a\~nadir tambi\'en la condici\'on cinem\'atica de que el s\'olido est\'e en
\textbf{reposo}. Al ser las \ecuacs~(\ref{ec:condEqSolRig}) vectoriales podemos
proyectar los vectores sobre los ejes de coordenadas y obtener
\begin{equation}
\label{ec:condEqCompSolRig}
\begin{dcases}
\sum_{i=1}^NF_{x_i}=0,\\
\sum_{i=1}^NF_{y_i}=0,\\
\sum_{i=1}^NF_{z_i}=0,
\end{dcases}
\hspace{3cm}
\begin{dcases}
\sum_{i=1}^NM_{x_i}=0,\\
\sum_{i=1}^NM_{y_i}=0,\\
\sum_{i=1}^NM_{z_i}=0,
\end{dcases}
\end{equation}
que son seis ecuaciones escalares, mismo n\'umero que los grados de libertad del
s\'olido r\'igido.

\section{Equilibrio del s\'olido vinculado}
\label{sec:ESV}

Entendemos por \textit{enlace},\index{enlace} \textit{v\'inculo}
\index{vinculo@v\'inculo} o \textit{ligadura}\index{ligadura} toda limitaci\'on
al movimiento libre de los puntos de un sistema material. Para el c\'alculo del
movimiento o del equilibrio de dichos sistemas sujetos a v\'inculos es necesario
introducir un principio que nos permita pasar de la mec\'anica de los sistemas
vinculados a la mec\'anica de los sistemas libres. Tal principio de llama de
\textit{liberaci\'on}\index{principio!de liberaci\'on} y se enuncia como sigue:
podemos suponer a cualquier sistema libre de ligaduras con tal de a\~nadir a las
fuerzas activas las llamadas \textit{fuerzas de reacci\'on vincular},
\index{fuerza!de reacci\'on vincular} las cuales sustituyen a dichos v\'inculos.
Todo sistema sujeto a enlaces se puede transformar en un sistema libre
sustituyendo \'estos por fuerzas que produzcan los mismos efectos est\'aticos o
din\'amicos que dichas ligaduras.

Las caracter\'isticas generales de las fuerzas de reacci\'on vincular son:
\begin{itemize}
\item No producen movimiento, sino que tan solo impiden el que generan las
      fuerzas activas cuando aqu\'el no es compatible con los v\'inculos.
\item Su magnitud es ilimitada, pudiendo tomar valores tan altos como sea
      necesario dependiendo de las fuerzas activas, pero se anulan cuando las
      fuerzas activas se anulan.
\item Su direcci\'on y sentido dependen de la naturaleza del v\'inculo.
\end{itemize}

De acuerdo con el \textit{principio de reacci\'on vincular}
\index{principio!de reacci\'on vincular} podemos sustituir en el sistema
vinculado los enlaces por las fuerzas de reacci\'on vincular correspondientes,
de tal forma que tendremos el sistema libre sometido a las fuerzas activas y a
las de reacci\'on vincular. Las \ecuacs~(\ref{ec:condEqSolRig})
y~(\ref{ec:condEqCompSolRig}) se transforman en
\begin{equation}
\label{ec:EqSolRigActReac}
\begin{drcases}
\sum_{i=1}^K\vec{F}_i+\sum_{i=1}^{K'}\vec{F}'_i=0,\\
\sum_{i=1}^K\vec{M}_i+\sum_{i=1}^{K'}\vec{M}'_i=0
\end{drcases}
\longrightarrow
\begin{dcases}
\sum_{i=1}^KF_{x_i}+\sum_{i=1}^{K'}F'_{x_i}=0,\\
\sum_{i=1}^KF_{y_i}+\sum_{i=1}^{K'}F'_{y_i}=0,\\
\sum_{i=1}^KF_{z_i}+\sum_{i=1}^{K'}F'_{z_i}=0,
\end{dcases}
\hspace{1.5cm}
\begin{dcases}
\sum_{i=1}^KM_{x_i}+\sum_{i=1}^{K'}M'_{x_i}=0,\\
\sum_{i=1}^KM_{y_i}+\sum_{i=1}^{K'}M'_{y_i}=0,\\
\sum_{i=1}^KM_{z_i}+\sum_{i=1}^{K'}M'_{z_i}=0,
\end{dcases}
\end{equation}
donde el n\'umero total de fuerzas es $N=K+K'$,  $\vec{F}'$ son las fuerzas de
reacci\'on vincular \index{fuerza!de reacci\'on vincular} y $\vec{M}'$ los
momentos que generan respecto al mismo punto sobre el que se calculan los
momentos de las fuerzas activas. Las \ecuacs~(\ref{ec:EqSolRigActReac}) muestran
que las reacciones vinculares forman un sistema de fuerzas opuesto al de las
fuerzas activas. Las fuerzas etiquetadas como $\vec{F}$ son fuerzas exteriores
que generalmente nos vendr\'an dadas como datos, mientras que las fuerzas de
reacci\'on vincular, etiquetadas como $\vec{F}'$, ser\'an inc\'ognitas que
tendremos que determinar para comprobar si con las fuerzas activas hay o no
equilibrio.

\Lafigura~\ref{fig:cap8-02-tipos-enlace}, transcrita parcialmente de
\citet[p\'ag.\nbs 129]{beer1990a}, muestra las reacciones en una serie de
uniones en dos dimensiones. En v\'inculos de tipo rodillo, balanc\'in o
\textbf{apoyo sobre superficie lisa}, es decir, sin rozamiento, la reacci\'on es
una fuerza \textbf{perpendicular} a la superficie de apoyo. En enlaces tipo
cable o biela la reacci\'on es una fuerza cuya direcci\'on es la del propio
cable, mientras que en ligaduras tipo deslizadera o pasador la reacci\'on es
perpendicular a la direcci\'on de deslizamiento. En articulaciones o apoyos en
superficies rugosas, esto es, \textbf{con rozamiento}, la reacci\'on es un
vector fuerza de direcci\'on arbitraria, el cual puede descomponerse en su
proyecci\'on sobre dos direcciones perpendiculares para faciliar los c\'alculos.
Por \'ultimo, las reacciones a un empotramiento son un vector fuerza de
direcci\'on arbitraria m\'as un momento.

\section{S\'olido vinculado a otros}
\label{sec:SVO}

Consideremos un sistema constituido por un conjunto de s\'olidos vinculados los
unos a los otros. El estudio del equilibrio de tal sistema se logra mediante el
\textit{principio de fragmentaci\'on},\index{principio!de fragmentaci\'on} que
establece que si un sistema de puntos materiales est\'a en equilibrio y lo
dividimos en varios subsistemas, cada uno de ellos ha de estar en equilibrio por
separado. Con tal principio el estudio queda reducido a considerar aisladamente
cada uno de los s\'olidos que constituyen el sistema y a aplicar en cada caso
las condiciones de equilibrio~(\ref{ec:EqSolRigActReac}), teniendo en cuenta que
las fuerzas interiores ejercidas entre s\'olidos ser\'an consideradas como
exteriores para cada uno por separado. Al pasar de un fragmento al siguiente las
fuerzas interiores correspondientes a cada ligadura cambian de signo en virtud
del \textit{principio de acci\'on y reacci\'on}.
\index{principio!de acci\'on y reacci\'on}

\newpage %necesito esta orden para que el párrafo anterior no ponga parte de la
         %última frase en la página siguiente

\begin{figure}[H]
\centering
\includegraphics[width=0.965\textwidth]{fig-cap8-02-tipos-enlace}
\caption{Reacciones en apoyos y uniones bidimensionales.}
\label{fig:cap8-02-tipos-enlace}
\end{figure}

\section{Ejemplos}

En esta secci\'on resolveremos varios problemas en dos dimensiones de Est\'atica
con el fin de mostrar la mec\'anica de c\'alculo en este tipo de cuestiones.
Aunque es imposible abarcar todas las posibles combinaciones de elementos en un
libro, cualquier sistema, por complicado que sea, se reduce a la suma de
subsistemas m\'as peque\~nos, por lo que los ejercicios aqu\'i presentados son
importantes a modo de introducci\'on.

\begin{figure}[htb]
\centering
\includegraphics[width=0.99\textwidth]{fig-cap8-03-barra-cable}
\caption{Barra homog\'enea unida a una pared por una articulaci\'on y un cable
         inextensible.}
\label{fig:cap8-03-barra-cable}
\end{figure}

\Lafigura~\ref{fig:cap8-03-barra-cable} (izquierda) muestra un sistema en
equilibrio compuesto por una barra horizontal homog\'enea
$\overline{\text{\textsc{ab}}}$ de longitud
$L_{\overline{\text{\textsc{ab}}}}=\SI{3}{\metre}$, sujeta por el extremo $A$ a
una pared mediante una articulaci\'on y mediante un cable inextensible y de masa despreciable que va
del punto $C$ al $D$. El peso de la barra es
$P_{\overline{\text{\textsc{ab}}}}=50\,\text{kp}$, en el extremo $B$ se aplica
una fuerza hacia abajo $F_1=30\,\text{kp}$ y las distancias entre los distintos
puntos del sistema son $L_{\overline{\text{\textsc{ad}}}}=\SI{1}{\metre}$,
$L_{\overline{\text{\textsc{ac}}}}=\SI{2}{\metre}$ y
$L_{\overline{\text{\textsc{cb}}}}=\SI{1}{\metre}$. Se pide calcular las
reacciones vinculares en el punto $A$ que garantizan el equilibrio del sistema.

Lo primero que haremos en este tipo de problemas ser\'a definir el sistema de
coordenadas de trabajo, tal y como se puede ver en
\lafigura~\ref{fig:cap8-03-barra-cable} (superior derecha). Habremos de indicar
los sentidos positivos de los ejes $X$ e $Y$, as\'i como el sentido positivo de
los giros que representar\'an los momentos de los diversos vectores
considerados. A lo largo del problema respetaremos en todo momento el sistema
establecido. El siguiente paso consistir\'a en dibujar el llamado
\textit{diagrama del s\'olido libre},\index{diagrama!del s\'olido libre} el
cual comprende el sistema \textbf{completo} y donde representaremos todas las
fuerzas directamente aplicadas, as\'i como las reacciones en los diversos
v\'inculos que existan, tal y como se estableci\'o en la
secci\'on~\ref{sec:ESV}. Este diagrama se presenta en
\lafigura~\ref{fig:cap8-03-barra-cable} (inferior derecha). Como se puede ver,
se ha representado dicha barra como un segmento horizontal sobre el cual se han
aplicado los siguientes elementos:
\begin{itemize}
\item $P_{\overline{\text{\textsc{ab}}}}$, que es el peso de la barra. Debido a
      la acci\'on de la gravedad apunta hacia abajo (sentido negativo del eje
      $Y$ en nuestro sistema de coordenadas) y su punto de aplicaci\'on es el
      centro de masas de la barra, que coincide con su centro geom\'etrico al
      ser \'esta homog\'enea.
\item $F_1$, que es la fuerza aplicada externamente en el extremo $B$, tal y
      como se indica en los datos del problema. Como se trata de un vector
      deslizante lo aplicamos en el punto $B$, lo que nos facilitar\'a los
      c\'alculos posteriores.
\item $T$, que es la tensi\'on del cable inextensible. En este caso es necesario
      tenerla en cuenta porque el cable une la barra con un elemento externo al
      sistema como es la pared (si un cable une elementos del propio sistema no
      es necesario tener en cuenta la tensi\'on en el diagrama del s\'olido
      libre, ya que al estar en equilibrio se compensan las tensiones de los
      extremos). Con el objetivo de simplificar los c\'alculos es conveniente
      descomponer el vector tensi\'on en sus dos componentes, $T_x$ y $T_y$.
\item $R_{Ax}$ y $R_{Ay}$, que son las componentes de la fuerza de reacci\'on
      vincular en la articulaci\'on del extremo $A$. Como se puede ver en
      \lafigura~\ref{fig:cap8-02-tipos-enlace}, la reacci\'on debida a una
      articulaci\'on es una fuerza de direcci\'on desconocida, que podemos
      descomponer en sus componentes a lo largo de los ejes $X$ e~$Y$. Nos
      encontramos en principio con el problema de que, al tener la reacci\'on
      direcci\'on desconocida, no podemos asegurar los sentidos de dichas
      componentes, pero esto no es realmente un impedimento: si al calcular la
      soluci\'on obtenemos un n\'umero positivo querr\'a decir que hemos
      considerado el sentido correcto, mientras que si es negativo el sentido
      real ser\'a el opuesto al dibujado. En muchos problemas es sencillo
      deducir el sentido de las reacciones, pero en otros no es intuitivo en
      absoluto. En este caso el sentido de $R_{Ax}$ est\'a claro, ya que la
      \'unica fuerza real sobre el eje $X$ es $T_x$ y $R_{Ax}$ ha de tener un
      sentido tal que la anule al estar el sistema en equilibrio.
\end{itemize}

A continuaci\'on pasaremos a aplicar las \ecuacs~(\ref{ec:EqSolRigActReac}). Las
componentes horizontal y vertical de las fuerzas y reacciones son (asumimos que
$\sum F=\sum_{i=1}^K F_i+\sum_{i=1}^{K'} F'_i$)
\begin{equation}
\label{ec:EcuacReacBarra}
\begin{dcases}
\sum F_x=0\rightarrow-T_x+R_{Ax}=0\rightarrow R_{Ax}=T\,\cos\alpha,\\
\sum F_y=0\rightarrow=
-P_{\overline{\text{\textsc{ab}}}}-F_1+T_y-R_{Ay}=0\rightarrow
R_{Ay}=-P_{\overline{\text{\textsc{ab}}}}-F_1+T\,\sin\alpha,
\end{dcases}
\end{equation}
donde hemos puesto las componentes de la tensi\'on en la forma
$T_x=T\,\cos\alpha$ y $T_y=T\,\sin\alpha$, con $\cos\alpha=2/\sqrt{5}$ y
$\sin\alpha=1/\sqrt{5}$, valores que se deducen del tri\'angulo
$\widetriangle{\text{\textsc{dac}}}$. El problema a\'un no ha sido resuelto, ya
que tenemos dos ecuaciones y tres inc\'ognitas, por lo que es necesario aplicar
las ecuaciones de equilibrio de los momentos. El primer paso ser\'a la
elecci\'on de un punto del sistema como polo de momentos, que puede ser
cualquiera, pero que ser\'a conveniente seleccionar de tal manera que
simplifique al m\'aximo los c\'alculos. En este caso elegimos el punto $A$ como
centro de reducci\'on, ya que al estar aplicados sobre \'el las reacciones
$R_{Ax}$ y $R_{Ay}$ sus momentos ser\'an cero. Planteamos entonces la ecuaci\'on
correspondiente (trabajaremos con el m\'odulo con signo del momento total, no
con sus componentes):
\begin{equation}
\label{ec:EcuacMomBarra}
\sum M_A=0\rightarrow
-P_{\overline{\text{\textsc{ab}}}}\,\frac{L_{\overline{\text{\textsc{ab}}}}}{2}-
F_1\,L_{\overline{\text{\textsc{ab}}}}+T_y\,L_{\overline{\text{\textsc{ac}}}}=0
\rightarrow
T=\frac{P_{\overline{\text{\textsc{ab}}}}\,\frac{L_{\overline{\text{\textsc{ab}}}}}{2}+
F_1\,L_{\overline{\text{\textsc{ab}}}}}{L_{\overline{\text{\textsc{ac}}}}\,\sin\alpha},
\end{equation}
donde se ha tenido en cuenta el criterio de signos establecido y de nuevo se
toma $T_y=T\,\sin\alpha$. Vemos tambi\'en que los m\'odulos de los momentos se
reducen a los productos de los m\'odulos de los vectores implicados al estar
trabajando con \'angulos rectos. La componente $T_x$ no genera momento alguno al
estar contenida en la direcci\'on $\overline{\text{\textsc{ca}}}$. Sustituyendo
en la \ecuac~(\ref{ec:EcuacMomBarra}) los datos conocidos obtenemos el valor de
la tensi\'on del cable en $C$, que tiene por valor
\begin{equation}
T=\frac{50\cdot1.5+30\cdot3}{2\cdot\frac{1}{\sqrt{5}}}=
\frac{165\sqrt{5}}{2}\,\text{kp},
\end{equation}
dato que sustituido en las \ecuacs~(\ref{ec:EcuacReacBarra}) lleva a los valores
de las reacciones vinculares en $A$:
\begin{equation}
\begin{dcases}
R_{Ax}=\frac{165\sqrt{5}}{2}\,\frac{2}{\sqrt{5}}=165\,\text{kp},\\
R_{Ay}=-50-30+\frac{165\sqrt{5}}{2}\,\frac{1}{\sqrt{5}}=\frac{5}{2}\,\text{kp}.
\end{dcases}
\end{equation}
Como se puede observar, el valor de $R_{Ay}$ es positivo, luego el sentido que
hab\'iamos elegido en el diagrama del s\'olido libre es correcto.

\begin{figure}[htb]
\centering
\includegraphics[width=0.99\textwidth]{fig-cap8-04-sist-empotrado}
\caption{Conjunto empotrado en el suelo de dos barras homog\'eneas unidas por
         una articulaci\'on y un cable inextensible.}
\label{fig:cap8-04-sist-empotrado}
\end{figure}

El segundo ejemplo consta, como se puede ver en
\lafigura~\ref{fig:cap8-04-sist-empotrado} (superior izquierda), de un sistema
empotrado en el suelo en el punto $A$ y formado por dos barras homog\'eneas
unidas por una articulaci\'on y un cable inextensible y de masa despreciable. La
barra $\overline{\text{\textsc{ab}}}$ tiene un peso
$P_{\overline{\text{\textsc{ab}}}}=\num{30000}\,\text{kp}$, una longitud
$L_{\overline{\text{\textsc{ab}}}}=\SI{15}{\metre}$, una articulaci\'on $C$
situada a $\SI{3}{\metre}$ del extremo $A$ y forma un \'angulo de
$\SI{60}{\degree}$ con la horizontal. Unida a dicha articulaci\'on se encuentra
la barra $\overline{\text{\textsc{cd}}}$, que se mantiene en posici\'on
horizontal gracias a que su extremo $D$ est\'a unido mediante un cable
inextensible al extremo $B$ de la barra $\overline{\text{\textsc{ab}}}$. Dicha
barra tiene un peso $P_{\overline{\text{\textsc{cd}}}}=\num{25000}\,\text{kp}$ y
una longitud $L_{\overline{\text{\textsc{cd}}}}=\SI{12}{\metre}$. Se pide hallar
las fuerzas y los momentos de reacci\'on en los puntos $A$ y $C$ y la tensi\'on
del cable.

El primer paso en la resoluci\'on del problema, tal y como hicimos en el ejemplo
anterior, es dibujar el diagrama del s\'olido libre, mediante el cual trataremos
de determinar las reacciones vinculares en $A$. En este caso (ver
\figura~\ref{fig:cap8-04-sist-empotrado}, superior derecha) la tensi\'on del
cable no se tiene en cuenta al unir \'este dos elementos del sistema. Los
elementos del diagrama del s\'olido libre son:
\begin{itemize}
\item $P_{\overline{\text{\textsc{ab}}}}$ y $P_{\overline{\text{\textsc{cd}}}}$,
      que son los pesos de las barras $\overline{\text{\textsc{ab}}}$ y
      $\overline{\text{\textsc{cd}}}$, respectivamente, y est\'an aplicados en
      sus puntos medios al ser homog\'eneas.
\item $M'_A$, que es el momento de reacci\'on en $A$. Como en este punto hay un
      empotramiento, sus reacciones son una fuerza de direcci\'on desconocida y
      un momento, cuyo giro hemos dibujado en el sentido positivo de nuestro
      sistema de coordenadas, aunque si en el resultado final obtenemos un
      n\'umero negativo querr\'a decir que el giro es en sentido contrario.
\item $R_{Ay}$, que es la reacci\'on vincular en $A$ a lo largo del eje $Y$.
      Como en este caso no hay ninguna fuerza horizontal aplicada, no existe la
      reacci\'on horizontal correspondiente.
\item Los \'angulos $\alpha$, $\beta$ y $\gamma$ tienen por valores
      $\alpha=\SI{60}{\degree}$, $\beta=\SI{30}{\degree}$ y
      $\gamma=\SI{30}{\degree}$. El valor de $\alpha$ es un dato y $\beta$ y
      $\gamma$ son de f\'acil deducci\'on a partir de las medidas de los
      elementos del sistema.
\end{itemize}

Como hemos indicado, no hay componente horizontal de fuerzas, por lo que la
reacci\'on en esta direcci\'on en el punto $A$ tambi\'en ser\'a cero. En cuanto
a la reacci\'on vertical tenemos que
\begin{equation}
\sum F_y=0\rightarrow
-P_{\overline{\text{\textsc{ab}}}}-P_{\overline{\text{\textsc{cd}}}}+
R_{Ay}=0\rightarrow R_{Ay}=
P_{\overline{\text{\textsc{ab}}}}+P_{\overline{\text{\textsc{cd}}}}=
\num{30000}+\num{25000}=\num{55000}\,\text{kp}.
\end{equation}

Para el c\'alculo de momentos elegiremos el punto $A$ como polo. La aplicaci\'on
de la ecuaci\'on de equilibrio da
\begin{equation}
\label{ec:EcMomEmpotramiento}
\begin{split}
&\sum M_A=0\rightarrow
P_{\overline{\text{\textsc{ab}}}}\,\overline{\text{\textsc{a$'$a}}}-
P_{\overline{\text{\textsc{cd}}}}\,\left(\frac{\overline{\text{\textsc{cd}}}}{2}-
\overline{\text{\textsc{ca$''$}}}\right)+M'_A=0\rightarrow\\
&M'_A=-P_{\overline{\text{\textsc{ab}}}}\,\overline{\text{\textsc{a$'$a}}}+
P_{\overline{\text{\textsc{cd}}}}\,\left(\frac{\overline{\text{\textsc{cd}}}}{2}-
\overline{\text{\textsc{ca$''$}}}\right)=
-P_{\overline{\text{\textsc{ab}}}}\,\frac{\overline{\text{\textsc{ab}}}}{2}\,\cos\alpha+
P_{\overline{\text{\textsc{cd}}}}\,\left(\frac{\overline{\text{\textsc{cd}}}}{2}-
\overline{\text{\textsc{ac}}}\sin\gamma\right),
\end{split}
\end{equation}
donde para el c\'alculo de los momentos de $P_{\overline{\text{\textsc{ab}}}}$ y
$P_{\overline{\text{\textsc{cd}}}}$ hemos tenido en cuenta que son vectores
deslizantes y los hemos aplicado de tal modo que el \'angulo que formen con sus
correspondientes brazos sea igual a $\pi/2$. Sustituyendo valores en la
\ecuac~(\ref{ec:EcMomEmpotramiento}) obtenemos
\begin{equation}
M'_A=\num{30000}\cdot\frac{15}{2}\cdot\frac{1}{2}+
\num{25000}\cdot\left(\frac{12}{2}-3\cdot\frac{1}{2}\right)=
\num{112500}-\num{112500}=\SI[per-mode=reciprocal]{0}{\kilogram\per\metre},
\end{equation}
luego el sistema estar\'ia en equilibrio aunque no hubiese empotramiento y
estuviese simplemente apoyado en el suelo en su extremo $A$.

Queda por \'ultimo determinar las reacciones vinculares en la articulaci\'on $C$
y la tensi\'on del cable, para lo cual aplicaremos el principio de
fragmentaci\'on (ver secci\'on~\ref{sec:SVO}). Para ello podemos escoger
cualquiera de los elementos del sistema como fragmento de trabajo, siempre y
cuando contenga los v\'inculos cuyas reacciones queremos calcular. En nuestro
caso podr\'iamos elegir la barra $\overline{\text{\textsc{ab}}}$ o la
$\overline{\text{\textsc{cd}}}$. Trabajaremos con la barra
$\overline{\text{\textsc{cd}}}$, fragmento en el que dibujaremos todas las
fuerzas y momentos implicados, que forman el llamado \textit{diagrama
fragmentado}\index{diagrama!fragmentado} (ver
\figura~\ref{fig:cap8-04-sist-empotrado}, inferior derecha). Los elementos de
trabajo son:
\begin{itemize}
\item $P_{\overline{\text{\textsc{cd}}}}$, que, como en el caso del diagrama del
      s\'olido libre, es el peso de la barra $\overline{\text{\textsc{cd}}}$.
\item $T$, que es la tensi\'on del cable que une los puntos $B$ y $D$, y que se
      descompone en sus componentes $T_x=T\,\cos\delta$ y $T_y=T\,\sin\delta$,
      con $\delta=\SI{30}{\degree}$, valor que se deduce f\'acilmente a partir
      de las medidas dadas en el enunciado del problema. En este caso la
      tensi\'on del cable aparece porque une el fragmento de trabajo con un
      elemento externo a \'el.
\item $R_{Cx}$ y $R_{Cy}$, que son la componentes de la reacci\'on en la
      articulaci\'on $C$, necesarias para producir el equilibrio de la barra
      $\overline{\text{\textsc{cd}}}$.
\end{itemize}

Las ecuaciones de equilibrio ser\'an (seleccionamos $C$ como
polo de momentos):
\begin{equation}
\begin{dcases}
\sum F_x=0\rightarrow-T_x+R_{Cx}=0\rightarrow R_{Cx}=T_x,\\
\sum F_y=0\rightarrow-P_{\overline{\text{\textsc{cd}}}}+T_y+R_{Cy}=0\rightarrow
R_{Cy}=-T_y+P_{\overline{\text{\textsc{cd}}}},\\
\sum M_C=0\rightarrow
-P_{\overline{\text{\textsc{cd}}}}\,\frac{{\overline{\text{\textsc{cd}}}}}{2}+
T_y\,\overline{\text{\textsc{cd}}}=0\rightarrow T_y=
P_{\overline{\text{\textsc{cd}}}}\,\frac{{\overline{\text{\textsc{cd}}}}}{2}\,
\frac{1}{\overline{\text{\textsc{cd}}}},
\end{dcases}
\end{equation}
de donde, sustituyendo datos, obtenemos
\begin{equation}
\begin{dcases}
T_y=\num{25000}\cdot\frac{12}{2}\cdot\frac{1}{12}=\num{12500}\,\text{kp}\rightarrow
T=\frac{T_y}{\sin\SI{30}{\degree}}=\frac{\num{12500}}{1/2}=\num{25000}\,\text{kp},\\
R_{Cy}=-\num{12500}+\num{25000}=\num{12500}\,\text{kp},\\
T_x=R_{Cx}=T\cos\SI{30}{\degree}=
\num{25000}\,\frac{\sqrt{3}}{2}=\num{12500}\,\sqrt{3}\,\text{kp}.
\end{dcases}
\end{equation}
