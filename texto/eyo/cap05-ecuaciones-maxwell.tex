\chapter{Ecuaciones de Maxwell y ondas electromagn\'eticas}
\label{cap:EM}

\section{El campo electromagn\'etico}

El concepto de \textit{campo electromagn\'etico}\index{campo!electromagn\'etico}
es necesario para explicar la denominada \textit{interacci\'on
electromagn\'etica},\index{interacci\'on electromagn\'etica} que es un tipo de
interacci\'on entre las part\'iculas fundamentales que componen la materia. Los
campos el\'ectrico y magn\'etico, como hemos visto en los temas anteriores, son
campos de fuerza debidos al comportamiento de las cargas el\'ectricas y cuya
denominaci\'on depende del estado de movimiento de \'estas. Las cargas
el\'ectricas en reposo originan un campo electrost\'atico,
\index{campo!electrost\'atico} es decir, independiente del tiempo, mientras que
su movimiento ordenado representa una corriente el\'ectrica
\index{corriente!el\'ectrica} que, a su vez, produce un campo de fuerzas
adicional, denominado campo magn\'etico.\index{campo!magn\'etico} Este nuevo
campo se dice que es magnetost\'atico\index{campo!magnetost\'atico} si las
cargas se mueven a velocidad constante respecto al sistema de referencia o punto
de observaci\'on. Por su parte, los movimientos acelerados o de otro tipo
producen campos el\'ectrico y magn\'etico que var\'ian con el tiempo y que se
denominan campos electromagn\'eticos.

La regi\'on en la que reside el campo electromagn\'etico es el dominio de
existencia de los campos vectoriales siguientes:
\begin {itemize}
\item Campo el\'ectrico $\vec{E}$ (unidad:
      $\si[per-mode=reciprocal]{\volt\per\metre}$).\index{campo!el\'ectrico}
\item Desplazamiento el\'ectrico $\vec{D}$ (unidad:
      $\si[per-mode=reciprocal]{\coulomb\per\square\metre}$).
      \index{desplazamiento!el\'ectrico}
\item Inducci\'on magn\'etica $\vec{B}$ (unidad: $\si{\tesla}$).
      \index{inducci\'on!magn\'etica}
\item Campo magn\'etico $\vec{H}$ (unidad:
      $\si[per-mode=reciprocal]{\ampere\per\metre}$).\index{campo!magn\'etico}
\end{itemize}

Si los campos el\'ectrico y magn\'etico existen en un punto $P$ de una regi\'on
del espacio, su presencia puede detectarse f\'isicamente por medio de una carga
$q$ colocada en ese punto y que se mueva a velocidad $\vec{v}$. La fuerza
$\vec{F}$ resultante en esa carga tendr\'a dos componentes: una debida a
$\vec{E}$ y otra a $\vec{B}$, la cuales pueden ser cuantificadas mediante la ley
de Lorentz (\ecuac~(\ref{ec:LeyLorentzEB})).\indnp{Lorentz, Hendrik Antoon}

\section{Ecuaciones de Maxwell}

La conexi\'on de los campos el\'ectrico y magn\'etico con las fuentes de carga y
de corriente que los crean viene determinada por un juego de relaciones
conocidas como \textit{ecuaciones de
Maxwell}\footnote{\href{\biomate{Maxwell}}{James Clerk Maxwell (1831--1879)}.},
\indnp{Maxwell, James Clerk} que sintetizan diversas leyes experimentales
descubiertas por otros cient\'ificos. Estas ecuaciones describen por completo
los fen\'omenos electromagn\'eticos y son las siguientes\footnote{En este exto
trabajaremos con las ecuaciones en su forma integral. Se remite al lector a la
bibliograf\'ia fundamental de la asignatura para verlas en su forma
diferencial.}:
\begin{enumerate}
\item Ley de Gauss\index{ley!de Gauss} para el campo el\'ectrico. Tal y como ya
      vimos en la secci\'on~\ref{sec:FlujoElectrico}, la ley de Gauss para el
      campo el\'ectrico indica que el flujo del campo el\'ectrico $\vec{E}$ que
      atraviesa una superficie cerrada $\Sigma$ en un medio de permitividad
      $\varepsilon$ es igual a la carga total $Q$ encerrada dividida entre
      $\varepsilon$:
      \begin{equation}
      \Phi_{\vec{E}}=\oiint\limits_\Sigma\vec{E}\cdot d\vec{s}=
      \frac{Q}{\varepsilon}.
      \end{equation}
\item Ley de Gauss para el campo magn\'etico. Como tambi\'en vimos en la
      secci\'on~\ref{sec:FMLG}, la ley de Gauss para el campo magn\'etico nos
      indica que el flujo magn\'etico sobre una superficie cerrada es
      \textbf{siempre} igual a cero, lo que significa que el flujo que entra en
      una superficie tal es igual al que sale de la misma:
      \begin{equation}
      \Phi_{\vec{B}}=\oiint\limits_\Sigma\vec{B}\cdot d\vec{s}=0.
      \end{equation}
      Se puede afirmar, entonces, que las l\'ineas de inducci\'on magn\'etica
      son \textbf{siempre} cerradas.\index{inducci\'on!magn\'etica!l\'ineas de}
\item Ley de Faraday-Henry.\index{ley!de Faraday-Henry}\indnp{Faraday, Michael}
      \indnp{Henry, Joseph} En la secci\'on~\ref{sec:LFHL} qued\'o establecido
      que si el flujo magn\'etico a trav\'es del \'area rodeada por un circuito
      var\'ia, se induce una fuerza electromotriz\index{fuerza!electromotriz}
      que es igual a la variaci\'on por unidad de tiempo del flujo que atraviesa
      dicho circuito con el signo cambiado \citep[\pnbs961]{tipler2010b}:
      \begin{equation}
      \varepsilon=\oint\limits_C\vec{E}\cdot d\vec{l}=
      -\frac{d}{dt}\iint\limits_\Sigma\vec{B}\cdot d\vec{s}=
      -\frac{d\Phi_{\vec{B}}}{dt},
      \end{equation}
      donde $C$ es la l\'inea que representa al circuito y $\Sigma$ la
      superficie encerrada por \'el.

      Esta ecuaci\'on nos indica que la fuerza electromotriz se puede definir
      como la energ\'ia cedida por un campo no electrost\'atico (integral
      curvil\'inea) por unidad de carga a lo largo de un circuito cerrado. A su
      vez, esto representa la variaci\'on cambiada de signo del flujo
      magn\'etico que atraviesa la superficie $\Sigma$ encerrada por dicho
      circuito. Se puede decir, por otro lado, que el campo el\'ectrico
      $\vec{E}$ no es conservativo, por lo que no se puede expresar como el
      gradiente de un potencial escalar.\index{campo!el\'ectrico}
\item Ley de Amp\`ere generalizada.\index{ley!de Amp\`ere} En la
      secci\'on~\ref{sec:LA} estudiamos la ley de Amp\`ere en el caso
      \textbf{especial} de campos magn\'eticos inm\'oviles y corrientes
      el\'ectricas estacionarias (sin variaci\'on en el tiempo), pero dicha
      formulaci\'on falla cuando los campos el\'ectrico y/o magn\'etico no
      cumplen las citadas condiciones. En el caso de que el desplazamiento
      el\'ectrico\index{desplazamiento!el\'ectrico} var\'ie con el tiempo la ley
      de Amp\'ere debe ampliarse para tener en cuenta la contribuci\'on al
      campo magn\'etico de la \textit{densidad de corriente de desplazamiento}
      \index{densidad de corriente!de desplazamiento}:
      \begin{equation}
      \vec{J}_d=\frac{\partial\vec{D}}{\partial t}.
      \end{equation}
      Esta densidad de corriente es la contribuci\'on de Maxwell
      \indnp{Maxwell, James Clerk} para hacer compatible la primitiva ley de
      Amp\`ere\index{ley!de Amp\`ere} con el \textit{principio de continuidad de
      la carga},\index{principio!de continuidad de la carga} que es la
      expresi\'on matem\'atica del resultado experimental fundamental de que en
      todo proceso electromagn\'etico la carga neta se conserva
      \citep[\pnbs256]{wangsness1983}. La ecuaci\'on queda, entonces,
      \begin{equation}
      \label{ec:EcMaxwell4}
      \oint\limits_C\vec{B}\cdot d\vec{l}=
      \mu\iint\limits_\Sigma\vec{J}\cdot d\vec{s}+
      \mu\,\varepsilon\iint\limits_\Sigma\frac{d\vec{E}}{dt}\cdot d\vec{s},
      \end{equation}
      donde $\vec{J}$ es la densidad de corriente libre,
      \index{densidad de corriente!libre} que hab\'iamos definido en la
      \ecuac~(\ref{ec:DensCorr}). Si dividimos la \ecuac~(\ref{ec:EcMaxwell4})
      entre $\mu$ obtenemos
      \begin{equation}
      \oint\limits_C\vec{H}\cdot d\vec{l}=
      \iint\limits_\Sigma\vec{J}\cdot d\vec{s}+
      \iint\limits_\Sigma\frac{d\vec{D}}{dt}\cdot d\vec{s}=
      \iint\limits_\Sigma\vec{J}\cdot d\vec{s}+
      \iint\limits_\Sigma\vec{J}_d\cdot d\vec{s}=I+I_d,
      \end{equation}
      donde se han utilizado las relaciones~(\ref{ec:DeE}) y~(\ref{ec:BmH}), que
      veremos en la secci\'on~\ref{sec:CdlM}, $I$ es la intensidad de la
      corriente e $I_d$ es la corriente de desplazamiento total encerrada por la
      trayectoria de integraci\'on
      \citep[\ppnbs426 y\nbs427]{wangsness1983}
\end{enumerate}

Las cuatro ecuaciones de Maxwell, junto con la ley de fuerzas de Lorentz
\index{ley!de fuerzas de Lorentz} y la ley de conservaci\'on de la carga
\index{ley!de conservaci\'on de la carga} constituyen los postulados
fundamentales del electromagnetismo. Son v\'alidas para medios
lineales\footnote{En electromagnetismo un medio es lineal cuando los valores
(tensores) de la conductividad y de la permitividad el\'ectricas son
independientes del m\'odulo del campo el\'ectrico y el valor (tensor) de la
permeabilidad magn\'etica es independiente del m\'odulo del campo magn\'etico.}
y no lineales, is\'otropos\footnote{En electromagnetismo un medio es is\'otropo
cuando los vectores densidad de corriente y campo el\'ectrico son paralelos en
todos los puntos del material. Lo mismo ocurre con los vectores $\vec{D}$ y
$\vec{E}$, y $\vec{B}$ y $\vec{H}$. Los valores de la conductividad y de la
permitividad el\'ectricas son independientes del campo el\'ectrico y el valor de
la permeabilidad magn\'etica es independiente del campo magn\'etico.} y no
is\'otropos.\index{medio!lineal (electromagnetismo)}
\index{medio!is\'otropo (electromagnetismo)}

\section{Caracterizaci\'on de los medios}
\label{sec:CdlM}

Los medios f\'isicos en los que pueden actuar los campos electromagn\'eticos,
los podemos clasificar en conductores, aislantes o diel\'ectricos, y
magn\'eticos. Si el medio es lineal, homog\'eneo e is\'otropo, sus propiedades
se pueden caracterizar de un modo completo introduciendo tres constantes
escalares: conductividad $\sigma$, permitividad $\varepsilon$ y permeabilidad
$\mu$. Existen las siguientes relaciones en cuanto al tipo de medio:
\begin{itemize}
\item En medios conductores
      \begin{equation}
      \vec{J}=\sigma\vec{E}
      \end{equation}
\item En diel\'ectricos
      \begin{equation}
      \label{ec:DeE}
      \vec{D}=\varepsilon\vec{E}
      \end{equation}
\item En medios magn\'eticos
      \begin{equation}
      \label{ec:BmH}
      \vec{B}=\mu\vec{H}
      \end{equation}
\end{itemize}

Tambi\'en se verifica que:
\begin{itemize}
\item En \textbf{medios no lineales} $\vec{J}$ es funci\'on de $\vec{E}$,
      $\vec{D}$ de $\vec{E}$ y $\vec{B}$ de $\vec{H}$.
\item En \textbf{medios anis\'otropos} $\vec{J}$ y $\vec{E}$ no son paralelos,
      como tampoco lo son $\vec{D}$ y $\vec{E}$ ni $\vec{B}$ y $\vec{H}$. En
      este caso las magnitudes escalares $\sigma$, $\varepsilon$ y $\mu$ se
      convierten en matrices de tres por tres (magnitudes tensoriales).
\item En \textbf{medios no homog\'eneos}\footnote{En electromagnetismo un medio
      es homog\'eneo cuando los valores (tensores) de la conductividad y de la
      permitividad el\'ectricas y de la permeabilidad magn\'etica con constantes
      en todos sus puntos.}\index{medio!homog\'eneo (electromagnetismo)} las
      propiedades del medio son diferentes en los  diversos puntos del mismo,
      por lo que en este caso los valores de  $\sigma$, $\varepsilon$ y $\mu$
      son funciones de las coordenadas espaciales.
\end{itemize}

\section{Ondas electromagn\'eticas}
\label{sec:OE}

Una de las consecuencias m\'as importantes de las ecuaciones de Maxwell fue la
predicci\'on de la existencia de las ondas electromagn\'eticas antes de que
Hertz\footnote{\href{\biomate{Hertz_Heinrich}}
{Heinrich Rudolf Hertz (1857--1894)}.}\indnp{Hertz, Heinrich Rudolf} realizara
en $1888$ los experimentos que le llevaron a la comprobaci\'on de la existencia
de las mismas. Las ondas electromagn\'eticas consisten en campos el\'ectricos y
magn\'eticos variables que son soluci\'on de las ecuaciones de Maxwell
\indnp{Maxwell, James Clerk}. Consideraremos el caso m\'as simple de una onda
que se propaga en un medio lineal, homog\'eneo e is\'otropo, que sea aislante
perfecto, es decir, que $\mu$ y $\varepsilon$ son constantes y $\sigma=0$. En
este medio no existen ni cargas libres ($\rho_v=0$) ni corrientes de
conducci\'on ($\vec{J}=0$), por lo que las ecuaciones de Maxwell toman la forma:
\begin{enumerate}
\item Ley de Gauss\index{ley!de Gauss} para el campo el\'ectrico:
      \begin{equation}
      \oiint\limits_\Sigma\vec{E}\cdot d\vec{s}=0.
      \end{equation}
\item Ley de Gauss para el campo magn\'etico:
      \begin{equation}
      \oiint\limits_\Sigma\vec{B}\cdot d\vec{s}=0.
      \end{equation}
\item Ley de Faraday-Henry:\index{ley!de Faraday-Henry}
      \begin{equation}
      \oint\limits_C\vec{E}\cdot d\vec{l}=
      -\frac{d}{dt}\iint\limits_\Sigma\vec{B}\cdot d\vec{s}.
      \end{equation}
\item Ley de Amp\`ere generalizada:\index{ley!de Amp\`ere}
      \begin{equation}
      \oint\limits_C\vec{B}\cdot d\vec{l}=
      \mu\varepsilon\frac{d}{dt}\iint\limits_\Sigma\vec{E}\cdot d\vec{s}.
      \end{equation}
\end{enumerate}

A partir de las expresiones indicadas se pueden obtener las ecuaciones de onda
\index{ecuaci\'on de ondas} (ecuaciones diferenciales de segundo orden) de los
campos el\'ectrico y magn\'etico, de acuerdo a las cuales se deduce que el campo
electromagn\'etico se propaga a una velocidad de m\'odulo
\begin{equation}
\label{ec:VelLuzOtroMedio}
v=\frac{1}{\sqrt{\varepsilon\mu}},
\end{equation}
con los valores de $\varepsilon$ y $\mu$ correspondientes al medio donde estemos
trabajando. Para el caso particular del vac\'io, la velocidad se denota como
\begin{equation}
\label{ec:VelLuz}
c=\frac{1}{\sqrt{\varepsilon_0\mu_0}}.
\end{equation}

En el caso de ondas \textbf{planas} con propagaci\'on en una sola direcci\'on
(el eje $X$, por ejemplo), las ecuaciones de onda de los campos el\'ectrico y
magn\'etico en el vac\'io son
\index{ecuaci\'on de ondas}
\begin{equation}
\begin{dcases}
\frac{\partial^2\vec{E}}{\partial x^2}=\mu_0\varepsilon_0\frac{\partial^2\vec{E}}{\partial t^2},\\
\frac{\partial^2\vec{B}}{\partial x^2}=\mu_0\varepsilon_0\frac{\partial^2\vec{B}}{\partial t^2},
\end{dcases}
\end{equation}
las cuales admiten como soluci\'on particular
\begin{equation}
\begin{dcases}
E_x=0,\,E_y=0,\,E_z=E,\\
B_x=0,\,B_y=B,\,B_z=0,
\end{dcases}
\end{equation}
donde $E$ y $B$ son funciones de $x$ y $t$:
\begin{equation}
\begin{dcases}
E=E\left(t-\frac{x}{c}\right),\\
B=B\left(t-\frac{x}{c}\right).
\end{dcases}
\end{equation}

Si las ondas son arm\'onicas se pueden expresar como
\index{ecuaci\'on de ondas}
\begin{equation}
\label{ec:OndaEM}
\begin{dcases}
\vec{E}=E_0\sin\left[2\pi\left(\frac{t}{T}-\frac{x}{\lambda}\right)\right]=
E_0\sin\left(\omega t-kx\right)\,\vec{k},\\
\vec{B}=B_0\sin\left[2\pi\left(\frac{t}{T}-\frac{x}{\lambda}\right)\right]=
B_0\sin\left(\omega t-kx\right)\,\vec{j},
\end{dcases}
\end{equation}
donde $E_0$ y $B_0$ son las amplitudes de los campos el\'ectrico y magn\'etico
respectivamente, $T$ es el per\'iodo, $\omega$ la frecuencia angular
($\omega=2\pi/T=2\pi\nu$, con $\nu=1/T$), $\lambda$ la longitud de onda y
$k=2\pi/\lambda$ el n\'umero de onda. Otra forma de escribir las
\ecuacs~(\ref{ec:OndaEM}) es
\begin{equation}
\label{ec:OndaEM1}
\vec{E}=
\begin{dcases}
E_x=0,\\
E_y=0,\\
E_z=E_0\sin\left(\omega t-kx\right),
\end{dcases}
\hspace{5mm}
\vec{B}=
\begin{dcases}
B_x=0,\\
B_y=B_0\sin\left(\omega t-kx\right),\\
B_z=0.
\end{dcases}
\end{equation}

Las \ecuacs~(\ref{ec:OndaEM}) y~(\ref{ec:OndaEM1}) representan una onda plana
polarizada linealmente, con un campo el\'ectrico $\vec{E}$ y un campo
magn\'etico $\vec{B}$ oscilando en fase en direcciones perpendiculares entre
s\'i y perpendiculares a la direcci\'on de propagaci\'on, esquema que se muestra
en \lafigura~\ref{fig:cap5-01-onda-elecmag}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.90\textwidth]{fig-cap5-01-onda-elecmag}
\caption{Onda electromagn\'etica plana polarizada linealmente desplaz\'andose en
         en sentido positivo del eje $X$ y con oscilaci\'on del campo
         el\'ectrico seg\'un el eje $Y$ y del magn\'etico seg\'un el eje $Z$.}
\label{fig:cap5-01-onda-elecmag}
\end{figure}

Las amplitudes $E_0$ y $B_0$ no son independientes, sino que est\'an
relacionadas seg\'un la expresi\'on
\begin{equation}
E_0=c\,B_0,
\end{equation}
relaci\'on que se cumple tambi\'en para valores instant\'aneos, por lo que
\begin{equation}
\label{ec:EcB}
E=c\,B,
\end{equation}
ecuaci\'on que pone de manifiesto que los campos el\'ectrico y magn\'etico
est\'an en fase, es decir, que toman valores extremos y nulos al mismo tiempo.

Otra soluci\'on de la ecuaci\'on de ondas es aqu\'ella en la cual los campos
el\'ectrico y magn\'etico tienen una magnitud constante pero \textbf{rotan}
alrededor de la direcci\'on de propagaci\'on, dando como resultado una onda
polarizada circularmente. Las componentes de los campos el\'ectrico y
magn\'etico seg\'un dos ejes perpendiculares se expresan entonces por
\index{ecuaci\'on de ondas}
\begin{equation}
\vec{E}=
\begin{dcases}
E_x=0,\\
E_y=\pm E_0\cos\left(\omega t-kx\right),\\
E_z=E_0\sin\left(\omega t-kx\right),
\end{dcases}
\hspace{5mm}
\vec{B}=
\begin{dcases}
B_x=0,\\
B_y=B_0\sin\left(\omega t-kx\right),\\
B_z=\pm B_0\cos\left(\omega t-kx\right),
\end{dcases}
\end{equation}
que corresponden a un desfase de $\pm\pi/2$ entre las componentes de cada campo,
siendo el campo magn\'etico perpendicular al campo el\'ectrico en cada instante.
Si las amplitudes de las dos ondas componentes ortogonales de cada campo son
distintas se obtiene una polarizaci\'on el\'iptica. Existen adem\'as otras
soluciones de las ecuaciones de Maxwell que son ondas planas pero que no
corresponden a un estado de polarizaci\'on definido.

Como conclusi\'on podemos afirmar que las soluciones en forma de onda plana que
hemos obtenido son completamente generales. Las ondas electromagn\'eticas planas
son transversales, con los campos el\'ectrico y magn\'etico perpendiculares
entre s\'i y a la direcci\'on de propagaci\'on. En cuanto a la direcci\'on y
sentido de propagaci\'on de una onda electromagn\'etica, quedan fijados por el
llamado
\textit{vector de Poynting}\footnote{\href{https://es.wikipedia.org/wiki/John_Henry_Poynting}
{John Henry Poynting (1852--1914)}.\indnp{Poynting, John Henry}}, que se define
como \citep[\pnbs232]{dejuana2003b}\index{vector!de Poynting}
\begin{equation}
\vec{S}=\vec{E}\wedge\vec{H},
\end{equation}
que para un medio de propagación lineal, homog\'eneo e is\'otropo puede
escribirse, de acuerdo a la \ecuac~(\ref{ec:BmH}), como
\begin{equation}
\vec{S}=\frac{\vec{E}\wedge\vec{B}}{\mu}.
\end{equation}
El m\'odulo \textbf{medio} de $\vec{S}$ es la intensidad de la onda
\citep[\pnbs1046]{tipler2010b}.

\section{Densidad de energ\'ia del campo electromagn\'etico}

La densidad de energ\'ia del campo electromagn\'etico
\index{densidad de energ\'ia del campo electromagn\'etico} viene expresada para
el vac\'io, como medio lineal, homog\'eneo e is\'otropo que es, por
\begin{equation}
w=\frac{1}{2}\varepsilon_0E^2+\frac{1}{2\mu_0}B^2.
\end{equation}
Utilizando las \ecuacs~(\ref{ec:VelLuz}) y~(\ref{ec:EcB}) se comprueba que la
mitad de la energ\'ia es el\'ectrica y la mitad magn\'etica, ya que
\begin{equation}
\varepsilon_0E^2=\frac{1}{\mu_0}B^2.
\end{equation}

Teniendo en cuenta las \ecuacs~(\ref{ec:DeE}) y~(\ref{ec:BmH}) la densidad de
energ\'ia del campo electromagn\'etico se puede escribir de la forma
\begin{equation}
w=\frac{1}{2}\vec{E}\cdot\vec{D}+\frac{1}{2}\vec{B}\cdot\vec{H},
\end{equation}
que es la expresi\'on mas general de esta magnitud.

La densidad de energ\'ia de un campo electromagn\'etico est\'atico (esto es, un
campo que no var\'ia con el tiempo) permanece constante. Sin embargo, cuando el
campo depende del tiempo, la energ\'ia electromagn\'etica tambi\'en sufre de la
misma dependencia en cada punto. Las variaciones de un campo electromagn\'etico
en el tiempo dan lugar, como hemos visto, a ondas electromagn\'eticas que se
propagan en el vac\'io con la velocidad indicada en la \ecuac~(\ref{ec:VelLuz}).
Podemos decir que la onda lleva la energ\'ia del campo electromagn\'etico. Esta
energ\'ia transportada por una onda se denomina a veces \textit{radiaci\'on
electromagn\'etica}.\index{radiaci\'on electromagn\'etica}

Como una carga en reposo respecto de un observador produce un campo est\'atico,
la carga no irradia energ\'ia electromagn\'etica. Se puede demostrar tambi\'en
que una carga en movimiento rectil\'ineo y uniforme no irradia energ\'ia
electromagn\'etica porque la energ\'ia total de un campo electromagn\'etico
est\'atico permanece constante. Cuando una carga est\'a en movimiento acelerado
se presenta una situaci\'on totalmente diferente. La energ\'ia total del campo
electromagn\'etico de una carga acelerada var\'ia con el tiempo, por lo que
\'esta s\'i que irradia energ\'ia electromagn\'etica.
