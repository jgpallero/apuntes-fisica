\chapter{El l\'aser}
\label{cL}

\section{Introducci\'on}
\label{sec:Intro}

Las fuentes de luz artificial m\'as comunes se construyen a partir de s\'olidos
sometidos a temperaturas muy elevadas (filamento de wolframio\footnote{Una
exposici\'on de las razones por las cuales algunos autores defienden que el
elemento W deber\'ia llamarse oficialmente wolframio y no tungsteno puede
encontrarse en \cite{roman2000,roman2005}. Desde noviembre de 2016 varias
organizaciones espa\~nolas han consensuado dar prioridad al t\'ermino wolframio
\citep{ciriano2017}.} en una l\'ampara incandescente, por ejemplo) o mediante la
excitaci\'on de gases (arg\'on, ne\'on, vapor de mercurio, etc.) mediante una
corriente el\'ectrica. Gracias a esta energ\'ia t\'ermica o el\'ectrica
suministrada a los \'atomos, \'estos pasan a tener configuraciones de energ\'ia
mayor (\textit{estados excitados})\index{atomo@\'atomo!estado excitado} como
consecuencia de que sus electrones saltan a niveles superiores. Dichos
electrones pueden <<caer>> espont\'aneamente a niveles de energ\'ia menor,
emitiendo como consecuencia fotones y, por lo tanto, radiaci\'on
electromagn\'etica en forma de ondas luminosas.

Al ser la radiaci\'on de los diferentes \'atomos independiente, las ondas que se
producen son incoherentes tanto espacialmente (con fases diferentes)
\index{onda!incoherencia!espacial} como temporalmente (existen diferentes
frecuencias),\index{onda!incoherencia!temporal} ya que la luz generada es la
suma de las radiaciones producidas por todos los \'atomos. Las fases relativas
de las ondas creadas en dos \'atomos arbitrarios tender\'a a cancelar la
radiaci\'on resultante en ciertas direcciones y a aumentarla en otras, variando
\'estas adem\'as con el tiempo. La energ\'ia total ser\'a entonces, y en
t\'ermino medio, radiada uniformemente en todas direcciones. Si quisi\'esemos
obtener luz coherente espacialmente a partir de una fuente de luz ordinaria
ser\'ia necesario utilizar un diafragma (que podr\'ia ser una placa con una
peque\~na abertura circular o una ranura, por ejemplo), mientras que si
necesit\'asemos coherencia temporal utilizar\'iamos un filtro monocrom\'atico.
Para conseguir ambos tipos de coherencia, espacial y temporal, se podr\'ia
emplear una cobinaci\'on de diafragma y filtro. En cualquier caso, la p\'erdida
de potencia con respecto a la suministrada por la fuente original ser\'ia
demasiado grande como para que el m\'etodo fuese pr\'actico
\nocite{rubio1980a}\citep[\pnbs287]{rubio1980b}.
%pongo aquí el \nocite para que en la bibliografía salgan primero el volumen 1 y
%luego el volumen 2

Cuando los \'atomos excitados se encuentran envueltos por una radiaci\'on de la
\textbf{misma} frecuencia que ellos son capaces de emitir se produce la llamada
\textit{emisi\'on estimulada}\index{l\'aser!emisi\'on estimulada}, de tal modo
que la radiaci\'on generada tiene la misma direcci\'on, fase, frecuencia y
polarizaci\'on que la onda excitadora. El dispositivo para obtener estos
resultados se llama l\'aser\index{l\'aser} (del ingl\'es \textit{light
amplification by stimulated emission of radiation}, amplificaci\'on de luz
mediante emisi\'on inducida de radiaci\'on) y con \'el se pueden conseguir
potencias muy altas y grandes longitudes de coherencia
\citep[\pnbs435]{casas1994}. La Real Academia Espa\~nola (RAE) define l\'aser en
su primera acepci\'on\footnote{\url{http://dle.rae.es/?id=MxT0wrP}.} como un
\textit{dispositivo electr\'onico que, basado en una emisi\'on inducida,
amplifica de manera extraordinaria un haz de luz monocrom\'atico y coherente}.

La luz producida por un l\'aser es mucho m\'as intensa, direccional,
monocrom\'atica y coherente que la obtenida por cualquier otra fuenta ordinaria
\citep[\pnbs290]{rubio1980b}. Veamos en detalle estas caracter\'isticas:
\begin{itemize}
\item Gran intensidad: gracias al gran camino recorrido por los fotones en sus
      reflejos en la cavidad resonante (concepto que veremos en lo que sigue) la
      emisi\'on estimulada permite la obtenci\'on de grandes intensidades para
      la radiaci\'on l\'aser.
\item Coherencia: todos los \'atomos emiten sus fotones en fase.
\item Pureza espectral: todos los fotones contenidos en la radiaci\'on tienen la
      misma longitud de onda. Aunque cierto en teor\'ia, en la pr\'actica la
      longitud de onda de un l\'aser tiene una muy peque\~na incertidumbre
      asociada.
\item La luz l\'aser presenta una alta direccionalidad debido a que s\'olo los
      fotones que cumplen con unas ciertas condiciones de alineaci\'on son
      capaces de mantener de manera sostenida los reflejos en la cavidad
      resonante. El haz l\'aser posee tambi\'en una muy reducida divergencia (en
      torno a los milirradianes), aunque \'esta nunca podr\'a ser nula. El valor
      m\'inimo posible para la divergencia del l\'aser es, para aqu\'ellos cuyo
      haz est\'e limitado por la abertura del dispositivo, igual
      a \citep[\pnbs600]{hecht2000}\index{l\'aser!divergencia}
      \begin{equation}
      \theta=2.44\frac{\lambda}{D},
      \end{equation}
      donde $\theta$ es el \'angulo que subtiende el haz, $\lambda$ es la
      longitud de onda y $D$ el di\'ametro de la abertura de salida. La
      ecuaci\'on anterior proporciona un valor doble al de la
      expresi\'on~(\ref{ec:AngDifCirc}), que da la difracci\'on por una abertura
      circular, porque all\'i el \'angulo se mide desde el eje del haz. En los
      dispositivos no limitados por la abertura de salida la divergencia puede
      reducirse a \citep[\pnbs600]{hecht2000}
      \begin{equation}
      \theta=1.27\frac{\lambda}{D_0},
      \end{equation}
      donde $D_0$ es el di\'ametro m\'inimo del haz, que est\'a situado en el
      interior del instrumento.
\end{itemize}

\section{Principios b\'asicos de funcionamiento de los l\'aseres}

Todos los sistemas at\'omicos tienen asociado un nivel de energ\'ia
caracter\'istico, llamado \textit{nivel fundamental},
\index{atomo@\'atomo!nivel de energ\'ia!fundamental} que es el nivel de m\'inima
energ\'ia. Poseen tambi\'en unos \textit{niveles excitados},
\index{atomo@\'atomo!nivel de energ\'ia!excitado} que corresponden a niveles de
energ\'ia mayor al fundamental y en los que los electrones pueden estar de forma
temporal. Una radiaci\'on incidente en un cuerpo cede energ\'ia a la materia,
haciendo pasar a los electrones afectados a niveles de energ\'ia superiores, por
lo que el sistema at\'omico pasar\'a a un nivel excitado, fen\'omeno que se
denomina \textit{absorci\'on}.\index{atomo@\'atomo!absorci\'on} Por su parte, la
transici\'on de un \'atomo de un nivel de energ\'ia superior $E_2$ a otro
inferior $E_1$ se denomina \textit{emisi\'on}.\index{atomo@\'atomo!emisi\'on} En
este proceso se emite mediante un fot\'on radiaci\'on electromagn\'etica de
frecuencia igual a\index{fot\'on}
\begin{equation}
\nu=\frac{E_2-E_1}{h},
\end{equation}
donde $h=\SI{6.626e-34}{\joule\second}$ es la costante de Planck,
\index{constante!de Planck} tal y como vimos en la secci\'on~\ref{sec:NL}.

El fen\'omeno de emisi\'on, como se indic\'o en la secci\'on~\ref{sec:Intro},
se da generalmente de forma espont\'anea (\textit{emisi\'on espont\'anea}),
\index{atomo@\'atomo!emisi\'on!espont\'anea} presentando un car\'acter
totalmente aleatorio. Ahora bien, en presencia de una radiaci\'on
electromagn\'etica de frecuencia apropiada aparece el fen\'omeno de
\textit{emisi\'on estimulada},\index{atomo@\'atomo!emisi\'on!estimulada} en cuyo
caso un fot\'on estimula la <<ca\'ida>> de un electr\'on de un \'atomo excitado
en un nivel de energ\'ia $E_2$ a otro $E_1$ de energ\'ia menor, consiguiendo en
el proceso que se emita un nuevo fot\'on de la misma fase, estado de
polarizaci\'on y direcci\'on que el original; es decir, que tenemos dos fotones
coherentes. Entonces, si m\'as de la mitad de los fotones contin\'uan
estimulando sucesivas emisiones tendremos una reacci\'on en cadena que
provocar\'a una amplificaci\'on de la luz. Algunos fotones ser\'an absorbidos
por los \'atomos que se encuentren en un estado menor de energ\'ia, por lo que
para que la amplificaci\'on tenga lugar ha de haber m\'as \'atomos en el mayor
nivel de energ\'ia que en el menor. Esta situaci\'on se denomina
\textit{inversi\'on de la poblaci\'on},
\index{l\'aser!inversi\'on de la poblaci\'on} ya que es una situaci\'on inversa
a la que corresponde al material en equilibrio para una temperatura dada
\citep[\pnbs289]{rubio1980b}. Un tratamiento m\'as detallado del efecto de
emisi\'on puede verse en \citet[\ppnbs435 a\nbs438]{casas1994}.

Los m\'etodos m\'as utilizados en la pr\'actica para producir la inversi\'on de
la poblaci\'on y poder generar radiaci\'on l\'aser son
\citep[\ppnbs443 a\nbs445]{casas1994}:
\begin{itemize}
\item \textit{Bombeo \'optico}.
      \index{l\'aser!inversi\'on de la poblaci\'on!bombeo \'optico} Supongamos
      una cavidad de paredes transparentes que contiene una sustancia activa con
      tres niveles de energ\'ia $E_0<E_m<E_n$. Si sobre esta cavidad hacemos
      incidir radiaci\'on de frecuencia
      \begin{equation}
      \nu=(E_n-E_0)/h,
      \end{equation}
      los \'atomos que est\'an en $E_0$ pueden pasar al estado $E_n$ por
      absorci\'on de un fot\'on. De forma espont\'anea pueden existir ahora tres
      transiciones de electrones entre los distintos niveles: de $n$ a $0$, de
      $m$ a $0$ o de $n$ a $m$ (ver \figura~\ref{fig:cap10-01-niveles-E}).
      \begin{figure}[htb]
      \centering
      \includegraphics[width=0.75\textwidth]{fig-cap10-01-niveles-E}
      \caption{Bombeo y transiciones espont\'aneas entre tres niveles at\'omicos
               de energ\'ia.}
      \label{fig:cap10-01-niveles-E}
      \end{figure}
      Las probabilidades de transici\'on de electrones entre los distintos
      niveles est\'an gobernados por ciertos valores llamados
      \textit{coeficientes de Einstein},
      \index{atomo@\'atomo!coeficientes de Einstein} cuyos valores rec\'iprocos
      se conocen como \textit{vida media}
      \index{atomo@\'atomo!vida media de un nivel de energ\'ia} del nivel $E$
      correspondiente \citep[\pnbs436]{casas1994}. Si la probabilidad de las
      transiciones de $n$ a $m$ es mucho mayor que las otras dos el nivel de
      energ\'ia $E_m$ se denomina \textit{metaestable}
      \index{atomo@\'atomo!nivel metaestable} y queda enriquecido con respecto a
      $E_0$ y $E_n$, ya que querr\'a decir que los electrones se mantienen
      durante un tiempo relativamente mayor en el nivel $E_m$ que en el $E_n$.
      Se obtiene as\'i la inversi\'on de poblaci\'on.
      \index{l\'aser!inversi\'on de la poblaci\'on} \'Este es el mecanismo que
      se usa en los l\'aseres de rub\'i, aunque hay otras estructuras, como las
      de $4$ niveles, presente en los l\'aseres de neodimio, por ejemplo, que
      exigen menos energ\'ia para el bombeo. En estas condiciones, para iniciar
      la reacci\'on en cadena que permita obtener la amplificaci\'on de la luz
      se necesitar\'a una onda de frecuencia
      \begin{equation}
      \label{ec:num0}
      \nu_{m0}=(E_m-E_0)/h.
      \end{equation}
\item \textit{Excitaci\'on colisional mediante electrones}.
      \index{l\'aser!inversi\'on de la poblaci\'on!excitaci\'on colisional mediante electrones}
      Consiste en la conversi\'on directa de la energ\'ia cin\'etica de los
      electrones en energ\'ia de excitaci\'on. Se usa en los l\'aseres de Ar y
      de $\text{CO}_2$.
\item \textit{Transferencia de excitaci\'on por colisiones inel\'asticas}.
      \index{l\'aser!inversi\'on de la poblaci\'on!transferencia de excitaci\'on por colisiones inel\'asticas}
      En este caso la energ\'ia de excitaci\'on se transfiere desde otro
      elemento en estado excitado (normalmente helio). Se utiliza en l\'aseres
      He-Ne y He-metal.
\item \textit{Excitaci\'on qu\'imica o fotoqu\'imica}.
      \index{l\'aser!inversi\'on de la poblaci\'on!excitaci\'on qu\'imica o fotoqu\'imica}
      Estas t\'ecnicas se aplican en l\'aseres de alta potencia y aprovechan el
      hecho de que en algunos tipos de reacci\'on qu\'imica el producto queda en
      estado excitado, mientras que en otros dicho producto s\'olo es estable en
      este estado.
\end{itemize}

\section{Tipos de l\'aser}

En esta secci\'on veremos dos tipos fundamentales de l\'aser: los l\'aseres de
s\'olidos\index{l\'aser!de s\'olido} y los de gas.\index{l\'aser!de gas} El
primer dispositivo fue construido por Theodore
Maiman\footnote{\href{https://es.wikipedia.org/wiki/Theodore_Harold_Maiman}
{Theodore Harold Maiman (1927--2007)}.}\indnp{Maiman, Theodore Harold} en 1960 y
pertenece al tipo de l\'aseres s\'olidos.
\index{l\'aser!de s\'olido!l\'aser de rub\'i} Consta de una barra cil\'indrica
de rub\'i, que es \'oxido de aluminio ($\text{Al}_2\text{O}_3$) con impurezas de
\'oxido de cromo ($\text{Cr}_2\text{O}_3$) en una proporci\'on en peso de
aproximadamente el $0.05\%$ \citep[\pnbs596]{hecht2000}. Las caras planas del
cilindro, que han de ser talladas lo m\'as perpendicularmente posible a su
generatriz, est\'an recubiertas de plata de tal forma que una de ellas es
\textbf{totalmente} reflectora, mientras que la otra lo es s\'olo
\textbf{parcialmente} (en torno al $95\%$). La barra de rub\'i est\'a rodeada
por una l\'ampara helicoidal de descarga que emite un destello de luz, que
ser\'a la fuente de energ\'ia utilizada para provocar la inversi\'on de la
poblaci\'on.\index{l\'aser!inversi\'on de la poblaci\'on}
\begin{figure}[htb]
\centering
\includegraphics[width=0.86\textwidth]{fig-cap10-02-laser-rubi}
\caption{Esquema de un l\'aser de rub\'i (izquierda) y principio de
         funcionamiento (derecha).}
\label{fig:cap10-02-laser-rubi}
\end{figure} Dicha l\'ampara est\'a conectada a una bater\'ia y todo el montaje
se aloja en un cilindro de aluminio (ver \figura~\ref{fig:cap10-02-laser-rubi},
izquierda\footnote{Imagen tomada de
\href{https://commons.wikimedia.org/wiki/File:Ruby_laser.jpg}
{Wikimedia Commons}, etiquetada de dominio p\'ublico.}). El l\'aser de rub\'i es
un l\'aser de tres niveles, $E_0<E_m<E_n$ \citep[\pnbs597]{hecht2000}: con los
destellos de la l\'ampara de descarga los electrones de los \'atomos de cromo
pasan del estado $E_0$ al $E_n$ para, poco tiempo despu\'es, caer al estado
metaestable $E_m$,\index{atomo@\'atomo!nivel metaestable} donde permanecen
alrededor de unos $\SI{5}{\milli\second}$, tiempo suficiente para que se
produzca la inversi\'on de la poblaci\'on.
\index{l\'aser!inversi\'on de la poblaci\'on} A continuaci\'on algunos \'atomos
comienzan a pasar del estado $E_m$ al $E_0$ emitiendo cada uno un fot\'on de
frecuencia $\nu_{m0}$ (\ecuac~(\ref{ec:num0})), los cuales, al encontrarse con
otros \'atomos de cromo, estimular\'an la emisi\'on de m\'as fotones con su
misma frecuencia, direcci\'on de propagaci\'on y fase. Los fotones cuya
trayectoria no sea paralela al eje del cilindro saldr\'an fuera de \'el y se
perder\'an, mientras que los que tengan trayectorias que s\'i lo sean
comenzar\'an a rebotar en las superficies especulares extremas del cilindro e
ir\'an estimulando nuevas ca\'idas de $E_m$ a $E_0$ y produciendo la emisi\'on
de m\'as fotones. De este modo, los extremos del cilindro de rub\'i forman una
\textit{cavidad \'optica}\index{l\'aser!cavidad \'optica} o \textit{resonador},
\index{l\'aser!resonador|see{cavidad \'optica}} donde se encierra una onda
estacionaria\footnote{Las dimensiones de la cavidad resonante son importantes
para que la onda tenga la frecuencia adecuada.}. Al encontrarse con la cara
semirreflectante del cilindro parte de la onda estacionaria se transmite hacia
el exterior formando un haz l\'aser de tono rojo de $\SI{694.3}{\nano\metre}$,
color debido a la fuerte absorci\'on del azul y el verde por parte de los iones
de cromo \citep[\pnbs1084]{tipler2010b}. Como la luz de la l\'ampara de descarga
no es continua, sino que se aplica mediante destellos, el haz l\'aser tampoco lo
es, sino que se manifiesta en pulsos de unos pocos milisegundos de duraci\'on.
El proceso se esquematiza en \lafigura~\ref{fig:cap10-02-laser-rubi} (derecha).
Los l\'aseres de rub\'i y los de neodimio (tambi\'en de estado s\'olido), se
utilizan en la medida de distancias a sat\'elites\footnote{Para el seguimiento a sat\'elites con l\'aseres de rub\'i se consiguen pulsos con
duraciones de entre $\SI{10}{\nano\second}$ y $\SI{50}{\nano\second}$ y
potencias pico de hasta $\SI{1}{\giga\watt}$ \citep[\pnbs412]{seeber2003}; con
los de neodimio, los pulsos pueden llegar a ser a\'un m\'as cortos, de entre
$\SI{30}{\pico\second}$ y $\SI{200}{\pico\second}$
\citep[\ppnbs202 y\nbs203]{montenbruck2000}.} (SLR, de \textit{satellite laser
ranging}) para el estudio de las mareas (oce\'anicas y terrestres), de la
rotaci\'on y el campo gravitarorio de la Tierra, etc. Este tipo de dispositivos
tambi\'en se utilizan en la medida de distancias a la Luna  (LLR, de
\textit{lunar laser ranging}), empleadas en estudios de rotaci\'on de la Tierra,
en la determinaci\'on de algunos par\'ametros del sistema Tierra-Luna, etc. (ver
\citet[cap\'itulo\nbs8]{seeber2003} para obtener informaci\'on detallada sobre
sus aplicaciones m\'as importantes en el \'ambito de la Geodesia).

El otro grupo importante de l\'aseres que vamos a estudiar es el compuesto por
los dispositivos que utilizan un gas. El m\'as com\'un es el l\'aser de
helio-ne\'on (l\'aser de He-Ne),\index{l\'aser!de s\'olido!l\'aser de He-Ne} en
el cual una mezcla de estos dos elementos, generalmente un $15\%$ de He y un
$85\%$ de Ne \citep[\pnbs1086]{tipler2010b}, es la generadora de la radiaci\'on.
\begin{figure}[htb]
\centering
\includegraphics[width=0.82\textwidth]{fig-cap10-03-laser-hene}
\caption{Esquema de un l\'aser de He-Ne.}
\label{fig:cap10-03-laser-hene}
\end{figure}
Como muestra\footnote{Imagen tomada de
\href{https://commons.wikimedia.org/wiki/File:Hene-1.png}{Wikimedia Commons},
con licencia original Creative Commons BY-SA 3.0.}
\lafigura~\ref{fig:cap10-03-laser-hene}, el sistema est\'a compuesto por un tubo
que contiene el gas, en cuyos extremos se acoplan dos espejos, uno plano
reflectante al $100\%$ y otro c\'oncavo reflectante al $99\%$ (por el que
saldr\'a el haz), los cuales forman la cavidad de resonancia. El espejo
c\'oncavo enfoca la luz paralela al eje de la cavidad resonante en el espejo
plano, haciendo que la alineaci\'on de ambos sea \textbf{menos} cr\'itica que en
el caso del l\'aser de rub\'i \citep[\pnbs1086]{tipler2010b}. Al dispositivo se
le aplica una corriente el\'ectrica que recorre el gas de modo que la colisi\'on
de los electrones de la corriente con los \'atomos de He hacen que \'este se
excite hasta un nivel $E_{1\,\text{He}}$ por encima de su nivel fundamental
$E_{0\,\text{He}}$. Los \'atomos excitados de He colisionan con los de Ne
excit\'andolos hasta un nivel $E_{2\,\text{Ne}}$ y produci\'endose la
inversi\'on de la poblaci\'on.\index{l\'aser!inversi\'on de la poblaci\'on} La
radiaci\'on l\'aser, de longitud de onda igual a $\SI{632.8}{\nano\metre}$ (rojo
brillante) se genera entonces pr\'acticamente de forma inmediata al descender
los electrones a un nivel menos energ\'etico $E_{1\,\text{Ne}}$ y producirse la
emisi\'on estimulada. Acto seguido los electrones de los \'atomos de Ne caen al
nivel de m\'inima energ\'ia $E_{0\,\text{Ne}}$. Vemos, pues, que el de He-Ne es
un l\'aser de cuatro niveles \citep[\ppnbs1085 a\nbs1086]{tipler2010b}. Este
tipo de l\'aser se utiliza por ejemplo en el distanci\'ometro de alta
precisi\'on Kern Mekometer ME5000 \citep[\ppnbs145 a\nbs147]{rueger1996} y en la
medida interferom\'etrica de distancias en grav\'imetros absolutos de ca\'ida
libre \citep[\pnbs133]{torge1989}.

\section{Seguridad en el trabajo con dispositivos l\'aser}

Debido a su alta concentraci\'on de potencia los l\'aseres son dispositivos
peligrosos (potencias de $\SI{1}{\milli\volt}$ pueden producir da\~nos
irreversibles en el ojo), por lo cual es imprescindible adoptar ciertas medidas
de seguridad al manejarlos. Para ello se ha establecido una clasificaci\'on
atendiendo a su peligrosidad y posibles riesgos derivados de un mal manejo del
dispositivo. Dicha clasificaci\'on se recoge en la norma UNE-EN~60825-1:2015,
publicada en su versi\'on en castellano por la Agencia Espa\~nola de
Normalizaci\'on y Certificaci\'on. Dicha norma \textbf{no} es de acceso
gratuito\footnote{Puede accederse libremente al \'indice de la norma a trav\'es
de \url{https://www.une.org/encuentra-tu-norma/busca-tu-norma/norma?c=N0054667},
enlace <<Ver parte del contenido de la norma>>.}, por lo que a continuaci\'on se
da un resumen de las clases de l\'aser correspondientes a la norma
antigua\footnote{Las clasificaciones correspondientes a la norma
UNE-EN~60825-1/A2:2002 pueden consultarse en un documento preparado en su d\'ia
por el Ministerio de Trabajo y Asuntos Sociales, a\'un accesible en
\url{http://www.insht.es/InshtWeb/Contenidos/Documentacion/FichasTecnicas/NTP/Ficheros/601a700/ntp_654.pdf}.
Se recomienda tambi\'en consultar \url{http://www.sprl.upv.es/IOP\_RF\_01\%28a\%29.htm}.}
UNE-EN~60825-1/A2:2002:\index{l\'aser!clases}
\begin{itemize}
\item Clase 1: Seguros en condiciones razonables de utilizaci\'on.
\item Clase 1M: Como la clase 1, pero no seguros cuando se miran a trav\'es de
      instrumentos \'opticos como lupas o binoculares.
\item Clase 2: L\'aseres visibles (longitudes de onda entre
      $\SI{400}{\nano\metre}$ y $\SI{700}{\nano\metre}$). Los reflejos de
      aversi\'on protegen el ojo aunque se utilicen con instrumentos \'opticos.
\item Clase 2M: Como la clase 2, pero no seguros cuando se utilizan instrumentos
      \'opticos.
\item Clase 3R: L\'aseres cuya visi\'on directa es potencialmente peligrosa pero
      el riesgo es menor y necesitan menos requisitos de fabricaci\'on y medidas
      de control que la clase 3B.
\item Clase 3B: La visi\'on directa del haz es siempre peligrosa, mientras que
      la reflexi\'on difusa es normalmente segura.
\item Clase 4: La exposici\'on directa de ojos y piel siempre es peligrosa y la
      reflexi\'on difusa normalmente tambi\'en. Pueden originar incendios.
\end{itemize}

Todo el instrumental que utilice un l\'aser para su funcionamiento ha de indicar
en un lugar visible de su carcasa y en la documentaci\'on y manuales la clase a
la que pertenece. \Lafigura~\ref{fig:cap10-04-clase-laser} muestra dos capturas
de los folletos publicitarios de los sistemas l\'aser esc\'aner Trimble TX8
(superior) y Leica ScanStation P30/P40 (inferior) donde se muestra la
informaci\'on acerca de la clase de los dispositivos utilizados.
\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{fig-cap10-04-clase-laser-1}\\
\vspace{2mm}
\includegraphics[width=0.55\textwidth]{fig-cap10-04-clase-laser-2}
\caption{Informaci\'on (proveniente de los folletos publicitarios) de la clase
         de l\'aser empleado en los instrumentos Trimble TX8 (superior) y Leica
         ScanStation P30/P40 (inferior).}
\label{fig:cap10-04-clase-laser}
\end{figure}
