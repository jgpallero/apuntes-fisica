\chapter{Inducci\'on electromagn\'etica}
\label{cap:IE}

\section{Fen\'omenos de inducci\'on magn\'etica}

Se llama \textit{inducci\'on magn\'etica}\index{inducci\'on!magn\'etica} al
conjunto de procesos que experimenta un cuerpo conductor situado en una regi\'on
en la que existe un campo magn\'etico $\vec{B}$ cuando el flujo de \'este que
atraviesa al cuerpo \textbf{var\'ia} a lo largo del tiempo. Por un lado, se
establece sobre el conductor una diferencia de potencial inducida, denominada
\textit{fuerza electromotriz}\index{fuerza!electromotriz} (simbolizada como
$\varepsilon$ o abreviada como \textit{fem}), que produce una corriente
el\'ectrica inducida\index{corriente!inducida} sobre el mismo. Como consecuencia
de la aparici\'on de dicha corriente se genera un campo magn\'etico inducido
\index{campo!magn\'etico!inducido} $\vec{B}_\text{ind}$ en el entorno del cuerpo
conductor \textbf{diferente} del campo magn\'etico original $\vec{B}$.

Veamos algunos ejemplos de fen\'omenos de inducci\'on magn\'etica:
\begin{itemize}
\item Sea una bobina por la que \textbf{no} circula corriente. Si acercamos un
      im\'an a la bobina se observa que aparece una corriente en ella.
      Inicialmente, el campo creado por el im\'an en la regi\'on en la que
      est\'a situada la bobina es nulo y, por tanto, el flujo de  campo
      magn\'etico que la atraviesa (o el n\'umero de l\'ineas de campo que
      atraviesa la superficie limitada por la ella) es nulo. Al acercar el
      im\'an el campo aumenta y el flujo que atraviesa la bobina tambi\'en. Esto
      crea una \textit{fem} sobre la bobina que produce la corriente inducida.
      Si dejamos de mover el im\'an el flujo que atraviesa la bobina
      permanecer\'a constante y la corriente inducida desaparecer\'a.
\item El mismo efecto se produce si se sustituye el im\'an del ejemplo anterior
      por una segunda bobina por la que circula una corriente el\'ectrica y que
      se aproxime a la primera. La intensidad que circula por la bobina
      generar\'a un campo magn\'etico, que ser\'a el que inducir\'a la corriente
      en la bobina fija. En el momento en que dejamos de acercar la bobina el
      flujo magn\'etico no var\'ia, por lo que la corriente inducida desaparece.
\item Si mantenemos dos bobinas fijas y hacemos pasar una corriente
      \textbf{variable} en el tiempo por una de ellas, observamos que empieza a
      circular una corriente inducida por la otra.
\end{itemize}
La descripci\'on matem\'atica que explica el fen\'omeno de inducci\'on
magn\'etica viene dada por la leyes de Faraday-Henry y de Lenz, que fueron
deducidas de manera experimental.\index{ley!de Faraday-Henry}\index{ley!de Lenz}

\section{Leyes de Faraday-Henry y de Lenz}
\label{sec:LFHL}

La\indnp{Faraday, Michael} \textit{ley de
Faraday-Henry}\footnote{\href{https://es.wikipedia.org/wiki/Joseph_Henry}{Joseph Henry (1797--1878)}.}
\indnp{Henry, Joseph} (a veces indicada simplemente como
ley de Faraday)\index{ley!de Faraday-Henry}
\index{ley!de Faraday|see{ley de Faraday-Henry}} establece que si el flujo
magn\'etico a trav\'es del \'area rodeada por un circuito var\'ia a lo largo del
tiempo, se induce una fuerza electromotriz\index{fuerza!electromotriz} que es
igual a la variaci\'on por unidad de tiempo del flujo que atraviesa dicho
circuito con el signo cambiado \citep[\pnbs961]{tipler2010b}:
\begin{equation}
\label{ec:LeyFHL}
\varepsilon=-\frac{d\Phi_{\vec{B}}}{dt}.
\end{equation}
El signo menos indica que el sentido de la fuerza electromotriz inducida en el
circuito es tal que genera un campo magn\'etico inducido
\index{campo!magn\'etico!inducido} que se \textbf{opone} al cambio de flujo
magn\'etico\index{campo!magn\'etico!flujo} que lo produce.
\Lafigura~\ref{fig:cap4-04-01-iman-faraday-henry} muestra un im\'an en
movimiento, cuyo campo magn\'etico atraviesa una espira conductora de superficie
$S$. De esta forma, el flujo magn\'etico a trav\'es del circuito var\'ia,
gener\'andose entonces un campo el\'ectrico no conservativo $\vec{E}$, del cual
deriva, a trav\'es del trabajo realizado sobre las cargas m\'oviles, una fuerza
electromotriz. Entonces, teniendo en cuenta la parte de la
\ecuac~(\ref{ec:FElectromotriz}) correspondiente al campo no conservativo
tenemos que
\begin{equation}
\varepsilon=\oint\limits_C\vec{E}\cdot d\vec{L}=
-\frac{d}{dt}\iint\limits_S\vec{B}\cdot d\vec{s}=-\frac{d\Phi_{\vec{B}}}{dt},
\end{equation}
donde $L$ es el circuito de la espira y $S$ la superficie que encierra.
\begin{figure}[htb]
\centering
\includegraphics[width=0.80\textwidth]{fig-cap4-01-iman-faraday-henry}
\caption{Cuando el im\'an se mueve hacia la derecha el flujo magn\'etico que
         atraviesa la espira var\'ia, gener\'andose un campo el\'ectrico.}
\label{fig:cap4-04-01-iman-faraday-henry}
\end{figure}

El signo negativo presente en la \ecuac~(\ref{ec:LeyFHL}) es reflejo de la ley
de Lenz\footnote{\href{https://es.wikipedia.org/wiki/Heinrich_Lenz}
{Heinrich Friedrich Emil Lenz (1804--1865)}.},\index{ley!de Lenz}
\indnp{Lenz, Heinrich Friedrich Emil} que indica que la direcci\'on de todo
efecto de inducci\'on magn\'etica\index{inducci\'on!magn\'etica} es la que se
opone a la causa. Otra manera de formularla es la siguiente: cuando se produce
una variaci\'on del flujo magn\'etico $\Phi_{\vec{B}}$ que atraviesa una
superficie, el campo magn\'etico debido a la corriente inducida genera a su vez
un flujo magn\'etico sobre la misma superficie que se opone a dicha variaci\'on
\citep[\pnbs965]{tipler2010b}. \Lafigura~\ref{fig:cap4-02-signos-lenz} muestra
los distintos signos que puede tomar la fuerza electromotriz
\index{fuerza!electromotriz} generada sobre un circuito por un campo
magn\'etico.
\begin{figure}[htb]
\centering
\includegraphics[width=0.43\textwidth]{fig-cap4-02-signos-lenz}
\caption{Fuerza electromotriz y campo magn\'etico inducido generados en un
         circuito por un campo magn\'etico variable. En este caso la variaci\'on
         de $\vec{B}$ se consigue subiendo y bajando el circuito.}
\label{fig:cap4-02-signos-lenz}
\end{figure}

Recordando que el flujo magn\'etico\index{campo!magn\'etico!flujo} para una
espira es (\ecuac~(\ref{ec:FlujoMagUnaEspira}))
\begin{equation}
\Phi_{\vec{B}}=B\,S\cos\theta,
\end{equation}
donde $\theta$ es el \'angulo que forma el vector normal a la superficie $S$
con el campo magn\'etico $\vec{B}$, podr\'a conseguirse su variaci\'on de tres
formas distintas:
\begin{enumerate}
\item Variando el m\'odulo $B$ del campo magn\'etico, es decir, variando el
      valor de la inducci\'on magn\'etica a trav\'es de la superficie del
      circuito (por ejemplo acercando o alejando un im\'an o, en un
      electroim\'an, variando el campo $\vec{B}$ creado a trav\'es de la
      variaci\'on de la corriente $\vec{I}$ que lo crea).
      \Lafigura~\ref{fig:cap4-04-01-iman-faraday-henry} muestra
      esquem\'aticamente el efecto del acercamiento de un im\'an al circuito, el
      cual aumentar\'a el flujo magn\'etico a trav\'es de ella, por lo que
      aparecer\'an una fuerza electromotriz y una corriente (flecha roja)
      inducidas.
\item Variando el tama\~no de la superficie $S$ del circuito que es atravesado
      por las l\'ineas del campo magn\'etico, efecto que se muestra en
      \lafigura~\ref{fig:cap4-03-variac-superficie}, donde al mover la varilla
      hacia la derecha aumentar\'a el flujo magn\'etico entrante a trav\'es del
      circuito rectangular, con lo que aparecer\'an en \'el una fuerza
      electromotriz y una corriente (flechas rojas) inducidas.
      \begin{figure}[htb]
      \centering
      \includegraphics[width=0.34\textwidth]{fig-cap4-03-variac-superficie}
      \caption{Variaci\'on de la superficie de un circuito.}
      \label{fig:cap4-03-variac-superficie}
      \end{figure}
\item\label{p3FlujoM} Variando el \'angulo $\theta$ que forma el campo
      magn\'etico con el vector perpendicular a la superficie, por ejemplo
      haciendo girar el propio circuito dentro de $\vec{B}$.
      \Lafigura~\ref{fig:cap4-04-giro-circuito} muestra c\'omo al girar la
      espira alrededor de su eje cambiar\'a el flujo magn\'etico a trav\'es del
      circuito rectangular, con lo que aparecer\'an en \'el una fuerza
      electromotriz y una corriente inducidas.
      \begin{figure}[htb]
      \centering
      \includegraphics[width=0.19\textwidth]{fig-cap4-04-giro-circuito}
      \caption{Variaci\'on del \'angulo de un circuito con respecto a
               $\vec{B}$.}
      \label{fig:cap4-04-giro-circuito}
      \end{figure}
\end{enumerate}

Se denomina
\textit{fuerza electromotriz de movimiento}
\index{fuerza!electromotriz!de movimiento} a la fuerza electromotriz inducida
por el movimiento de un conductor en un campo magn\'etico $\vec{B}$. Dada esta
fuerza electromotriz inducida $\varepsilon_i$ y conociendo la resistencia $R$
del circuito, la intensidad inducida $I_i$ se calcula como
\begin{equation}
I_i=\frac{\varepsilon_i}{R}.
\end{equation}

\section{Autoinducci\'on e inducci\'on mutua}

Si por un circuito circula una corriente \textbf{de intensidad variable en el
tiempo},\index{corriente!de intensidad variable} entonces puede inducir una
fuerza electromotriz sobre s\'i mismo, fen\'omeno se denomina
\textit{autoinducci\'on}.\index{autoinducci\'on} Seg\'un la ley de Biot-Savart
\index{ley!de Biot-Savart} (ver secci\'on \ref{sec:CEFCM}), la corriente
el\'ectrica producir\'a un campo magn\'etico, el cual variar\'a en la medida en
que lo haga la intensidad que, seg\'un las condiciones iniciales impuestas, es
variable con el tiempo. Por lo tanto, la variaci\'on del campo magn\'etico
generado producir\'a un cambio en el flujo magn\'etico y seg\'un la ley de
Faraday-Henry\index{ley!de Faraday-Henry} podremos escribir
\begin{equation}
\varepsilon=-\frac{d\Phi_{\vec{B}}}{dt}=
-\frac{d\Phi_{\vec{B}}}{dI}\frac{dI}{dt}=-L\frac{dI}{dt},
\end{equation}
donde $L=d\Phi_{\vec{B}}/dI$ se denomina \textit{coeficiente de
autoinducci\'on}.\index{coeficiente!de autoinducci\'on}
\index{autoinducci\'on!coeficiente de|see{coeficiente de autoinducci\'on}} El
coeficiente de autoinducci\'on depende de la geometr\'ia del circuito (forma y
tama\~no), pero \textbf{no} de la intensidad de la corriente el\'ectrica, y su
unidad en el SI es el henrio,\index{unidades!henrio} cuyo s\'imbolo es
$\si{\henry}$ ($\SI{1}{\henry}=\SI[per-mode=reciprocal]{1}{\weber\per\ampere}$).
El sentido de la fuerza electromotriz autoinducida
\index{fuerza!electromotriz!autoinducida} es tal que se crea un campo
magn\'etico inducido que se opone a la variaci\'on de intensidad de corriente.

Sea un solenoide\index{solenoide} de longitud $l$ y $N$ vueltas por el que
circula una corriente el\'ectrica de intensidad $I$ variable con el tiempo (ver
\figura~\ref{fig:cap4-05-solenoide-autoinduccion}). El flujo magn\'etico
\index{campo!magn\'etico!flujo} en su interior ser\'a, seg\'un las
\ecuacs~(\ref{ec:FlujoMagUnaNEspiras}) y~(\ref{ec:BSolenoideCentro}),
\begin{equation}
\Phi_{\vec{B}}=\frac{\mu\,N^2\,I\,S}{l}=\mu\,n^2\,l\,I\,S,
\end{equation}
donde $n=N/l$ y $S$ es la superficie de las espiras. El coeficiente de
autoinducci\'on es, entonces,\index{coeficiente!de autoinducci\'on}
\begin{equation}
\label{ec:LSolenoide}
L=\frac{d\Phi_{\vec{B}}}{dI}=
\frac{d}{dI}\left(\mu\,n^2\,l\,I\,S\right)=\mu\,n^2\,l\,S,
\end{equation}
de donde tambi\'en se deduce que $\Phi_{\vec{B}}=L\,I$.
\begin{figure}[htb]
\centering
\includegraphics[width=0.39\textwidth]{fig-cap4-05-solenoide-autoinduccion}
\caption{Solenoide de longitud $l$, $N$ vueltas y superficie transversal $S$ por
         el que circula una corriente el\'ectrica de intensidad $I$.}
\label{fig:cap4-05-solenoide-autoinduccion}
\end{figure}

Un solenoide con muchas vueltas ($n$ grande) posee una gran autoinducci\'on, por
lo que se le llama \textit{inductor}.\index{inductor} La diferencia de potencial
entre los extremos de un inductor es\index{inductor!diferencia de potencial}
\begin{equation}
\Delta V=\varepsilon-I\,r=-L\frac{dI}{dt}-I\,r,
\end{equation}
donde $I$ es la intensidad de la corriente circulante y $r$ la resistencia
interna del inductor.\index{inductor!resistencia interna}

Sean dos circuitos $1$ y $2$, situados uno cerca del otro. Si por el circuito
$1$ circula una corriente el\'ectrica variable con el tiempo $I_1(t)$, adem\'as
de autoinducirse una fuerza electromotriz sobre el propio circuito $1$ tambi\'en
se inducir\'a una sobre el circuito $2$. La raz\'on es que el flujo del campo
magn\'etico creado por el circuito $1$ a trav\'es del circuito $2$ ser\'a
variable con el tiempo. El flujo $\Phi_{\vec{B}_{12}}$ es proporcional al campo
creado por el circuito $1$ sobre el circuito $2$, $\vec{B}_{12}$, y a la
corriente $I_1$ que pasa por el circuito $1$, esto es,
\begin{equation}
\Phi_{\vec{B}_{12}}\propto\vec{B}_{12}\propto I_1,
\end{equation}
donde el factor del proporcionalidad se denomina \textit{coeficiente de
inducci\'on mutua}\index{coeficiente!de inducci\'on mutua} $M_{12}$, luego
\begin{equation}
\Phi_{\vec{B}_{12}}=M_{12}\,I_1.
\end{equation}
La fuerza electromotriz inducida sobre el circuito $2$, denominada
$\varepsilon_2$, ser\'a
\begin{equation}
\varepsilon_2=-\frac{d\Phi_{\vec{B}_{12}}}{dt}=
-\frac{d}{dt}\left(M_{12}\,I_1\right)=
-M_{12}\frac{dI_1}{dt}.
\end{equation}

En el planteamiento desarrollado hasta aqu\'i se considera un circuito que
genera un campo e induce una corriente sobre otro. Se puede realizar el mismo
razonamiento intercambiando el papel desempe\~nado por los circuitos $1$ y $2$,
donde las f\'ormulas resultantes ser\'an las mismas, pero intercambiando los
sub\'indices $1$ y $2$. Se comprueba experimentalmente que $M_{12}=M_{21}$.

\section{Energ\'ia magn\'etica}

Al igual que un condensador almacena energ\'ia el\'ectrica, un inductor
\index{inductor} almacena energ\'ia magn\'etica.\index{energ\'ia!magn\'etica}
Ve\'amoslo analizando el caso de un circuito formado por (ver
\figura~\ref{fig:cap4-06-circuito-energia-magnetica}):
\begin{enumerate}
\item Un generador de fuerza electromotriz\index{fuerza!electromotriz!generador}
      $\varepsilon$ y resistencia interna despreciable.
\item Un inductor\index{inductor} $L$ de resistencia interna despreciable.
\item Una resistencia $R$ (del resto del circuito).
\item Un interruptor $S$.
\end{enumerate}
\begin{figure}[htb]
\centering
\includegraphics[width=0.45\textwidth]{fig-cap4-06-circuito-energia-magnetica}
\caption{Circuito con generador $(\varepsilon,r\approx0)$, inductor $L$,
         resistencia $R$ e interruptor $S$.}
\label{fig:cap4-06-circuito-energia-magnetica}
\end{figure}

Al cerrar el interruptor circula una corriente $I$ por el circuito, apareciendo
una ca\'ida de potencial igual a $-I\,R$ en la resistencia y una diferencia de
potencial $-L\cdot dI/dt$ en el inductor. Seg\'un la ecuaci\'on de los
potenciales la fuerza electromotriz generada en la bater\'ia se consume en $R$ y
$L$ seg\'un la ecuaci\'on
\begin{equation}
\varepsilon-I\,R-L\frac{dI}{dt}=0,
\end{equation}
de donde, multiplicando por $I$ a ambos lados, obtenemos
\begin{equation}
\varepsilon\,I=I^2\,R+L\,I\frac{dI}{dt},
\end{equation}
que es la potencia suministrada por el generador, donde el t\'ermino $I^2\,R$ es
la potencia disipada por la resistencia del circuito (ver
\ecuac~(\ref{ec:PotLeyJoule})). Por \'ultimo, el t\'ermino $L\,I\cdot dI/dt$ es
la energ\'ia por unidad de tiempo en el inductor. Si $U_m$ es la energ\'ia en
el inductor tenemos
\begin{equation}
\frac{dU_m}{dt}=L\,I\frac{dI}{dt},
\end{equation}
de donde se deduce que
\begin{equation}
dU_m=L\,I\,dI,
\end{equation}
ecuaci\'on diferencial que, integrando, proporciona
\begin{equation}
U_m=\int_0^IL\,I\,dI=\frac{1}{2}L\,I^2+C=\frac{1}{2}L\,I^2,
\end{equation}
ecuaci\'on que expresa la energ\'ia almacenada en un inductor que transporta una
corriente $I$ y donde la constante de integraci\'on $C$ vale $0$, ya que $U_m$
ha de anularse cuando $I=0$.

En el proceso de producir corriente en un inductor se genera un campo $\vec{B}$
en el interior de la bobina. As\'i, la energ\'ia almacenada en el conductor se
debe a la energ\'ia almacenada en el campo magn\'etico creado. Por ejemplo, para
el caso particular de un solenoide con $N$ espiras y $l\gg R$, a partir de la
\ecuac~(\ref{ec:BSolenoideCentro}) obtenemos
\begin{equation}
I=\frac{B}{\mu\,n},
\end{equation}
donde $n=N/l$. Por otra parte, el coeficiente de autoinducci\'on
\index{coeficiente!de autoinducci\'on} correspondiente es el expresado en la
\ecuac~(\ref{ec:LSolenoide}), lo que nos lleva a la siguiente expresi\'on para
la energ\'ia magn\'etica:\index{energ\'ia!magn\'etica}
\begin{equation}
U_m=\frac{B^2S\,l}{2\mu}.
\end{equation}
Teniendo en cuenta que el t\'ermino $S\,l$ es el volumen del solenoide se define
la magnitud \textit{densidad de energ\'ia magn\'etica}
\index{energ\'ia!magn\'etica!densidad} como
\begin{equation}
\label{ec:DensEMag}
u_m=\frac{B^2}{2\mu},
\end{equation}
ecuaci\'on, deducida para el caso del inductor, v\'alida para cualquier campo
magn\'etico, independientemente de c\'omo haya sido generado
\citep[\pnbs978]{tipler2010b}. Sus unidades, como densidad de energ\'ia que es,
son $\si[per-mode=reciprocal]{\joule\per\cubic\metre}$ o
$\si[per-mode=reciprocal]{\newton\per\square\metre}$.

\section{Corriente alterna}

Consideremos ahora la variaci\'on de flujo magn\'etico
\index{flujo!magn\'etico!variaci\'on} en el caso de una espira en rotaci\'on
dentro de un campo magn\'etico $\vec{B}$ (ver
\figura~\ref{fig:cap4-04-giro-circuito}). Si la espira, que supondremos de
superficie total $S$, se subdivide en peque\~nos trozos $d\vec{s}$, el flujo
magn\'etico a trav\'es de cada uno de ellos ser\'a, seg\'un la
\ecuac~(\ref{ec:FlujoMagDif}),
\begin{equation}
d\Phi_{\vec{B}}=\vec{B}\cdot d\vec{s}=B\cos\theta\,ds,
\end{equation}
que integrando para toda la superficie da
\begin{equation}
\Phi_{\vec{B}}=\iint\limits_SB\cos\theta\,ds=
B\cos\theta\iint\limits_S ds=B\,S\cos\theta,
\end{equation}
donde se ha considerado que $\vec{B}$ es constante en direcci\'on, sentido y
m\'odulo.

Si ahora hacemos girar la espira con velocidad angular $\omega$ constante
alrededor de un eje vertical (perpendicular a los vectores $\vec{B}$ y
$d\vec{s}$), el \'angulo entre el vector inducci\'on magn\'etica y el vector
normal a la superficie del circuito variar\'a con el tiempo seg\'un la
ecuaci\'on
\begin{equation}
\theta=\omega\,t,
\end{equation}
por lo que el flujo magn\'etico a trav\'es de la espira queda en la forma
\begin{equation}
\Phi_{\vec{B}}=B\,S\cos(\omega\,t),
\end{equation}
ecuaci\'on que puede generalizarse como
\begin{equation}
\label{ec:FlujoMagEspiraRotac}
\Phi_{\vec{B}}=B\,N\,S\cos(\omega\,t),
\end{equation}
para el caso de un circuito con $N$ espiras.

Como la \ecuac~(\ref{ec:FlujoMagEspiraRotac}) depende del tiempo, el flujo
experimentar\'a una variaci\'on (ver de nuevo
\lafigura~\ref{fig:cap4-04-giro-circuito}): ser\'a m\'aximo (hacia la derecha)
cuando el plano de la espira sea perpendicular al campo magn\'etico $\vec{B}$ y
$\cos\theta=\cos 0=1$, luego pasar\'a por cero cuando la espira est\'e en el
plano que contiene al vector $\vec{B}$ y $\cos\theta=\cos\pi/2=0$, a
continuaci\'on el flujo ser\'a m\'inimo (m\'aximo en valor absoluto, pero
negativo, pues el vector $d\vec{s}$ formar\'a un \'angulo igual a $\pi$ con el
vector inducci\'on) y, finalmente, volver\'a a ser cero cuando la espira est\'a
de nuevo en el plano del papel. Como el flujo magn\'etico que atraviesa la
espira est\'a variando con el tiempo mientras \'esta gira, aparecer\'a en ella,
seg\'un la ley de Faraday-Henry, una fuerza electromotriz inducida dada por
\begin{equation}
\varepsilon_i=-\frac{d\Phi_{\vec{B}}}{dt}=B\,N\,S\,\omega\sin(\omega\,t),
\end{equation}
de donde, teniendo en cuenta que la funci\'on seno produce valores en el
intervalo $[-1,\,1]$, se deduce que su valor m\'aximo es
\begin{equation}
\varepsilon_{\max}=B\,N\,S\,\omega,
\end{equation}
pudiendo expresarse la fuerza electromotriz inducida como
\begin{equation}
\label{ec:EpsAlterna}
\varepsilon_i=\varepsilon_{\max}\sin(\omega\,t).
\end{equation}

La \ecuac~(\ref{ec:EpsAlterna}) es una funci\'on senoidal que oscila entre los
valores $\varepsilon_{\max}$ y $-\varepsilon_{\max}$, como se puede apreciar en
\lafigura~\ref{fig:cap4-07-grafico-sinusoide-c-alterna}. A dicha funci\'on se le
llama \textit{fuerza electromotriz alterna},\index{fuerza!electromotriz!alterna}
y \textit{generador de corriente alterna}
\index{generador!de corriente alterna|see{alternador}} o \textit{alternador}
\index{alternador} al dispositivo que la genera.\index{corriente!alterna}
\begin{figure}[htb]
\centering
\includegraphics[width=0.60\textwidth]{fig-cap4-07-grafico-sinusoide-c-alterna}
\caption{Variaci\'on de la fuerza electromotriz inducida en un alternador.}
\label{fig:cap4-07-grafico-sinusoide-c-alterna}
\end{figure}
El per\'iodo de la funci\'on es $T=2\pi/\omega$, donde a $\omega$ se le denomina
en el lenguaje propio de la corriente alterna \textit{frecuencia angular de la
corriente}.\index{corriente!alterna!frecuencia angular} Sin embargo, es m\'as
frecuente caracterizar la oscilaci\'on de la corriente alterna mediante la
frecuencia lineal\index{corriente!alterna!frecuencia} $\nu=2\pi/\omega$. As\'i,
decimos que la corriente alterna tiene una frecuencia de, por ejemplo,
$\SI{50}{\hertz}$, lo que significa que tiene un per\'iodo de repetici\'on de
$T=1/\nu=\SI{0.02}{\second}$. Por ello, una l\'ampara de nuestras casas en
realidad se enciende y se apaga $100$ veces en cada segundo ($50$ veces con
polaridad positiva y otras $50$ veces con polaridad negativa), un ritmo
demasiado r\'apido para que sea detectado por el ojo humano.

Si la espira del circuito tiene una resistencia $R$ entonces la corriente
alterna inducida ser\'a
\begin{equation}
I_i=\frac{\varepsilon_i}{R}=\frac{\varepsilon_{\max}}{R}\sin(\omega\,t)=
I_{\max}\cdot\sin(\omega\,t).
\end{equation}

En definitiva, por el mero hecho de hacer girar una espira dentro de una
regi\'on sometida a una inducci\'on magn\'etica $\vec{B}$, por ejemplo debida a
un im\'an, se consigue generar una corriente alterna\index{corriente!alterna} en
dicha regi\'on. Es decir, un simple efecto mec\'anico-magn\'etico permite
generar corriente, lo que constituye la base de la moderna producci\'on de
electricidad a gran escala. As\'i, en una central hidroel\'ectrica el agua que
cae en el salto desde la presa hasta su base hace mover una turbina que, a su
vez, provoca el giro de una bobina dentro de una inducci\'on magn\'etica
(alternador),\index{alternador} dando lugar a una variaci\'on de flujo
magn\'etico a su trav\'es. Esto induce la aparici\'on de una corriente
el\'ectrica alterna,\index{corriente!alterna} que luego es transformada a una
tensi\'on elevada para ser distribuida por los cables de alta tensi\'on (con
menor intensidad de corriente para reducir las p\'erdidas por calor Joule),
siendo finalmente de nuevo transformada, pero ahora a tensi\'on baja, para que
llegue as\'i a nuestros domicilios, comercios, industrias, etc.
