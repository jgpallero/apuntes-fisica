\chapter{Corriente el\'ectrica}
\label{cap:CoE}

\section{Corriente el\'ectrica}

La corriente el\'ectrica\index{corriente!el\'ectrica} es, en sentido amplio, el
movimiento ordenado de cargas el\'ectricas\index{carga!el\'ectrica} en un medio
cualquiera \citep[\pnbs85]{dejuana2003b}. Tambi\'en se puede definir como el
flujo de cargas el\'ectricas que atraviesa la secci\'on transversal de un
conductor por unidad de tiempo \citep[\pnbs840]{tipler2010b}. Como veremos m\'as
adelante, para conseguir el movimiento ordenado de las cargas es imprescindible
la presencia de un campo el\'ectrico\index{campo!el\'ectrico} (o diferencia de
potencial).\index{diferencia de potencial}

Normalmente, y as\'i lo haremos aqu\'i, se habla de corriente el\'ectrica en
metales, debida al movimiento de los electrones libres de estos materiales.
Pero, de un modo m\'as general, puede haber corriente por el desplazamiento de
iones, tanto positivos como negativos, en el seno de un l\'iquido mediante la
llamada conducci\'on electrol\'itica. Incluso se puede tener una corriente
el\'ectrica que viaje por el vac\'io, sin necesidad de un soporte material,
trat\'andose en este caso de conducci\'on el\'ectrica mediante haces de
part\'iculas cargadas.

En un \'atomo de cobre, por ejemplo, existen $29$ electrones ligados al n\'ucleo
por su atracci\'on electrost\'atica. Los electrones m\'as externos est\'an
ligados m\'as d\'ebilmente que los m\'as internos a causa de su mayor distancia
al n\'ucleo y la repulsi\'on de los electrones m\'as internos. Cuando un gran
n\'umero de \'atomos de cobre se combinan en una pieza de cobre met\'alico, el
enlace de los electrones de cada \'atomo individual se modifica por
interacciones con los \'atomos pr\'oximos y uno o m\'as de los electrones
externos de cada \'atomo queda en libertad para moverse por todo el metal. El
n\'umero de electrones libres depende del metal particular, pero t\'ipicamente
oscila alrededor de un electr\'on por \'atomo.

La magnitud empleada para medir si ese movimiento es m\'as o menos intenso es la
\textit{intensidad de corriente el\'ectrica}
\index{intensidad!de corriente el\'ectrica} que, de acuerdo a la definici\'on
dada en el primer p\'arrafo, es
\begin{equation}
\label{ec:IntCorr}
I=\frac{dQ}{dt},
\end{equation}
donde la secci\'on atravesada por las cargas el\'ectricas se supone
perpendicular a su direcci\'on de desplazamiento. La intensidad de corriente
el\'ectrica es una magnitud escalar y su unidad es el
amperio\footnote{\href{\biomate{Ampere}}{Andr\'e Marie Amp\`ere (1775--1836)}.}:
\indnp{Amp\`ere, Andr\'e Marie}
$\SI{1}{\ampere}=\SI[per-mode=reciprocal]{1}{\coulomb\per\second}$.
\index{unidades!amperio} N\'otese que dado que $\SI{1}{\coulomb}$ es una
cantidad de carga enorme, $\SI{1}{\ampere}$ tambi\'en ser\'a una intensidad de
corriente elevada. Por ello, en circuitos t\'ipicos de microelectr\'onica se
tienen corrientes del orden de $\si{\milli\ampere}$ y, de hecho, una intensidad
de s\'olo $\SI{70}{\milli\ampere}$ ya resulta peligrosa para el ser humano,
pudiendo causarle una fibrilaci\'on card\'iaca (contracciones irregulares del
m\'usculo del coraz\'on). En la naturaleza, un rayo en una tormenta puede
llegar a alcanzar intensidades de hasta $\SI{e4}{\ampere}$.

Cuando sobre un conductor no act\'ua ning\'un campo los electrones se mueven
libremente, siendo su velocidad promedio nula
(\figura~\ref{fig:cap2-01-elec-conductor}, izquierda). En presencia de un campo
el\'ectrico $\vec{E}$ son acelerados con una fuerza $\vec{F}=-e\,\vec{E}$
(\ecuac~(\ref{ec:FqEma})), por lo que comienzan a desplazarse en sentido opuesto
al campo el\'ectrico (\figura~\ref{fig:cap2-01-elec-conductor}, derecha). En su
movimiento, los electrones van chocando con los \'atomos que componen el
material y pierden energ\'ia, que vuelven a adquirir al ser acelerados de nuevo
por el campo. El resultado de este proceso es un desplazamiento de los
electrones de velocidad media $\vec{v}_d$, denominada \textit{velocidad de
desplazamiento o de arrastre}.
\index{velocidad!de arrastre (corriente el\'ectrica)}
\index{velocidad!de desplazamiento|see{velocidad de arrastre (corriente el\'ectrica)}}
\begin{figure}[htb]
\centering
\includegraphics[width=0.75\textwidth]{fig-cap2-01-elec-conductor}
\caption{Electrones libres en ausencia (izquierda) y en presencia (derecha) de
         un campo el\'ectrico.}
\label{fig:cap2-01-elec-conductor}
\end{figure}

Sea $n$ el n\'umero de electrones por unidad de volumen (densidad) que
atraviesan una secci\'on $A$ de cable conductor en un tiempo $dt$, $e$ el valor
absoluto de la carga del electr\'on y $v_d$ el m\'odulo de su velocidad de
desplazamiento. Entonces, la carga $dQ$ que atraviesa dicha secci\'on en el
tiempo $dt$ es
\begin{equation}
dQ=e\,n\,A\,v_d\,dt,
\end{equation}
de donde, teniendo en cuenta la \ecuac~(\ref{ec:IntCorr}), obtenemos
\begin{equation}
\label{ec:IntCorrENAVD}
I=\frac{dQ}{dt}=e\,n\,A\,v_d.
\end{equation}

La direcci\'on y el sentido de la corriente el\'ectrica
\index{corriente!el\'ectrica} coinciden con las del campo el\'ectrico
\index{campo!el\'ectrico} $\vec{E}$ que la genera. Esto es un \textbf{convenio}
que deriva de haber utilizado el valor absoluto de la carga del electr\'on, $e$.
En realidad, el desplazamiento de los electrones
\index{desplazamiento!de los electrones} (el movimiento real de carga) se da en
el sentido \textbf{opuesto} al del campo el\'ectrico $\vec{E}$, como se indica
en \lafigura~\ref{fig:cap2-01-elec-conductor} (derecha). Este criterio
convencional obedece a razones hist\'oricas.
\Lafigura~\ref{fig:cap2-02-sentido-corriente} muestra esquem\'aticamente los
sentidos convencional y real de desplazamiento de los electrones. La corriente
el\'ectrica se puede interpretar como un flujo de electrones (portadores
negativos de carga) desde el cuerpo de menor potencial al de mayor potencial o
como un flujo de portadores de carga positiva desde el cuerpo de mayor al de
menor potencial.
\begin{figure}[htb]
\centering
\includegraphics[width=0.34\textwidth]{fig-cap2-02-sentido-corriente}
\caption{Sentido de la corriente el\'ectrica.}
\label{fig:cap2-02-sentido-corriente}
\end{figure}

Dada la definici\'on anterior para la intensidad de corriente el\'ectrica
\index{intensidad!de corriente el\'ectrica} se deduce que cuanta m\'as secci\'on
tenga el conductor, mayor carga podr\'a atravesarla, esto es, llevar\'a mayor
intensidad de corriente. Para caracterizar lo buen conductor que es un material
determinado con respecto a otro se utiliza el concepto de
\textit{densidad de corriente},\index{densidad de corriente} que se define como
la intensidad por unidad de secci\'on. La densidad de corriente es un vector que
en cada punto del conductor tiene el m\'odulo igual a la cantidad de
electricidad que atraviesa una superficie normal a la direcci\'on de la
corriente por unidad de tiempo. Su direcci\'on coincide con la de la corriente y
su sentido con el del movimiento de los portadores de carga
\textbf{positiva}\footnote{Como se ha indicado, las que en realidad se mueven
son las cargas negativas (electrones), pero ficticiamente se habla de movimiento
de cargas positivas cuando se quiere indicar el sentido convencional de la
corriente.} \citep[\pnbs87]{dejuana2003b}, por lo que
\begin{equation}
\label{ec:DensCorr}
\vec{J}=\frac{dI}{dA}\vec{u},
\end{equation}
donde $\vec{u}$ es un vector unitario de direcci\'on y sentido iguales al
convencional de la corriente. Sus unidaden en el SI son
$\si[per-mode=reciprocal]{\ampere\per\square\metre}$. Teniendo en cuenta las
\ecuacs~(\ref{ec:IntCorrENAVD}) y~(\ref{ec:DensCorr}) se deduce para el m\'odulo
del vector densidad de corriente que
\begin{equation}
J=e\,n\,v_d.
\end{equation}

La intensidad de corriente\index{intensidad!de corriente el\'ectrica} indica
entonces cu\'antos portadores de carga atraviesan una secci\'on por unidad de
superficie y tiempo, y puede variar de un punto a otro del material. Su m\'odulo
se calcular\'ia como
\begin{equation}
I=\iint\vec{J}\cdot d\vec{A}=\iint J\cos\alpha\,dA,
\end{equation}
donde $\alpha$ es el \'angulo que forma el vector densidad de corriente con el
vector perpendicular a la secci\'on del conductor.
\begin{figure}[htb]
\centering
\includegraphics[width=0.85\textwidth]{fig-cap2-03-iguala-potenciales}
\caption{Conexi\'on de dos conectores con diferente potencial.}
\label{fig:cap2-03-iguala-potenciales}
\end{figure}

Sean dos conductores $A$ y $B$ con diferente potencial y $V_A>V_B$.
\index{potencial!el\'ectrico} Ambos generan un campo el\'ectrico dirigido en el
sentido decreciente de sus potenciales
(\figura~\ref{fig:cap2-03-iguala-potenciales}, izquierda). Si se unen dichos
conductores (ver \figura~\ref{fig:cap2-03-iguala-potenciales}, derecha) la carga
se transfiere hasta que se igualan los potenciales y la energ\'ia potencial del
sistema. Antes de unir los conductores la energ\'ia potencial del sistema es
(\ecuac~(\ref{ec:EPotSistema}))
\begin{equation}
\label{ec:EpSeparados}
U=\frac{Q_AV_A+Q_BV_B}{2},
\end{equation}
mientras que despu\'es de unirlos es
\begin{equation}
\label{ec:EpUnidos}
U=\frac{(Q_A+Q_B)V}{2}.
\end{equation}
Igualando, ya que la energ\'ia potencial se conserva, las
\ecuacs~(\ref{ec:EpSeparados}) y~(\ref{ec:EpUnidos}) obtenemos para el potencial
\citep[\pnbs85]{dejuana2003b}
\begin{equation}
V=\frac{Q_AV_A+Q_BV_B}{Q_A+Q_B}.
\end{equation}

\section{Ley de Ohm, resistencia y conductividad}

La \textit{ley de
Ohm}\footnote{\href{\biomate{Ohm}}{Georg Simon Ohm (1789--1854)}.}
\indnp{Ohm, Georg Simon}\index{ley!de Ohm} es una ley experimental que establece
que la densidad de corriente en un conductor es proporcional al campo
el\'ectrico que la genera y depende de las caracter\'isticas particulares de
dicho material conductor. Su expresi\'on general es
\begin{equation}
\label{ec:JsigmaE}
\vec{J}=\sigma\,\vec{E},
\end{equation}
donde $\sigma$ es la conductividad el\'ectrica\index{conductividad el\'ectrica}
del material, que es dependiente de caracter\'isticas tales como la estructura
cristalina, densidad de electrones, temperatura, etc., e independiente de la
geometr\'ia del conductor.

La conductividad el\'ectrica $\sigma$ es una caracter\'istica del material que
indica la menor o mayor facilidad que presenta para que se muevan los portadores
de carga en su interior, por lo que los conductores se caracterizar\'an por
tener valores altos de $\sigma$, mientras que en los diel\'ectricos la
conductividad ser\'a baja. Los conductores cuya conductividad es independiente
del valor del campo el\'ectrico se denominan \textit{lineales}
\index{conductor!lineal} u \textit{\'ohmicos}.
\index{conductor!ohmico@\'ohmico|see{conductor lineal}} La unidad de la
conductividad el\'ectrica es el siemens partido de metro,
$\si[per-mode=reciprocal]{\siemens\per\metre}$, donde el
siemens\footnote{\href{https://es.wikipedia.org/wiki/Werner_von_Siemens}
{Ernst Werner von Siemens (1816--1892)}.}\indnp{Siemens, Ernst Werner von} se
define como $\SI{1}{\siemens}=\SI[per-mode=reciprocal]{1}{\ampere\per\volt}$.
Tambi\'en tenemos que $\SI{1}{\siemens}=\SI{1}{\ohm}$, donde la unidad
$\si{\ohm}$ se definir\'a a continuaci\'on.
\index{unidades!siemens}
\begin{figure}[htb]
\centering
\includegraphics[width=0.95\textwidth]{fig-cap2-04-variacion-resistividad}
\caption{Variaci\'on de la resistividad el\'ectrica $\rho$ en funci\'on de la
         temperatura para materiales conductores (izquierda), diel\'ectricos y
         semiconductores (centro) y superconductores (derecha).}
\label{fig:cap2-04-variacion-resistividad}
\end{figure}

Se define tambi\'en la \textit{resistividad el\'ectrica}
\index{resistividad el\'ectrica} como la inversa de la conductividad:
\begin{equation}
\rho=\frac{1}{\sigma},
\end{equation}
cuyas unidades son $\si{\ohm\metre}$, donde la unidad $\si{\ohm}$ de denomina
ohmio y vale $\SI{1}{\ohm}=\SI[per-mode=reciprocal]{1}{\volt\per\ampere}$. En
primera aproximaci\'on, la resistividad en los medios conductores var\'ia
linealmente con la temperatura:
\begin{equation}
\rho=\rho_0+(T-T_0)\frac{d\rho}{dT},
\end{equation}
si bien dicha variaci\'on no es igual en todos los materiales. En los cuerpos
conductores la resistividad aumenta con la temperatura, existiendo siempre una
resistividad residual para valores cercanos al cero absoluto
(\figura~\ref{fig:cap2-04-variacion-resistividad}, izquierda). En los materiales
diel\'ectricos\index{diel\'ectrico} o\index{semiconductor}
semiconductores\footnote{Un semiconductor es un material que se puede comportar
como conductor o aislante, dependiendo de determinadas condiciones como la
temperatura, presi\'on, campo electromagn\'etico, etc.} la resistividad decrece
con la temperatura (\figura~\ref{fig:cap2-04-variacion-resistividad}, centro),
mientras que en los cuerpos superconductores\index{superconductor} cae a cero a
temperaturas pr\'oximas al cero absoluto
(\figura~\ref{fig:cap2-04-variacion-resistividad}, derecha).

La diferencia de potencial\index{diferencia de potencial} $\Delta V$ entre dos
puntos de un conductor por los que circula una corriente de intensidad $I$
verifica que
\begin{equation}
\label{ec:VRI}
\Delta V=R\,I,
\end{equation}
que es la forma m\'as habitual de expresar la ley de Ohm\index{ley!de Ohm} y
donde $R$ es la \textit{resistencia el\'ectrica}.\index{resistencia el\'ectrica}
La resistencia el\'ectrica de un conductor es una medida de la oposici\'on que
presenta al paso de la corriente el\'ectrica. Su unidad es el ohmio
\index{unidades!ohmio}
($\SI{1}{\ohm}=\SI[per-mode=reciprocal]{1}{\volt\per\ampere}$), que es la
resistencia que presenta un conductor que transporta una intensidad de
$\SI{1}{\ampere}$ cuando la diferencia de potencial entre sus extremos es de
$\SI{1}{\volt}$.

La resistencia depende de la resistividad $\rho$\index{resistividad el\'ectrica}
y de la geometr\'ia del conductor. Sea un conductor de secci\'on $S$ por el que
circula una corriente de intensidad $I$ y tomemos dos puntos $A$ y $B$ en \'el.
La diferencia de potencial entre ellos es (\ecuac~(\ref{ec:CABDeltaV}))
\begin{equation}
\label{ec:LeyOhm}
V_A-V_B=\int_A^B\vec{E}\cdot d\vec{L}=\int_A^BE\,dL=
\int_A^B\frac{J}{\sigma}\,dL=\int_A^B\frac{I}{S\,\sigma}\,dL=
I\int_A^B\frac{dL}{S\,\sigma}=I\,R,
\end{equation}
que demuestra la \ecuac~(\ref{ec:VRI}) y donde se ha tenido en cuenta que
$\vec{E}$ y $d\vec{L}$ son vectores de la misma direcci\'on y sentido, por lo
que $\vec{E}\cdot d\vec{L}=E\,dL$, y se ha utilizado el m\'odulo de la
\ecuac~(\ref{ec:DensCorr}) y la expresi\'on~(\ref{ec:JsigmaE}). Tambi\'en se ha
considerado la intensidad como una constante entre $A$ y~$B$. Por tanto, la
resistencia de un elemento $dL$ de conductor de secci\'on $S$ entre dos puntos
$A$ y~$B$ viene dada por
\begin{equation}
R=\int_A^B\frac{dL}{S\,\sigma}=\int_A^B\rho\frac{dL}{S}=\rho\frac{L}{S},
\end{equation}
de donde se concluye que la resistencia el\'ectrica
\index{resistencia el\'ectrica} de un material aumenta cuanto mayor es su
longitud y menor su secci\'on.
\begin{figure}[htb]
\centering
\includegraphics[width=0.85\textwidth]{fig-cap2-05-asociacion-resistencias}
\caption{Asociaci\'on de resistencias: en serie (izquierda) y en paralelo
         (derecha).}
\label{fig:cap2-05-asociacion-resistencias}
\end{figure}

En un circuito cualquiera las resistencias se pueden asociar, present\'andose
los dos casos b\'asicos siguientes:
\begin{enumerate}
\item Asociaci\'on en serie (\figura~\ref{fig:cap2-05-asociacion-resistencias},
      izquierda):\index{resistencias el\'ectricas!asociaci\'on en serie}
\begin{itemize}
\item La intensidad que recorre las resistencias es la misma.
\item La diferencia de potencial entre los extremos de cada una no es igual
      ($\Delta V_A^B\neq\Delta V_B^C$).
\item Se cumple la siguiente relaci\'on entre diferencias de potencial:
      $\Delta V_A^C=\Delta V_A^B+\Delta V_B^C$.
\item La resistencia equivalente es
      \begin{equation}
      R_\text{equiv}=R_1+R_2.
      \end{equation}
\end{itemize}
\item Asociaci\'on en paralelo
      (\figura~\ref{fig:cap2-05-asociacion-resistencias}, derecha):
      \index{resistencias el\'ectricas!asociaci\'on en paralelo}
\begin{itemize}
\item La intensidad que recorre las resistencias es diferente ($I_1\neq I_2$).
\item Se verifica la siguiente relaci\'on entre intensidades:
      $I_\text{total}=I_1+I_2$.
\item La diferencia de potencial entre los extremos de cada resistencia es
      igual.
\item La resistencia equivalente es
      \begin{equation}
      \frac{1}{R_\text{equiv}}=\frac{1}{R_1}+\frac{1}{R_2}.
      \end{equation}
\end{itemize}
\end{enumerate}

\section{Ley de Joule y potencia disipada}

Si se establece una diferencia de potencial entre dos puntos de un conductor o
circuito se genera una corriente el\'ectrica. El transporte de una cantidad de
carga $dq$ se traduce en un trabajo o liberaci\'on de energ\'ia $dW$ tal que
(ver \ecuac~(\ref{ec:TrabajoE}))
\begin{equation}
dW=(V_A-V_B)\,dq.
\end{equation}

La \textit{potencia}\index{potencia} $P$ entre los puntos $A$ y $B$ del
conductor se define como la energ\'ia entregada por unidad de tiempo:
\begin{equation}
P=\frac{dW}{dt}=(V_A-V_B)\frac{dq}{dt}=(V_A-V_B)\,I,
\end{equation}
y su unidad es el
vatio\footnote{\href{https://es.wikipedia.org/wiki/James_Watt}
{James Watt (1736--1819)}.}\indnp{Watt, James}\index{unidades!vatio}
($\SI{1}{\watt}=\SI[per-mode=reciprocal]{1}{\joule\per\second}=\SI{1}{\volt\ampere}$).

Si no existe ning\'un otro elemento que aporte o sustraiga energ\'ia de un
circuito, la energ\'ia asociada al transporte de cargas se consume y se pierde
en forma de calor a causa de la resistencia $R$ del conductor. Esta p\'erdida de
energ\'ia en forma de calor, debido al choque de los electrones con los \'atomos
del conductor, aumentando as\'i su agitaci\'on a costa de su energ\'ia, se
conoce como
\textit{efecto Joule}\footnote{\href{https://es.wikipedia.org/wiki/James_Prescott_Joule}
{James Prescott Joule (1818--1889)}.}\indnp{Joule, James Prescott} y produce el
calentamiento del conductor.\index{efecto!Joule}

La ley de Joule\index{ley!de Joule} establece que la cantidad de energ\'ia
el\'ectrica transformada (disipada) en calor en una resistencia $R$
\index{resistencia el\'ectrica} es proporcional al cuadrado de la intensidad y
al valor de la propia resistencia, y su expresi\'on anal\'itica es
\begin{equation}
\label{ec:PotLeyJoule}
P=\frac{dW}{dt}=(V_A-V_B)\,I=R\,I^2.
\end{equation}

\section{Fuerza electromotriz y ecuaci\'on del circuito}

Una corriente estacionaria,\index{corriente!estacionaria} que es aqu\'ella en la
que la densidad de carga en el conductor se mantiene constante en el tiempo, no
puede mantenerse \'unicamente por la acci\'on de un campo electrost\'atico. En
un circuito cerrado en el que act\'ua un campo el\'ectrico (electrost\'atico)
$\vec{E}$ sabemos por la \ecuac~(\ref{ec:CABDeltaV}) que la circulaci\'on
\index{circulaci\'on!de un campo el\'ectrico} es cero. Por otra parte, de
acuerdo con la expresi\'on~(\ref{ec:LeyOhm}), si el circuito es cerrado los
puntos $A$ y $B$ ser\'an coincidentes, de donde se deduce que $I=0$. En un campo
potencial, los electrones que parten de un punto y dan una vuelta completa a un
circuito cerrado hasta llegar al mismo punto no ganan ni pierden energ\'ia, por
lo que la energ\'ia que se pierde por el efecto Joule\index{efecto!Joule} no
puede proceder en ning\'un caso de un campo conservativo.

Por tanto, para establecer una corriente en un circuito se precisa de un campo
el\'ectrico $\vec{E}_g$ creado por un generador\footnote{La fuente de energ\'ia
no proporciona electrones, sino que mueve los que ya existen libres en el
conductor.},\index{generador} adem\'as del campo electrost\'atico potencial
$\vec{E}_e$, de forma que el campo el\'ectrico total ser\'a
\begin{equation}
\vec{E}=\vec{E}_e+\vec{E}_g=\frac{R\,S}{L}\vec{J},
\end{equation}
donde $L$ y $S$ son la longitud y secci\'on del circuito, respectivamente.

A la circulaci\'on\index{circulaci\'on!de un campo el\'ectrico} del campo
el\'ectrico $\vec{E}_g$, que no deriva de un potencial y que, por lo tanto, no
es nula, se le denomina \textit{fuerza electromotriz}\footnote{Adem\'as del
s\'imbolo $\varepsilon$, en ocasiones se utilizan las siglas f.e.m. para
denotarla.} del generador:\index{fuerza!electromotriz}
\begin{equation}
\label{ec:FElectromotriz}
\varepsilon=\oint\vec{E}\cdot d\vec{L}=
\oint(\vec{E}_e+\vec{E}_g)\cdot d\vec{L}=
\cancelto{^\text{\footnotesize{campo conservativo}}}{\oint\vec{E}_e\cdot d\vec{L}}+\oint\vec{E}_g\cdot d\vec{L}=
R\,I.
\end{equation}
Como el generador tiene una resistencia interna $r$, la resistencia total $R$ es
la suma de la resistencia total $R_0$ del circuito y la interna del generador,
por lo que\index{resistencia el\'ectrica!interna}
\index{resistencia el\'ectrica!total}
\begin{equation}
\label{ec:fem}
\varepsilon=R\,I=R_0\,I+r\,I=V_A-V_B+r\,I,
\end{equation}
y tiene como unidad el voltio\footnote{Hay que tener en cuenta que aunque se
hable de \textit{fuerza} electromotriz, \'esta no es en realidad una fuerza en
el sentido f\'isico de la palabra, sino una diferencia de potencial.},
$\si{\volt}$. De la \ecuac~(\ref{ec:fem}) se deduce que para una fuerza
electromotiz\index{fuerza!electromotriz} constante la diferencia de potencial
disminuye si aumenta la intensidad de corriente (ver
\figura~\ref{fig:cap2-06-fem}, izquierda). Queda entonces definido un generador
por la fuerza electromotriz $\varepsilon$ que es capaz de aportar y por su
resistencia interna $r$. \Lafigura~\ref{fig:cap2-06-fem} (derecha) muestra el
s\'imbolo convencional utilizado para identificar un generador.
\begin{figure}[htb]
\centering
\includegraphics[width=0.67\textwidth]{fig-cap2-06-fem}
\caption{Variaci\'on de la diferencia de potencial con la intensidad en un
         generador (izquierda) y su s\'imbolo convencional (derecha).}
\label{fig:cap2-06-fem}
\end{figure}

La fuerza electromotriz\index{fuerza!electromotriz} es la energ\'ia que debe
desarrollar un generador para hacer circular la unidad de carga a trav\'es de
todo el circuito (incluido el propio generador), por lo que otra manera de
expresarla es
\begin{equation}
\varepsilon=\frac{dW}{dq}\rightarrow dW=\varepsilon\,dq,
\end{equation}
lo que combinado con las \ecuacs~(\ref{ec:PotLeyJoule}) y~(\ref{ec:fem}) nos
conduce a
\begin{equation}
P=\frac{dW}{dt}=\varepsilon\frac{dq}{dt}=\varepsilon\,I=
(V_A-V_B)\,I+r\,I^2,
\end{equation}
luego la potencia del generador se invierte por una parte en hacer circular una
corriente por un circuito de resistencia equivalente $R_0$, y por otra en
consumo de su resistencia interna.

La tensi\'on entre los bornes de un generador es igual a su fuerza electromotriz
menos la ca\'ida \'ohmica debida a la resistencia en su interior:
\begin{equation}
V_A-V_B=\varepsilon-r\,I.
\end{equation}

Se define el \textit{rendimiento del generador},\index{generador!rendimiento}
$\eta$, como el cociente entre la potencia suministrada por el generador al
circuito y la potencia total que produce dicho generador:
\begin{equation}
\eta=\frac{I\,\Delta V}{\varepsilon\,I}=\frac{\Delta V}{\varepsilon}=
1-\frac{r\,I}{\varepsilon}.
\end{equation}

En un circuito, un \textit{receptor}\index{receptor (circuito el\'ectrico)} es
todo aparato que recibe energ\'ia y la transforma en otro tipo de energ\'ia
distinta al calor disipado por efecto Joule. La bombilla incandescente es un
tipo de receptor que transforma la energ\'ia el\'ectrica del circuito en
energ\'ia calor\'ifica, que calienta un filamento que emite luz, consumiendo una
potencia $P$. Se caracteriza por la potencia que desarrolla y el potencial
$\Delta V$ al que debe ser conectada. Se representa por el s\'imbolo indicado en
\lafigura~\ref{fig:cap2-07-motor-bombilla} (izquierda). El \textit{motor} es
\index{motor (circuito el\'ectrico)} otro tipo de receptor que transforma la
energ\'ia el\'ectrica del circuito en energ\'ia mec\'anica. Se representa por el
s\'imbolo indicado en \lafigura~\ref{fig:cap2-07-motor-bombilla} (izquierda),
donde $\varepsilon'$ representa la \textit{fuerza contraelectromotriz}
\index{fuerza!contraelectromotriz} y $r'$ la resistencia interna.
\begin{figure}[htb]
\centering
\includegraphics[width=0.60\textwidth]{fig-cap2-07-motor-bombilla}
\caption{Simbolos de bombilla (izquierda) y motor (derecha).}
\label{fig:cap2-07-motor-bombilla}
\end{figure}

Sea un motor conectado en los puntos de un circuito $A$ y $B$, con potenciales
$V_A$ y $V_B$:
\begin{itemize}
\item La potencia entregada por el circuito es $(V_A-V_B)\,I$.
\item La potencia mec\'anica producida por el motor es $P'$.
\item La potencia consumida por el motor en su resistencia interna $r'$ es
      $r'\,I^2$.
\end{itemize}
Por todo ello:
\begin{equation}
(V_A-V_B)\,I=P'+r'\,I^2,
\end{equation}
de donde se deduce que la potencia mec\'anica es
\begin{equation}
P'=(V_A-V_B-r'\,I)\,I.
\end{equation}

Se define \textit{fuerza contraelectromotriz}\index{fuerza!contraelectromotriz}
$\varepsilon'$ del receptor (en el ejemplo, de un motor) como la energ\'ia
el\'ectrica transformada en energ\'ia mec\'anica por unidad de carga:
\begin{equation}
\varepsilon'=V_A-V_B-r'\,I,
\end{equation}
por lo que la potencia mec\'anica\index{potencia} $P'$ se puede expresar como
\begin{equation}
P'=\varepsilon'\,I.
\end{equation}

El \textit{rendimiento del motor},\index{rendimiento de un motor} $\eta$, es el
cociente entre la potencia mec\'anica producida por \'este y la absorbida por el
circuito:
\begin{equation}
\eta=\frac{\varepsilon'\,I}{(V_A-V_B)\,I}=
\frac{V_A-V_B-r'\,I}{V_A-V_B}=1-\frac{r'\,I}{V_A-V_B}.
\end{equation}

Supongamos un circuito compuesto por un generador, un motor y una resistencia
\'ohmica equivalente $R$ del circuito, representado esquem\'aticamente en
\lafigura~\ref{fig:cap2-08-circuito-ab-cerr} (izquierda) y donde se define:
\begin{itemize}
\item Potencia entre los puntos $A$ y $B$: $(V_A-V_B)\,I$.
\item Potencia producida por el generador: $\varepsilon\,I$.
\item Potencia consumida por el generador: $r\,I^2$.
\item Potencia consumida por la resistencia: $R\,I^2$.
\item Potencia consumida por el motor y transformada en potencia mec\'anica:
      $\varepsilon'\,I$.
\item Potencia consumida por la resistencia interna del motor: $r'\,I^2$.
\end{itemize}
\begin{figure}[htb]
\centering
\includegraphics[width=0.999\textwidth]{fig-cap2-08-circuitos}
\caption{Circuitos con generador, resistencia y motor. Abierto (izquierda) y
         cerrado (derecha).}
\label{fig:cap2-08-circuito-ab-cerr}
\end{figure}

El balance de energ\'ias es, entonces,
\begin{equation}
(V_A-V_B)\,I+\varepsilon\,I=
r\,I^2+R\,I^2+r'\,I^2+\varepsilon'\,I,
\end{equation}
que, reordenando, queda
\begin{equation}
\label{ec:CircuitoAbierto}
V_A-V_B+\varepsilon-\varepsilon'=(r+R+r')\,I,
\end{equation}
donde se ha tenido en cuenta que las fuerzas electromotrices son positivas si la
corriente las atraviesa del polo negativo al positivo (y negativas en caso
contrario), y que las fuerzas contraelectromotrices son \textbf{siempre}
negativas.

Si se cierra el circuito (ver \figura~\ref{fig:cap2-08-circuito-ab-cerr},
derecha) el potencial de los puntos $A$ y $B$ se iguala, por lo que $V_A-V_B=0$
y la \ecuac~(\ref{ec:CircuitoAbierto}) se transforma en
\begin{equation}
\varepsilon-\varepsilon'=(r+R+r')\,I,
\end{equation}
que, reorganizada, queda como
\begin{equation}
I=\frac{\varepsilon-\varepsilon'}{r+R+r'}=\frac{\sum\varepsilon}{\sum R},
\end{equation}
y que se denomina \textit{ecuaci\'on del circuito}.
\index{ecuaci\'on!de un circuito} Tenemos, pues, que la intensidad que circula
por un circuito cerrado es el cociente de la suma algebraica de todas las
fuerzas electromotrices y contraelectromotrices entre la suma de todas las
resistencias.
